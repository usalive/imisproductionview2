$(function () {

    'use strict';

    var console = window.console || { log: function () { } };
    var $images = $('.docs-pictures');
    var $toggles = $('.docs-toggles');
    var $buttons = $('.docs-buttons');
    var options = {
        // inline: true,
        url: 'data-original',
        ready: function (e) {
            console.log(e.type);
        },
        show: function (e) {
            console.log(e.type);
        },
        shown: function (e) {
            console.log(e.type);
        },
        hide: function (e) {
            console.log(e.type);
        },
        hidden: function (e) {
            console.log(e.type);
        },
        view: function (e) {
            console.log(e.type);
        },
        viewed: function (e) {
            console.log(e.type);
        }
    };

    function toggleButtons(mode) {
        if (/modal|inline|none/.test(mode)) {
            $buttons
                .find('button[data-enable]')
                .prop('disabled', true)
                .filter('[data-enable*="' + mode + '"]')
                .prop('disabled', false);
        }
    }

    $images.on({
        ready: function (e) {
            console.log(e.type);
        },
        show: function (e) {
            console.log(e.type);
        },
        shown: function (e) {
            console.log(e.type);
        },
        hide: function (e) {
            console.log(e.type);
        },
        hidden: function (e) {
            console.log(e.type);
        },
        view: function (e) {
            console.log(e.type);
        },
        viewed: function (e) {
            console.log(e.type);
        }
    }).viewer(options);

    toggleButtons(options.inline ? 'inline' : 'modal');

    $toggles.on('change', 'input', function () {
        var $input = $(this);
        var name = $input.attr('name');

        options[name] = name === 'inline' ? $input.data('value') : $input.prop('checked');
        $images.viewer('destroy').viewer(options);
        toggleButtons(options.inline ? 'inline' : 'modal');
    });

    $buttons.on('click', 'button', function () {
        var data = $(this).data();
        var args = data.arguments || [];

        if (data.method) {
            if (data.target) {
                $images.viewer(data.method, $(data.target).val());
            } else {
                $images.viewer(data.method, args[0], args[1]);
            }

            switch (data.method) {
                case 'scaleX':
                case 'scaleY':
                    args[0] = -args[0];
                    break;

                case 'destroy':
                    toggleButtons('none');
                    break;
            }
        }
    });

    $('[data-toggle="tooltip"]').tooltip();
});

function imgviewerfun(picclass, btnclass) {
    var console = window.console || { log: function () { } };
    var $images = $('.' + picclass);
    var $toggles = $('.docs-toggles');
    var $buttons = $('.' + btnclass);
    var options = {
        // inline: true,
        url: 'data-original',
        toolbar: false,
        tooltip: false,
        title: false,
        navbar: false,
        ready: function (e) {
        },
        show: function (e) {
        },
        shown: function (e) {
        },
        hide: function (e) {
        },
        hidden: function (e) {
        },
        view: function (e) {
        },
        viewed: function (e) {
        }
    };

    function toggleButtons(mode) {
        if (/modal|inline|none/.test(mode)) {
            $buttons
                .find('button[data-enable]')
                .prop('disabled', true)
                .filter('[data-enable*="' + mode + '"]')
                .prop('disabled', false);
        }
    }

    $images.on({
        ready: function (e) {
        },
        show: function (e) {
        },
        shown: function (e) {
        },
        hide: function (e) {
        },
        hidden: function (e) {
        },
        view: function (e) {
        },
        viewed: function (e) {
        }
    }).viewer(options);

    toggleButtons(options.inline ? 'inline' : 'modal');

    // $toggles.on('change', 'input', function () {
    //     var $input = $(this);
    //     var name = $input.attr('name');

    //     options[name] = name === 'inline' ? $input.data('value') : $input.prop('checked');
    //     $images.viewer('destroy').viewer(options);
    //     toggleButtons(options.inline ? 'inline' : 'modal');
    // });

    $buttons.on('click', 'button', function () {
        var data = {
            enable: 'modal',
            method: 'show'
        }
        var args = [];

        if (btnclass === 'docs-buttons-originalFile') {
            var origname = $('#OrigFileName').val();
            var ext = origname.substring(origname.lastIndexOf('.'));
            if (ext === '.pdf' || ext === '.PDF' || ext === 'pdf' || ext === 'PDF') {
                return;
            }
        }

        if (btnclass === 'docs-buttons-proofFile') {
            var proofname = $('#ProofFileName').val();
            var ext = proofname.substring(proofname.lastIndexOf('.'));
            if (ext === '.pdf' || ext === '.PDF' || ext === 'pdf' || ext === 'PDF') {
                return;
            }
        }

        if (btnclass === 'docs-buttons-finalFile') {
            var finalname = $('#FinalFileName').val();
            var ext = finalname.substring(finalname.lastIndexOf('.'));
            if (ext === '.pdf' || ext === '.PDF' || ext === 'pdf' || ext === 'PDF') {
                return;
            }
        }

        if (data.method) {
            if (data.target) {
                $images.viewer(data.method, $(data.target).val());
            } else {
                $images.viewer(data.method, args[0], args[1]);
            }

            switch (data.method) {
                case 'scaleX':
                case 'scaleY':
                    args[0] = -args[0];
                    break;

                case 'destroy':
                    toggleButtons('none');
                    break;
            }
        }
    });

    $('[data-toggle="tooltip"]').tooltip();
}

