import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { ProductionViewtabService } from './services/production-viewtab.service';
import { IncompleteOrderService } from './services/incomplete-order.service';
import { environment } from '../environments/environment';
import { GlobalClass } from './GlobalClass';
import * as alasql from 'alasql';

@Component({
  selector: 'asi-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  productionForm: FormGroup;

  mediaAssetsList = [];
  issueDatesList = [];
  adTypesList = [];
  repsList = [];

  adTypesModel: any = {};

  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  selectedMediaAsset = '';
  selectedIssueDate = '';
  selectedAdType = '';
  selectedRep = '';

  isproductionViewFormSubmitted = false;
  inCompleteTab = true;
  completeTab = false;
  pickUpFilesTab = false;
  inCompleteDisable = false;
  completeDisable = false;
  pickupFilesDisable = false;
  isLoading = true;

  changeDropdown = 0;
  pageFraction = 0;
  totalPageFraction = 0;

  constructor(private formBuilder: FormBuilder,
    private productionViewtabService: ProductionViewtabService,
    private incompleteOrderService: IncompleteOrderService,
    private _globalClass: GlobalClass) { }

  ngOnInit() {
    const url = window.location.href;
    if (url.search('InComplete') > 1) {
      this.setInCompleteOrderTabActive();
    } else if (url.search('Complete') > 1) {
      this.setCompleteOrderTabActive();
    } else if (url.search('PickUpFiles') > 1) {
      this.setPickUpFilesTabActive();
    } else {
      this.setInCompleteOrderTabActive();
    }
    this.productionForm = this.formBuilder.group({
      'MediaAssetId': [null, Validators.required],
      'IssueDateId': [null, Validators.required],
      'AdTypeId': [null],
      'RepId': [null],
    });

    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      console.log(error);
    }

    this.getDropDownDataInitially();
  }

  // Tab click function
  setInCompleteOrderTabActive() {
    let url = window.location.href;

    if (url.indexOf('#') > -1) {
      url = url.substring(0, url.lastIndexOf('#'));
      // do nothing
    }

    location.replace(url + '#/productionViewTab/InComplete');
    try {
      this.inCompleteTab = true;
      this.completeTab = false;
      this.pickUpFilesTab = false;
      this.inCompleteDisable = false;
      this.completeDisable = false;
      this.pickupFilesDisable = false;
      (<HTMLSelectElement>document.getElementById('ddlpvExport')).selectedIndex = 0;
    } catch (error) {
      console.log(error);
    }
  }

  setCompleteOrderTabActive() {
    let url = window.location.href;

    if (url.indexOf('#') > -1) {
      url = url.substring(0, url.lastIndexOf('#'));
      // do nothing
    }

    location.replace(url + '#/productionViewTab/Complete');
    try {
      this.inCompleteTab = false;
      this.completeTab = true;
      this.pickUpFilesTab = false;
      this.inCompleteDisable = false;
      this.completeDisable = false;
      this.pickupFilesDisable = false;
      (<HTMLSelectElement>document.getElementById('ddlpvExport')).selectedIndex = 0;
    } catch (error) {
      console.log(error);
    }
  }

  setPickUpFilesTabActive() {
    let url = window.location.href;

    if (url.indexOf('#') > -1) {
      url = url.substring(0, url.lastIndexOf('#'));
      // do nothing
    }

    location.replace(url + '#/productionViewTab/PickUpFiles');
    try {
      this.inCompleteTab = false;
      this.completeTab = false;
      this.pickUpFilesTab = true;
      this.inCompleteDisable = false;
      this.completeDisable = false;
      this.pickupFilesDisable = false;
      (<HTMLSelectElement>document.getElementById('ddlpvExport')).selectedIndex = 0;
    } catch (error) {
      console.log(error);
    }
  }

  // ---- List ------
  getAllMediaAssets() {
    try {
      this.productionViewtabService.GetAllMediaAssets().subscribe(result => {
        if (result.StatusCode === 1) {
          this.mediaAssetsList = result.Data;
        } else {
          this.mediaAssetsList = [];
        }
      }, error => {
        this.isLoading = false;
        console.log(error);
      });
    } catch (error) {
      console.log(error);
    }
  }

  getAllIssueDates() {
    try {
      this.productionViewtabService.GetAllIssueDates().subscribe(result => {
        if (result.StatusCode === 1) {
          const issueDate = result.Data;
          const orderByIssueDate = alasql('SELECT * FROM ? AS add order by CoverDate desc', [issueDate]);
          this.issueDatesList = orderByIssueDate;
        } else {
          this.issueDatesList = [];
        }
      }, error => {
        this.isLoading = false;
        console.log(error);
      });
    } catch (error) {
      console.log(error);
    }
  }

  getAllAdType() {
    try {
      this.productionViewtabService.GetMediaOrdersAdType().subscribe(result => {
        if (result.StatusCode === 1) {
          this.adTypesList = result.Data;
        } else {
          this.adTypesList = [];
        }
      }, error => {
        this.isLoading = false;
        console.log(error);
      });
    } catch (error) {
      console.log(error);
    }
  }

  getAllReps() {
    try {
      this.productionViewtabService.GetAllReps().subscribe(result => {
        if (result.StatusCode === 1) {
          this.repsList = result.Data;
        } else {
          this.repsList = [];
        }
      }, error => {
        this.isLoading = false;
        console.log(error);
      });
    } catch (error) {
      console.log(error);
    }
  }

  getDropDownDataInitially() {
    try {
      this.incompleteOrderService.getDataForDropDownInitially().subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data != null) {
            if (result.Data.length > 0) {
              const Data = result.Data;
              this.fillDropDownInitially(Data);
            } else {
              this.resetSelectList();
            }
          } else {
            this.resetSelectList();
          }
        } else {
          this.resetSelectList();
        }
      }, error => {
        console.log(error);
      });
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  fillDropDownInitially(objData) {
    try {
      if (objData.length > 0) {
        const adTypes = objData[0].AdTypes;
        const IssueDates = objData[0].IssueDates;
        const MediaAssets = objData[0].MediaAssets;
        const Reps = objData[0].Reps;
        if (MediaAssets.length > 0) {
          this.mediaAssetsList = alasql('SELECT * FROM ? AS add ORDER BY MediaAssetName ASC', [MediaAssets]);
        } else {
          this.mediaAssetsList = [];
        }

        if (IssueDates.length > 0) {
          this.issueDatesList = alasql('SELECT * FROM ? AS add ORDER BY CoverDate desc', [IssueDates]);
        } else {
          this.issueDatesList = [];
        }

        if (adTypes.length > 0) {
          this.adTypesList = alasql('SELECT * FROM ? AS add ORDER BY AdTypeName ASC', [adTypes]);
        } else {
          this.adTypesList = [];
        }

        if (Reps.length > 0) {
          this.repsList = alasql('SELECT * FROM ? AS add ORDER BY RepName ASC', [Reps]);
        } else {
          this.repsList = [];
        }

      } else {
        this.mediaAssetsList = [];
        this.issueDatesList = [];
        this.adTypesList = [];
        this.repsList = [];
      }
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  getOrderByCustomFilterOption() {
    try {
      this.isproductionViewFormSubmitted = true;
      if (this.productionForm.valid) {
        if (this.inCompleteTab === true) {
          this.productionViewtabService.send_OrderCustomFilterOption('getOrderByCustomFilterOption');
          // this.customFilter.emit(true);
          this.totalPageFraction = this._globalClass.getPageFraction();
        } else if (this.completeTab === true) {
          this.productionViewtabService.send_OrderCompleteFilterOption('getCompleteOrderByCustomFilterOption');
          this.totalPageFraction = this._globalClass.getPageFraction();
        } else if (this.pickUpFilesTab === true) {
          this.productionViewtabService.send_OrderPickupFilterOption('getPickUpFilesOrderByCustomFilterOption');
          this.totalPageFraction = this._globalClass.getPageFraction();
        }
      } else {
        return;
      }
    } catch (error) {
      console.log(error);
    }
  }

  onChangeMediaAsset() {
    try {
      const dropDown = this.productionForm.controls['MediaAssetId'].value;
      const Id = dropDown;
      this.selectedMediaAsset = Id;
      this.changeDropdown = 0;
      if (Id !== undefined && Id !== '' && Id !== '0' && Id != null) {
        this.productionForm.controls['IssueDateId'].setValue(null);
        this.productionForm.controls['AdTypeId'].setValue(null);
        this.productionForm.controls['RepId'].setValue(null);
        this.getDataOnChangeofDropdown();
      } else if (Id === undefined || Id === '' || Id === '0' || Id === null) {
        this.productionForm.controls['MediaAssetId'].setValue(null);
        this.productionForm.controls['IssueDateId'].setValue(null);
        this.productionForm.controls['AdTypeId'].setValue(null);
        this.productionForm.controls['RepId'].setValue(null);
        // this.getAllIssueDates();
        // this.getAllAdType();
        // this.GetAllReps();
        this.getDropDownDataInitially();
      }
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  onChangeIssueDate() {
    try {
      const dropDown = this.productionForm.controls['IssueDateId'].value;
      const Id = dropDown;
      this.selectedIssueDate = Id;
      this.changeDropdown = 1;
      if (Id !== undefined && Id !== '' && Id !== '0' && Id != null) {
        this.productionForm.controls['AdTypeId'].setValue(null);
        this.productionForm.controls['RepId'].setValue(null);
        this.getDataOnChangeofDropdown();
      }
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  onChangeAdType() {
    try {
      const dropDown = this.productionForm.controls['AdTypeId'].value;
      const Id = dropDown;
      this.selectedAdType = Id;
      this.changeDropdown = 2;
      if (Id !== undefined && Id !== '' && Id !== '0' && Id != null) {
        this.productionForm.controls['RepId'].setValue(null);
        this.getDataOnChangeofDropdown();
      }
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  onChangeReps() {
    try {
      const dropDown = this.productionForm.controls['RepId'].value;
      const Id = dropDown;
      this.selectedRep = Id;
      this.changeDropdown = 3;
      if (Id !== undefined && Id !== '' && Id !== '0' && Id != null) {
        // this.changeSelect2DropDownColor('s2id_ddlReps', Id);
      } else {
        // this.changeSelect2DropDownColor('s2id_ddlReps', '');
      }
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  getDataOnChangeofDropdown() {
    try {
      const data = {
        MediaAssetId: this.productionForm.controls['MediaAssetId'].value,
        IssueDateId: this.productionForm.controls['IssueDateId'].value,
        AdTypeId: this.productionForm.controls['AdTypeId'].value,
        RepId: this.productionForm.controls['RepId'].value,
      };

      this.isLoading = true;
      this.incompleteOrderService.getDataForInCompleteDropDown(data).subscribe(result => {
        if (result.StatusCode === 1) {
          this.isLoading = false;
          if (result.Data != null) {
            if (result.Data.length > 0) {

              const apiData = result.Data;
              const mAssets = apiData[0].MediaAssets;
              const iDates = apiData[0].IssueDates;
              const reps = apiData[0].Reps;
              const aTypes = apiData[0].AdTypes;

              // set Media Asset Dropdown
              if (mAssets.length > 0) {
                if (this.selectedMediaAsset !== '' && this.selectedMediaAsset !== undefined && this.selectedMediaAsset !== '0') {
                  this.productionForm.controls['MediaAssetId'].setValue(this.selectedMediaAsset);
                } else {
                  this.productionForm.controls['MediaAssetId'].setValue(null);
                }
              } else {
                this.productionForm.controls['MediaAssetId'].setValue(this.selectedMediaAsset);
              }
              const issuedate = iDates;

              // set Issue Date Dropdown
              if (this.changeDropdown === 0) {
                if (iDates.length > 0) {
                  // $scope.IssueDates = iDates;
                  const orderByIssueDate = alasql('SELECT * FROM ? AS add order by CoverDate desc', [issuedate]);
                  this.issueDatesList = orderByIssueDate;
                  // tslint:disable-next-line:radix
                  const issueDate = parseInt(this.selectedIssueDate);
                  if (issueDate > 0) {
                    this.selectedIssueDate = '';
                  }
                  if (this.selectedIssueDate !== '' && this.selectedIssueDate !== undefined
                    && this.selectedIssueDate !== '0') {
                    this.productionForm.controls['IssueDateId'].setValue(this.selectedIssueDate);
                  } else {
                    this.productionForm.controls['IssueDateId'].setValue(null);
                  }
                } else {
                  this.issueDatesList = [];
                  this.productionForm.controls['IssueDateId'].setValue(null);
                }
              }

              // set AdType Dropdown
              if (this.changeDropdown === 0 || this.changeDropdown === 1) {
                if (aTypes.length > 0) {
                  this.adTypesList = aTypes;
                  // tslint:disable-next-line:radix
                  const adtype = parseInt(this.selectedAdType);
                  if (adtype > 0) {
                    this.selectedAdType = '';
                  }
                  if (this.selectedAdType !== '' && this.selectedAdType !== undefined && this.selectedAdType !== '0') {
                    this.productionForm.controls['AdTypeId'].setValue(this.selectedAdType);
                  } else {
                    this.productionForm.controls['AdTypeId'].setValue(null);
                  }
                } else {
                  this.adTypesList = [];
                  this.productionForm.controls['AdTypeId'].setValue(null);
                }
              }

              // set Rep Dropdown
              if (this.changeDropdown === 0 || this.changeDropdown === 1 || this.changeDropdown === 2) {
                if (reps.length > 0) {
                  this.repsList = reps;
                  // tslint:disable-next-line:radix
                  const rep = parseInt(this.selectedRep);
                  if (rep > 0) {
                    this.selectedRep = null;
                  }
                  if (this.selectedRep !== '' && this.selectedRep !== undefined && this.selectedRep !== '0') {
                    this.productionForm.controls['RepId'].setValue(this.selectedRep);
                  } else {
                    this.productionForm.controls['RepId'].setValue(null);
                  }
                } else {
                  this.repsList = [];
                  this.productionForm.controls['RepId'].setValue(null);
                }
              }

            }
          }
        } else {
          this.isLoading = false;
        }
      }, error => {
        this.isLoading = false;
        console.log(error);
      });
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  exportToFile() {
    const file = (<HTMLSelectElement>document.getElementById('ddlpvExport')).value;
    if (file === 'Excel') {
      if (this.inCompleteTab === true) {
        this.productionViewtabService.send_CustomExport('Excel');
      } else if (this.completeTab === true) {
        this.productionViewtabService.send_CompleteExport('Excel');
      } else if (this.pickUpFilesTab === true) {
        this.productionViewtabService.send_PickupExport('Excel');
      }
    } else if (file === 'PDF') {
      if (this.inCompleteTab === true) {
        this.productionViewtabService.send_CustomExport('PDF');
      } else if (this.completeTab === true) {
        this.productionViewtabService.send_CompleteExport('PDF');
      } else if (this.pickUpFilesTab === true) {
        this.productionViewtabService.send_PickupExport('PDF');
      }
    }
  }

  resetSelectList() {
    this.mediaAssetsList = [];
    this.issueDatesList = [];
    this.adTypesList = [];
    this.repsList = [];
  }

}

