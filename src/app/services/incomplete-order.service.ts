import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IncompleteOrderService {

  baseAPI = environment.ApiBaseURL;
  websiteRoot = environment.websiteRoot;

  issueDateApi = this.baseAPI + 'IssueDate';
  inCompleteMediaOrderApi = this.baseAPI + 'IncompleteMediaOrder';
  ordersNotInBuyerApi = this.baseAPI + 'MediaOrder/GetOrdersNotInBuyer';
  byMediaOrderApi = this.baseAPI + 'IncompleteMediaOrder/GetByMediaOrder';
  productionStatusApi = this.baseAPI + 'ProductionStatus';
  positionApi = this.baseAPI + 'Position';
  separationApi = this.baseAPI + 'Separation';
  productionStageApi = this.baseAPI + 'ProductionStage';
  mediaOrderProductionApi = this.baseAPI + 'MediaOrderProductionDetails';

  public headers: HttpHeaders;
  public sfheaders: HttpHeaders;
  public headers_token: HttpHeaders;
  authToken = environment.token;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });
    this.sfheaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'RequestVerificationToken': this.authToken
    });
  }

  getordersnotinbuyer(buyId, adid): Observable<any> {
    return this.http.get(this.ordersNotInBuyerApi + '/' + buyId + '/' + adid);
  }

  getByMediaOrderApi(mOrderId): Observable<any> {
    return this.http.get(this.byMediaOrderApi + '/' + mOrderId);
  }

  getAllProductionStatus(): Observable<any> {
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    return this.http.get(this.productionStatusApi, { headers: this.headers_token });
  }

  getByMediaAsset(mediaAssetId): Observable<any> {
    return this.http.get(this.issueDateApi + '/GetByMediaAsset/' + mediaAssetId);
  }

  getAllPosition(): Observable<any> {
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    return this.http.get(this.positionApi, { headers: this.headers_token });
  }

  getAllSeparation(): Observable<any> {
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    return this.http.get(this.separationApi, { headers: this.headers_token });
  }

  getAllProductionStage(): Observable<any> {
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.get(this.productionStageApi, { headers: this.headers_token });
  }

  getProductionDropdownInitially(): Observable<any> {
    return this.http.get(this.inCompleteMediaOrderApi + '/FillProductionDropdownInitially', { headers: this.sfheaders });
  }

  getDataForDropDownInitially(): Observable<any> {
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });
    return this.http.get(this.inCompleteMediaOrderApi + '/FillFilterDropdownInitially', { headers: this.headers_token });
  }

  setOrderProductionStatusToComplete(mOrderId): Observable<any> {
    return this.http.get(this.mediaOrderProductionApi + '/UpdateMediaOrderProductionStatusComplete/' + mOrderId,
      { headers: this.sfheaders });
  }

  getAllAdvertiser1(Offset, websiteroot): Observable<any> {
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });
    return this.http.get(websiteroot + '/api/Party?limit=500&Offset=' + Offset, { headers: this.headers_token });
  }

  getAdvertiserDetail_ByAdvertiserId(Offset, websiteroot,advertierId): Observable<any> {
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });
    return this.http.get(websiteroot + '/api/Party?limit=500&Id=eq:'+advertierId+'&Offset=' + Offset, { headers: this.headers_token });
  }

  getAdvertiserDetail_ByAdvertiserName(Offset, websiteroot,advertierName): Observable<any> {
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });
    if(advertierName.includes('&'))
      {
        let arrAdvertiserorAgencyName = advertierName.split("&");
        if(arrAdvertiserorAgencyName.length>0)
        advertierName = arrAdvertiserorAgencyName[0];
      }
    return this.http.get(websiteroot + '/api/Party?limit=500&OrganizationName=contains:'+advertierName+'&Offset=' + Offset, { headers: this.headers_token });
  }


  getIssueDate(issueDates: any): Observable<any> {
    const body = new HttpParams().set('issueDates', issueDates).toString();
    return this.http.post(this.issueDateApi + '/GetByMediaAssetAndDaterange', body, { headers: this.headers });
  }

  getOrderByCustomFilterOption(data: any, authToken): Observable<any> {
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': authToken
    });
    const body = JSON.stringify(data);
    // const body = new HttpParams()
    //   .set('MediaAssetId', data.MediaAssetId)
    //   .set('IssueDateId', data.IssueDateId)
    //   .set('AdTypeId', data.AdTypeId)
    //   .set('RepId', data.RepId)
    //   .set('IssueDateId', data.IssueDateId).toString();
    return this.http.post(this.inCompleteMediaOrderApi + '/GetIncompleteOrders', body, { headers: this.headers_token });
  }

  getDataForInCompleteDropDown(data: any): Observable<any> {
    const body = new HttpParams()
      .set('MediaAssetId', data.MediaAssetId)
      .set('IssueDateId', data.IssueDateId)
      .set('AdTypeId', data.AdTypeId)
      .set('RepId', data.RepId).toString();
    return this.http.post(this.inCompleteMediaOrderApi + '/FillDropdownForIncompleteOrder', body, { headers: this.sfheaders });
  }

  updateProduction(InCompleteOrder): Observable<any> {

    const body = JSON.stringify({
      'NewPickupInd': InCompleteOrder.NewPickupInd,
      'CreateExpectedInd': InCompleteOrder.CreateExpectedInd,
      'ChangesInd': InCompleteOrder.ChangesInd,
      'OnHandInd': InCompleteOrder.OnHandInd,

      'AdvertiserAgencyInd': InCompleteOrder.AdvertiserAgencyInd,
      'ProductionStatusId': InCompleteOrder.ProductionStatusId,
      'SeparationId': InCompleteOrder.SeparationId,
      'PositionId': InCompleteOrder.PositionId,
      'MaterialContactId': InCompleteOrder.MaterialContactId,
      'PickupMediaOrderId': InCompleteOrder.PickupMediaOrderId,

      'MaterialExpectedDate': InCompleteOrder.MaterialExpectedDate,
      'TrackingNumber': InCompleteOrder.TrackingNumber,
      'ClassifiedText': InCompleteOrder.ClassifiedText,
      'HeadLine': InCompleteOrder.HeadLine,
      'TearSheets': InCompleteOrder.TearSheets,
      'PageNumber': InCompleteOrder.PageNumber,
      'ProductionComment': InCompleteOrder.ProductionComment,

      'MediaOrderProductionDetailId': InCompleteOrder.MediaOrderProductionDetailId,
      'WebAdUrl': InCompleteOrder.WebAdUrl,

      'MediaOrderId': InCompleteOrder.MediaOrderId,
      // 'ProductionStatusName': InCompleteOrder.ProductionStatusName,
      'Completed': InCompleteOrder.Completed,

      'ProductionStages': InCompleteOrder.ProductionStages,

      'ProofFile': InCompleteOrder.ProofFile,
      'ProofFileName': InCompleteOrder.ProofFileName,
      'ProofFileExtension': InCompleteOrder.ProofFileExtension,

      'FinalFile': InCompleteOrder.FinalFile,
      'FinalFileName': InCompleteOrder.FinalFileName,
      'FinalFileExtension': InCompleteOrder.FinalFileExtension,

      'OriginalFile': InCompleteOrder.OriginalFile,
      'OrigFileName': InCompleteOrder.OrigFileName,
      'OriginalFileExtension': InCompleteOrder.OriginalFileExtension
    });
console.log(body);
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': 'token'
    });

    return this.http.post(this.inCompleteMediaOrderApi, body, { headers: this.headers_token });
  }
}
