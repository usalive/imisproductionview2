import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PickupFilesOrderService {
  headers_token: HttpHeaders;

  baseAPI = environment.ApiBaseURL;
  pickUpMediaOrderApi = this.baseAPI + 'PickUpFilesMediaOrder';
  inComOrderMediaApi = this.baseAPI + 'IncompleteMediaOrder';
  mediaOrderProductionApi = this.baseAPI + 'MediaOrderProductionDetails';


  public headers: HttpHeaders;
  public sfheaders: HttpHeaders;
  authToken = environment.token;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });
    this.sfheaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'RequestVerificationToken': this.authToken
    });
  }

  getInCompleteOrderByMediaOrderId(mOrderId): Observable<any> {
    return this.http.get(this.inComOrderMediaApi + '/GetByMediaOrder/' + mOrderId, { headers: this.sfheaders });
  }

  setOrderProductionStatusToInComplete(mOrderId): Observable<any> {
    return this.http.get(this.mediaOrderProductionApi + '/UpdateMediaOrderProductionStatusComplete/' + mOrderId,
      { headers: this.sfheaders });
  }

  getPickUpFilesOrderByCustomFilterOption(data: any): Observable<any> {
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });
    const body = JSON.stringify(data);
    // const body = new HttpParams()
    //   .set('MediaAssetId', data.MediaAssetId)
    //   .set('IssueDateId', data.IssueDateId)
    //   .set('AdTypeId', data.AdTypeId)
    //   .set('RepId', data.RepId)
    //   .set('IssueDateId', data.IssueDateId).toString();
    return this.http.post(this.pickUpMediaOrderApi + '/GetPickUpFilesOrders', body, { headers: this.headers_token });
  }

  getAdvertiserDetail_ByAdvertiserId(Offset, websiteroot,advertierId): Observable<any> {
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });
    return this.http.get(websiteroot + '/api/Party?limit=500&Id=eq:'+advertierId+'&Offset=' + Offset, { headers: this.headers_token });
  }

  getAdvertiserDetail_ByAdvertiserName(Offset, websiteroot,advertierName): Observable<any> {
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });
    if(advertierName.includes('&'))
      {
        let arrAdvertiserorAgencyName = advertierName.split("&");
        if(arrAdvertiserorAgencyName.length>0)
        advertierName = arrAdvertiserorAgencyName[0];
      }
    return this.http.get(websiteroot + '/api/Party?limit=500&OrganizationName=contains:'+advertierName+'&Offset=' + Offset, { headers: this.headers_token });
  }

  updateProduction(InCompleteOrder): Observable<any> {
    const body = JSON.stringify({
      'NewPickupInd': InCompleteOrder.NewPickupInd,
      'CreateExpectedInd': InCompleteOrder.CreateExpectedInd,
      'ChangesInd': InCompleteOrder.ChangesInd,
      'OnHandInd': InCompleteOrder.OnHandInd,

      'AdvertiserAgencyInd': InCompleteOrder.AdvertiserAgencyInd,
      'ProductionStatusId': InCompleteOrder.ProductionStatusId,
      'SeparationId': InCompleteOrder.SeparationId,
      'PositionId': InCompleteOrder.PositionId,
      'MaterialContactId': InCompleteOrder.MaterialContactId,
      'PickupMediaOrderId': InCompleteOrder.PickupMediaOrderId,

      'MaterialExpectedDate': InCompleteOrder.MaterialExpectedDate,
      'TrackingNumber': InCompleteOrder.TrackingNumber,
      'ClassifiedText': InCompleteOrder.ClassifiedText,
      'HeadLine': InCompleteOrder.HeadLine,
      'TearSheets': InCompleteOrder.TearSheets,
      'PageNumber': InCompleteOrder.PageNumber,
      'ProductionComment': InCompleteOrder.ProductionComment,

      'MediaOrderProductionDetailId': InCompleteOrder.MediaOrderProductionDetailId,
      'WebAdUrl': InCompleteOrder.WebAdUrl,

      'MediaOrderId': InCompleteOrder.MediaOrderId,
      // 'ProductionStatusName': InCompleteOrder.ProductionStatusName,
      'Completed': InCompleteOrder.Completed,

      'ProductionStages': InCompleteOrder.ProductionStages,

      'ProofFile': InCompleteOrder.ProofFile,
      'ProofFileName': InCompleteOrder.ProofFileName,
      'ProofFileExtension': InCompleteOrder.ProofFileExtension,

      'FinalFile': InCompleteOrder.FinalFile,
      'FinalFileName': InCompleteOrder.FinalFileName,
      'FinalFileExtension': InCompleteOrder.FinalFileExtension,

      'OriginalFile': InCompleteOrder.OriginalFile,
      'OrigFileName': InCompleteOrder.OrigFileName,
      'OriginalFileExtension': InCompleteOrder.OriginalFileExtension
    });

    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': 'token'
    });
    return this.http.post(this.inComOrderMediaApi, body, { headers: this.headers_token });
  }
}
