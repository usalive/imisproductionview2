import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductionViewtabService {

  baseAPI = environment.ApiBaseURL;
  mediaAssetApi = this.baseAPI + 'MediaAsset';
  issueDateApi = this.baseAPI + 'issuedate';
  adTypeAPI = this.baseAPI + 'adtype';
  repsAPI = this.baseAPI + 'reps';

  public headers: HttpHeaders;
  public sfheaders: HttpHeaders;
  authToken = environment.token;

  private subjectForCustomFilterOrder = new Subject<any>();
  private subjectForCompleteFilterOrder = new Subject<any>();
  private subjectForPickupFilterOrder = new Subject<any>();

  private subjectForCustomExport = new Subject<any>();
  private subjectForCompleteExport = new Subject<any>();
  private subjectForPickupFileExport = new Subject<any>();

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });
    this.sfheaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });
  }

  GetAllMediaAssets(): Observable<any> {
    return this.http.get(this.mediaAssetApi);
  }

  GetAllIssueDates(): Observable<any> {
    return this.http.get(this.issueDateApi);
  }

  GetMediaOrdersAdType(): Observable<any> {
    return this.http.get(this.adTypeAPI, { headers: this.sfheaders });
  }

  GetAllReps(): Observable<any> {
    return this.http.get(this.repsAPI);
  }




  /* ---------- InComplete Order Observable function ---------- */
  send_OrderCustomFilterOption(message: string) {
    this.subjectForCustomFilterOrder.next({ text: message });
  }
  get_OrderCustomFilterOption(): Observable<any> {
    return this.subjectForCustomFilterOrder.asObservable();
  }

  send_CustomExport(message: string) {
    this.subjectForCustomExport.next({ text: message });
  }
  get_CustomExport(): Observable<any> {
    return this.subjectForCustomExport.asObservable();
  }



  /* ---------- Complete Order Observable function ---------- */
  send_OrderCompleteFilterOption(message: string) {
    this.subjectForCompleteFilterOrder.next({ text: message });
  }
  get_OrderCompleteFilterOption(): Observable<any> {
    return this.subjectForCompleteFilterOrder.asObservable();
  }

  send_CompleteExport(message: string) {
    this.subjectForCompleteExport.next({ text: message });
  }
  get_CompleteExport(): Observable<any> {
    return this.subjectForCompleteExport.asObservable();
  }


  /* ---------- Pickup file Order Observable function ---------- */
  send_OrderPickupFilterOption(message: string) {
    this.subjectForPickupFilterOrder.next({ text: message });
  }
  get_OrderPickupFilterOption(): Observable<any> {
    return this.subjectForPickupFilterOrder.asObservable();
  }

  send_PickupExport(message: string) {
    this.subjectForPickupFileExport.next({ text: message });
  }
  get_PickupExport(): Observable<any> {
    return this.subjectForPickupFileExport.asObservable();
  }



}
