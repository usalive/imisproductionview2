import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompleteOrderService {
  baseAPI = environment.ApiBaseURL;
  getByMediaOrderApiUrl = this.baseAPI + 'CompleteMediaOrder/GetByMediaOrder';
  mediaOrderProductionApiUrl = this.baseAPI +  'MediaOrderProductionDetails';
  completeMediaOrderApiUrl = this.baseAPI + 'CompleteMediaOrder';

  public headers_token: HttpHeaders;
  authToken = environment.token;

  constructor(private http: HttpClient) { }

  getByMediaOrderApi(mOrderId): Observable<any> {
    return this.http.get(this.getByMediaOrderApiUrl + '/' + mOrderId);
  }

  setOrderProductionStatusToInComplete(mOrderId): Observable<any> {
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });

    return this.http.get(this.mediaOrderProductionApiUrl + '/UpdateMediaOrderProductionStatusInComplete/' + mOrderId,
      { headers: this.headers_token });
  }

  getOrderByCustomFilterOption(data: any): Observable<any> {
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });
    const body = JSON.stringify(data);
    // const body = new HttpParams()
    //   .set('MediaAssetId', data.MediaAssetId)
    //   .set('IssueDateId', data.IssueDateId)
    //   .set('AdTypeId', data.AdTypeId)
    //   .set('RepId', data.RepId)
    //   .set('IssueDateId', data.IssueDateId).toString();
    return this.http.post(this.completeMediaOrderApiUrl + '/GetCompleteOrders', body, { headers: this.headers_token });
  }

  getAdvertiserDetail_ByAdvertiserId(Offset, websiteroot,advertierId): Observable<any> {
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });
    return this.http.get(websiteroot + '/api/Party?limit=500&Id=eq:'+advertierId+'&Offset=' + Offset, { headers: this.headers_token });
  }

  getAdvertiserDetail_ByAdvertiserName(Offset, websiteroot,advertierName): Observable<any> {
    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': this.authToken
    });
    if(advertierName.includes('&'))
      {
        let arrAdvertiserorAgencyName = advertierName.split("&");
        if(arrAdvertiserorAgencyName.length>0)
        advertierName = arrAdvertiserorAgencyName[0];
      }
    return this.http.get(websiteroot + '/api/Party?limit=500&OrganizationName=contains:'+advertierName+'&Offset=' + Offset, { headers: this.headers_token });
  }
  

  updateProduction(completeOrder): Observable<any> {
    const body = JSON.stringify({
      'NewPickupInd': completeOrder.NewPickupInd,
      'CreateExpectedInd': completeOrder.CreateExpectedInd,
      'ChangesInd': completeOrder.ChangesInd,
      'OnHandInd': completeOrder.OnHandInd,

      'AdvertiserAgencyInd': completeOrder.AdvertiserAgencyInd,
      'ProductionStatusId': completeOrder.ProductionStatusId,
      'SeparationId': completeOrder.SeparationId,
      'PositionId': completeOrder.PositionId,
      'MaterialContactId': completeOrder.MaterialContactId,
      'PickupMediaOrderId': completeOrder.PickupMediaOrderId,

      'MaterialExpectedDate': completeOrder.MaterialExpectedDate,
      'TrackingNumber': completeOrder.TrackingNumber,
      'ClassifiedText': completeOrder.ClassifiedText,
      'HeadLine': completeOrder.HeadLine,
      'TearSheets': completeOrder.TearSheets,
      'PageNumber': completeOrder.PageNumber,
      'ProductionComment': completeOrder.ProductionComment,

      'MediaOrderProductionDetailId': completeOrder.MediaOrderProductionDetailId,
      'WebAdUrl': completeOrder.WebAdUrl,

      'MediaOrderId': completeOrder.MediaOrderId,
      // 'ProductionStatusName': completeOrder.ProductionStatusName,
      'Completed': completeOrder.Completed,

      'ProductionStages': completeOrder.ProductionStages,

      'ProofFile': completeOrder.ProofFile,
      'ProofFileName': completeOrder.ProofFileName,
      'ProofFileExtension': completeOrder.ProofFileExtension,

      'FinalFile': completeOrder.FinalFile,
      'FinalFileName': completeOrder.FinalFileName,
      'FinalFileExtension': completeOrder.FinalFileExtension,

      'OriginalFile': completeOrder.OriginalFile,
      'OrigFileName': completeOrder.OrigFileName,
      'OriginalFileExtension': completeOrder.OriginalFileExtension
    });

    this.headers_token = new HttpHeaders({
      'Content-Type': 'application/json',
      'RequestVerificationToken': 'token'
    });


    return this.http.post(this.completeMediaOrderApiUrl, body, { headers: this.headers_token });
  }

}
