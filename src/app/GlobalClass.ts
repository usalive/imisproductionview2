import { Injectable } from '@angular/core';
@Injectable()

export class GlobalClass {
    flag: any = true;

    pageFraction = 0;

    mainPartyAndOrganizationData = [];
    mainAdvertisers = [];
    mainAgencies = [];
    mainBillingToContacts = [];

    rPositions = [];
    rSeparations = [];
    rProductionStatus = [];
    rProductionStages1 = [];

    constructor() { }

    // ---------------------------------
    getflag() {
        return this.flag;
    }

    setflag(_flag) {
        this.flag = _flag;
    }

    removeflag() {
        this.flag = null;
    }

    // ---------------------------------

    getPageFraction() {
        return this.pageFraction;
    }

    setPageFraction(_PageFraction) {
        this.pageFraction = _PageFraction;
    }

    removePageFraction() {
        this.pageFraction = null;
    }

    // ---------------------------------

    getmainPartyAndOrganizationData() {
        return this.mainPartyAndOrganizationData;
    }

    setmainPartyAndOrganizationData(_mainPartyAndOrganizationData) {
        this.mainPartyAndOrganizationData = _mainPartyAndOrganizationData;
    }

    removemainPartyAndOrganizationData() {
        this.mainPartyAndOrganizationData = [];
    }

    // ---------------------------------

    getmainAdvertisers() {
        return this.mainAdvertisers;
    }

    setmainAdvertisers(_mainAdvertisers) {
        this.mainAdvertisers = _mainAdvertisers;
    }

    removemainAdvertisers() {
        this.mainAdvertisers = [];
    }

    // ---------------------------------

    getmainAgencies() {
        return this.mainAgencies;
    }

    setmainAgencies(_mainAgencies) {
        this.mainAgencies = _mainAgencies;
    }

    removemainAgencies() {
        this.mainAgencies = [];
    }

    // ---------------------------------

    getmainBillingToContacts() {
        return this.mainBillingToContacts;
    }

    setmainBillingToContacts(_mainBillingToContacts) {
        this.mainBillingToContacts = _mainBillingToContacts;
    }

    removemainBillingToContacts() {
        this.mainBillingToContacts = [];
    }

    // ---------------------------------

    getrPositions() {
        return this.rPositions;
    }

    setrPositions(_rPositions) {
        this.rPositions = _rPositions;
    }

    removerPositions() {
        this.rPositions = [];
    }

    // ---------------------------------

    getrSeparations() {
        return this.rSeparations;
    }

    setrSeparations(_rSeparations) {
        this.rSeparations = _rSeparations;
    }

    removerSeparations() {
        this.rSeparations = [];
    }

    // ---------------------------------

    getrProductionStatus() {
        return this.rProductionStatus;
    }

    setrProductionStatus(_rProductionStatus) {
        this.rProductionStatus = _rProductionStatus;
    }

    removerProductionStatus() {
        this.rProductionStatus = [];
    }
    // ---------------------------------

    getrProductionStages1() {
        return this.rProductionStages1;
    }

    setrProductionStages1(_rProductionStages1) {
        this.rProductionStages1 = _rProductionStages1;
    }

    removerProductionStages1() {
        this.rProductionStages1 = [];
    }
}
