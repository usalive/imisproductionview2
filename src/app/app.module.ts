import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { BsDatepickerModule } from 'ngx-bootstrap';

import { ProductionViewtabService } from './services/production-viewtab.service';
import { PickupFilesOrderService } from './services/pickup-files-order.service';
import { IncompleteOrderService } from './services/incomplete-order.service';
import { CompleteOrderService } from './services/complete-order.service';
import { SortListPipe } from './Directive/sortlist.pipe';
import { NumberDirective } from './Directive/numbers-only.directive';

import { CompleteOrdertabComponent } from './components/complete-ordertab/complete-ordertab.component';
import { IncompleteOrdertabComponent } from './components/incomplete-ordertab/incomplete-ordertab.component';
import { PickupfilesOrdertabComponent } from './components/pickupfiles-ordertab/pickupfiles-ordertab.component';

import { GlobalClass } from './GlobalClass';

@NgModule({
  declarations: [
    AppComponent,
    CompleteOrdertabComponent,
    IncompleteOrdertabComponent,
    PickupfilesOrdertabComponent,
    SortListPipe,
    NumberDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgSelectModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    ToastrModule.forRoot({
      preventDuplicates: true,
      timeOut: 5000,
      closeButton: true,
      positionClass: 'toast-top-right',
      progressBar: true
    }),
  ],
  providers: [
    ProductionViewtabService,
    PickupFilesOrderService,
    IncompleteOrderService,
    CompleteOrderService,
    GlobalClass
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
