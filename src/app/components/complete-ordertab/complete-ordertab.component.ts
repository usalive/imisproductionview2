import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService, ToastrModule } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import * as alasql from 'alasql';
import { CompleteOrderService } from '../../services/complete-order.service';
import { ProductionViewtabService } from '../../services/production-viewtab.service';
import { IncompleteOrderService } from '../../services/incomplete-order.service';
import { AppComponent } from '../../app.component';
import { environment } from '../../../environments/environment';
import { GlobalClass } from '../../GlobalClass';
import * as Common from '../../common/common';
import { isNullOrUndefined } from 'util';
declare var jsPDF: any;
declare var imgviewerfun: any;

@Component({
  selector: 'asi-complete-ordertab',
  templateUrl: './complete-ordertab.component.html',
  styleUrls: ['./complete-ordertab.component.css']
})
export class CompleteOrdertabComponent implements OnInit, OnDestroy {
  mediaOrderProductionDetailId: any;

  subscriptionForCompleteExport: Subscription;
  subscriptionForCompleteOrder: Subscription;

  completeForm: FormGroup;

  completeOrders: any[] = [];
  completeOrder: any = {};
  mediaAssetsList = [];
  billingToContactsList = [];
  productionStatusList = [];
  separationsList = [];
  positionsList = [];
  productionStages1List = [];
  completeOrder_ProductionStagesList = [];
  issueDatesList = [];

  selectedCheckbox = [];
  selectedMediaOrderIds = [];
  selectedAdvertiserIds = [];
  selectedAgencyIds = [];

  partyAndOrganizationData = [];

  tempArray = [];
  advertisers = [];
  agencies = [];
  personData = [];
  organizationWiseParty = [];

  dataObject: any = {};
  selectedIssueMediaOrder: any[] = [];
  mediaAssetModel: any = {};
  objModelExport: any = {};

  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';

  originalFile = '';
  originalFileImageSrc = '';
  originalFileExtension = '';
  selectedAdvertiserAgencyName='';

  finalFile = null;
  finalFileExtension = '';
  finalFileImageSrc = '';

  proofFile = null;
  proofFileExtension = '';
  proofFileImageSrc = '';

  extensionOriginal = '';
  extensionProof = '';
  extensionFinal = '';

  tempPdfPathForOriginal = '';
  tempPdfPathForProof = '';
  tempPdfPathForFinal = '';

  buttonText = 'Add';

  selectedMediaOrderId = '';
  selectedAdvertiserId = '';
  selectedAgencyId = '';

  buyId = 0;
  updatedBy = '';

  setProductionNoClick = true;
  flag = true;
  dvNew = false;
  dvPickup = true;

  isPdfTrueForProof = false;
  isPdfTrueForfinal = false;
  isPdfTrueForOriginal = false;

  isImgTrueForOriginal = false;
  isImgTrueForfinal = false;
  isImgTrueForProof = false;

  hasNext: boolean;
  offset: number;

  constructor(private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public appComponent: AppComponent,
    private completeOrderService: CompleteOrderService,
    private productionViewtabService: ProductionViewtabService,
    private incompleteOrderService: IncompleteOrderService,
    private _globalClass: GlobalClass) {
    ToastrModule.forRoot();
  }

  ngOnInit() {
    this.completeFormValidation();
    this.getToken();
    try {
      this.completeOrder = {};
      this.completeOrder.ProductionStages = [];
      this.completeOrder.NewPickupInd = '1';
      this.completeOrder.CreateExpectedInd = '0';
      this.completeOrder.AdvertiserAgencyInd = '1';
      this.completeOrder.OriginalFileExtension = '';
    } catch (error) {
      this.hideloader();
      console.log(error);
    }

    this.completeForm.controls['PickupMediaOrderId'].disable();
    this.completeForm.controls['NewPickupInd'].setValue('1');
    this.completeForm.controls['CreateExpectedInd'].setValue('0');
    this.completeForm.controls['AdvertiserAgencyInd'].setValue('1');
    
    this.subscriptionForCompleteOrder = this.productionViewtabService.get_OrderCompleteFilterOption().subscribe(message => {
      if (message.text === 'getCompleteOrderByCustomFilterOption') {
        this.getCompleteOrderByCustomFilterOption();
      }
    });

    this.subscriptionForCompleteExport = this.productionViewtabService.get_CompleteExport().subscribe(message => {
      if (message.text === 'Excel') {
        this.exportCompleteDataToExcel();
      } else if (message.text === 'PDF') {
        this.exportCompleteDataToPdf();
      }
    });

    if (this.appComponent.completeTab === true) {
      const MediaAssetId = this.appComponent.productionForm.controls['MediaAssetId'].value;
      const IssueDateId = this.appComponent.productionForm.controls['IssueDateId'].value;
      const AdTypeId = this.appComponent.productionForm.controls['AdTypeId'].value;
      const RepId = this.appComponent.productionForm.controls['RepId'].value;
      if (!isNullOrUndefined(AdTypeId) && AdTypeId !== 0
        || !isNullOrUndefined(MediaAssetId) && MediaAssetId !== 0
        || !isNullOrUndefined(IssueDateId) && IssueDateId !== 0
        || !isNullOrUndefined(RepId) && RepId !== 0) {
        this.getCompleteOrderByCustomFilterOption();
      }
    }

    this.getProductionDropDownDataInitially();
    //this.getAdvertiserThroughASI();
  }

  getToken() {
    try {
      this.updatedBy = environment.CurrentUserName;
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }
  completeFormValidation() {
    this.completeForm = this.formBuilder.group({
      'NewPickupInd': [null],
      'PickupMediaOrderId': [null],
      'AdvertiserAgencyInd': [null],
      'CreateExpectedInd': [null],
      'MaterialExpectedDate': [null],
      'TrackingNumber': [null],
      'ChangesInd': [null],
      'OnHandInd': [null],
      'OrigFileName': [null],
      'ProofFileName': [null],
      'FinalFileName': [null],
      'WebAdUrl': [null],
      'TearSheets': [null],
      'HeadLine': [null],
      'PageNumber': [null],
      'ProductionComment': [null],
      'MaterialContactId': [null],
      'MediaOrderId': [null],
      'ClassifiedText': [null],
      'SeparationId': [null],
      'PositionId': [null],
      'completeOrderProductionStages': [null],
      'ProductionStatusId': [null],
    });
  }

  getProductionDropDownDataInitially() {
    try {
      this.incompleteOrderService.getProductionDropdownInitially().subscribe(result => {
        if (result.StatusCode === 1) {
          this.hideloader();
          if (result.Data != null) {
            if (result.Data.length > 0) {
              const Data = result.Data;
              this.fillDropDownInitially(Data);
            } else {
              this.productionStatusList = [];
              this.positionsList = [];
              this.separationsList = [];
              this.productionStages1List = [];
            }
          } else {
            this.productionStatusList = [];
            this.positionsList = [];
            this.separationsList = [];
            this.productionStages1List = [];
          }
        } else {
          this.hideloader();
          this.productionStatusList = [];
          this.positionsList = [];
          this.separationsList = [];
          this.productionStages1List = [];
        }
      }, error => {
        this.hideloader();
        console.log(error);
      });
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  fillDropDownInitially(objData) {
    try {
      if (objData.length > 0) {
        const dbproductionStatus = objData[0].ProductionStatus;
        const dbProductionStages1 = objData[0].ProductionStages;
        const dbPositions = objData[0].Positions;
        const dbSeparations = objData[0].Separations;

        if (dbproductionStatus.length > 0) {
          this.productionStatusList = dbproductionStatus;
        } else {
          this.productionStatusList = [];
        }

        if (dbPositions.length > 0) {
          this.positionsList = dbPositions;
        } else {
          this.positionsList = [];
        }

        if (dbSeparations.length > 0) {
          this.separationsList = dbSeparations;
        } else {
          this.separationsList = [];
        }

        if (dbProductionStages1.length > 0) {
          this.productionStages1List = dbProductionStages1;
        } else {
          this.productionStages1List = [];
        }

      } else {
        this.productionStatusList = [];
        this.positionsList = [];
        this.separationsList = [];
        this.productionStages1List = [];
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getAdvertiserThroughASI() {
    try {
      if (this._globalClass.getmainPartyAndOrganizationData().length <= 0
        && this._globalClass.getmainAdvertisers().length <= 0
        && this._globalClass.getmainAgencies().length <= 0
        && this._globalClass.getmainBillingToContacts().length <= 0) {

        this.hasNext = true;
        this.offset = 0;
        this.getAdvertiserData();
      } else {
        this.partyAndOrganizationData = this._globalClass.getmainPartyAndOrganizationData();
        this.advertisers = this._globalClass.getmainAdvertisers();
        this.agencies = this._globalClass.getmainAgencies();
        this.personData = this._globalClass.getmainBillingToContacts();
      }

    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getCompleteOrderByCustomFilterOption() {
    this.selectedIssueMediaOrder = [];

    this.selectedCheckbox = [];
    this.selectedMediaOrderIds = [];
    this.selectedAdvertiserIds = [];
    this.selectedAgencyIds = [];

    this.resetValueChkChange();
    try {
      this.dataObject = {};
      this.dataObject.MediaAssetId = this.appComponent.productionForm.controls['MediaAssetId'].value;
      this.dataObject.IssueDateId = this.appComponent.productionForm.controls['IssueDateId'].value;
      this.dataObject.AdTypeId = this.appComponent.productionForm.controls['AdTypeId'].value;
      this.dataObject.RepId = this.appComponent.productionForm.controls['RepId'].value;

      const obj = {
        AdTypeId: this.dataObject.AdTypeId,
        IssueDateId: this.dataObject.IssueDateId,
        MediaAssetId: this.dataObject.MediaAssetId,
        RepId: this.dataObject.RepId,
      };

      this.showloader();
      this.completeOrderService.getOrderByCustomFilterOption(obj).subscribe(result => {
        this.hideloader();
        this._globalClass.setPageFraction(0);

        if (result.StatusCode === 1) {
          if (result.Data != null) {
            if (result.Data.length > 0) {

              const data = result.Data;
              this.tempArray = [];

              data.forEach((element, key) => {
                this.completeOrder = element;

                const t = this._globalClass.getPageFraction() + element.PageFraction;
                this._globalClass.setPageFraction(t);

                this.appComponent.pageFraction = t;

                let TN = '';
                let MED = '';
                let MediaAssetIssueDate = '';

                if (element.TrackingNumber != null && element.TrackingNumber !== '') {
                  TN = ' | ' + (element.TrackingNumber).toString();
                }

                if (element.MaterialExpectedDate != null && element.MaterialExpectedDate !== '') {
                  MED = ' | ' + Common.getDateInFormat(element.MaterialExpectedDate);
                }

                if (element.PickupIndFromPg != null && element.PickupIndFromPg !== '') {
                  MediaAssetIssueDate = element.PickupIndFromMediaAsset
                    + ' | ' + Common.getDateInFormat(element.PickupIndFromIssueDate)
                    + ' | ' + element.PickupIndFromPg;
                } else if (element.PickupIndFromIssueDate !== null && element.PickupIndFromIssueDate !== ''
                  && element.PickupIndFromIssueDate !== '0001-01-01T00:00:00') {
                  MediaAssetIssueDate = element.PickupIndFromMediaAsset + ' | ' + Common.getDateInFormat(element.PickupIndFromIssueDate);
                } else if (element.PickupIndFromMediaAsset != null && element.PickupIndFromMediaAsset !== '') {
                  MediaAssetIssueDate = element.PickupIndFromMediaAsset;
                }

                if (element.NewPickupInd) {
                  if (element.CreateExpectedInd) {
                    if (element.OnHandInd) {
                      this.completeOrder.MaterialInfo = 'New material we create on hand ' + TN + MED;
                    } else {
                      this.completeOrder.MaterialInfo = 'New material we create ' + TN + MED;
                    }
                  } else {
                    if (element.OnHandInd) {
                      this.completeOrder.MaterialInfo = 'New material expected on hand ' + TN + MED;
                    } else {
                      this.completeOrder.MaterialInfo = 'New material expected ' + TN + MED;
                    }
                  }
                } else if (element.ChangesInd) {
                  this.completeOrder.MaterialInfo = 'Pick-up with changes from ' + MediaAssetIssueDate;
                } else if (MediaAssetIssueDate !== '' && MediaAssetIssueDate != null) {
                  this.completeOrder.MaterialInfo = 'Pick-up from ' + MediaAssetIssueDate;
                } else {
                  this.completeOrder.MaterialInfo = 'Pick-up';
                }

                this.tempArray.push(this.completeOrder);
                this.completeOrder = {};
              });

              this.completeOrders = this.tempArray;
              this.setAdvertiserNameCompleteOrders(this.tempArray);

            } else {
              this.completeOrders = [];
            }
          } else {
            this.completeOrders = [];
          }
        } else if (result.StatusCode === 3) {
          this.completeOrders = [];
        } else {
          this.toastr.error(result.Message, '');
        }
      }, error => {
        this.hideloader();
        console.log(error);
      });
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  setAdvertiserNameCompleteOrders(objOrders) {
    try {
      if (objOrders.length > 0) {
        objOrders.forEach((element, index) => {
          const advertiserId = element.AdvertiserId;
          this.completeOrderService.getAdvertiserDetail_ByAdvertiserId(this.offset, this.websiteRoot,advertiserId).subscribe(result => {
            if (result != null && result !== undefined && result !== '') {
              const ItemData = result.Items.$values;
              const advertiserData=[];
              if (ItemData.length > 0) {
                ItemData.forEach(itemdatavalue => {
                  const type = itemdatavalue.$type;
                  if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                    advertiserData.push(itemdatavalue);
                  }
                  if(advertiserData.length>0)
                  {
                    const objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id = ?', [advertiserData, advertiserId]);
                    if (objAdvertiser.length > 0) {
                      console.log(objAdvertiser);
                      const aName=objAdvertiser[0].OrganizationName;
                      this.completeOrders[index]['AdvertiserName'] = aName;
                    }
                  }
                });
              }
            }
          }, error => {
            console.log(error);
          });
          // const objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id LIKE ?', [this.partyAndOrganizationData, advertiserId]);
          // if (objAdvertiser.length > 0) {
          //   const aName = objAdvertiser[0].Name;
          //   this.completeOrders[index]['AdvertiserName'] = aName;
          // } else {
          //   const aName = '';
          //   this.completeOrders[index]['AdvertiserName'] = aName;
          // }
        });
      } else {
        this.completeOrders = [];
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getAdvertiserData() {
    try {
      this.showloader();
      this.incompleteOrderService.getAllAdvertiser1(this.offset, this.websiteRoot).subscribe(result => {
        if (result !== null && result !== undefined && result !== '') {
          const ItemData = result.Items.$values;
          if (ItemData.length > 0) {

            ItemData.forEach(element => {
              this.partyAndOrganizationData.push(element);
              const type = element.$type;
              if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                this.advertisers.push(element);
                this.agencies.push(element);
              } else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                this.billingToContactsList.push(element);
              }
            });

            this.offset = result.Offset;
            const NextOffset = this.offset + 500;

            if (result.Count === 500) {
              this.offset = NextOffset;
              this.getAdvertiserData();
            } else {
              this.hideloader();
              this._globalClass.setmainPartyAndOrganizationData(this.partyAndOrganizationData);
              this._globalClass.setmainAdvertisers(this.advertisers);
              this._globalClass.setmainAgencies(this.agencies);
              this._globalClass.setmainBillingToContacts(this.billingToContactsList);
            }
          } else {
            this.hideloader();
            this._globalClass.setmainPartyAndOrganizationData(this.partyAndOrganizationData);
            this._globalClass.setmainAdvertisers(this.advertisers);
            this._globalClass.setmainAgencies(this.agencies);
            this._globalClass.setmainBillingToContacts(this.billingToContactsList);
          }
        } else {
          this.hideloader();
          this.advertisers = [];
          this.agencies = [];
          this.billingToContactsList = [];
          this.partyAndOrganizationData = [];
        }
      }, error => {
        this.hideloader();
        console.log(error);
      });
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  checkboxChange(objcompleteOrder) {
    try {
      const qId = objcompleteOrder.IssueDateId;
      const mId = objcompleteOrder.MediaAssetId;
      const mOrderId = objcompleteOrder.MediaOrderId;
      const advertiserId = objcompleteOrder.AdvertiserId;
      const agencyId = objcompleteOrder.BT_ID;
      const buyId = objcompleteOrder.BuyId;
      this.buyId = buyId;

      const idx = this.selectedCheckbox.indexOf(mOrderId);

      if (idx > -1) {

        this.selectedCheckbox.splice(idx, 1);
        this.selectedMediaOrderIds.forEach((element, index) => {
          // tslint:disable-next-line:radix
          if (parseInt(element) === parseInt(mOrderId)) {
            this.selectedMediaOrderIds.splice(index, 1);
            this.selectedAdvertiserIds.splice(index, 1);
            this.selectedAgencyIds.splice(index, 1);
          }
        });

        if (this.selectedCheckbox.length < 1) {
          this.resetValueChkChange();
        }

        if (this.selectedCheckbox.length === 1) {
          this.selectedMediaOrderId = this.selectedMediaOrderIds[0];
          this.selectedAdvertiserId = this.selectedAdvertiserIds[0];
          this.selectedAgencyId = this.selectedAgencyIds[0];
          this.getOrdersNotInBuyer(buyId, this.selectedAdvertiserId);
          const changeBillToValue = this.completeForm.controls['AdvertiserAgencyInd'].value;
          this.getBillToContact(changeBillToValue);
          this.setProductionNoClick = false;
        }

        if (this.selectedCheckbox.length > 1) {
          this.resetValueChkChange();
          this.selectedAdvertiserIds = [];
        }

      } else {
        this.selectedCheckbox.push(mOrderId);
        this.selectedMediaOrderIds.push(mOrderId);
        this.selectedAdvertiserIds.push(advertiserId);
        this.selectedAgencyIds.push(agencyId);

        if (this.selectedCheckbox.length === 1) {
          this.selectedMediaOrderId = this.selectedMediaOrderIds[0];
          this.selectedAdvertiserId = this.selectedAdvertiserIds[0];
          this.selectedAgencyId = this.selectedAgencyIds[0];
          this.getOrdersNotInBuyer(buyId, this.selectedAdvertiserId);
          const changeBillToValue = this.completeForm.controls['AdvertiserAgencyInd'].value;
          this.getBillToContact(changeBillToValue);
          this.setProductionNoClick = false;
        }
      }

      if (this.selectedCheckbox.length > 1) {
        this.resetValueChkChange();
      }

    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  resetValueChkChange() {

    try {
      this.selectedMediaOrderId = '';
      this.selectedAdvertiserId = '';

      this.selectedAgencyId = '';
      this.setProductionNoClick = true;
    } catch (error) {
      this.hideloader();
      console.log(error);
    }

    try {
      this.completeOrder = {};

      this.buyId = 0;

      this.completeForm.reset();

      this.completeForm.controls['NewPickupInd'].setValue('1');
      this.completeForm.controls['CreateExpectedInd'].setValue('0');
      this.completeForm.controls['AdvertiserAgencyInd'].setValue('1');

      this.completeForm.controls['OnHandInd'].setValue(false);
      this.completeForm.controls['ChangesInd'].setValue(false);

      this.completeOrder_ProductionStagesList = [];

      this.originalFile = null;
      this.originalFileImageSrc = null;
      this.originalFileExtension = null;

      this.finalFile = null;
      this.finalFileExtension = null;
      this.finalFileImageSrc = null;

      this.proofFile = null;
      this.proofFileExtension = null;
      this.proofFileImageSrc = null;

      this.tempPdfPathForOriginal = null;
      this.tempPdfPathForProof = null;
      this.tempPdfPathForFinal = null;

      this.buttonText = 'Add';
      this.setProductionNoClick = true;
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  // Get list of orders which is not in buyer
  getOrdersNotInBuyer(buyId, adid) {
    try {
      this.incompleteOrderService.getordersnotinbuyer(buyId, adid).subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data != null) {
            if (result.Data.length > 0) {

              this.tempArray = [];

              result.Data.forEach((element, key) => {
                const issueDate = Common.getDateInFormat(element.IssueDate);
                const pageNo = element.PageNumber == null ? '' : element.PageNumber;
                if (pageNo !== null && pageNo !== undefined && pageNo !== '') {
                  this.mediaAssetModel.MediaAssetIssueDate = element.MediaAssetName + ' | ' + issueDate + ' | ' + pageNo;
                } else {
                  this.mediaAssetModel.MediaAssetIssueDate = element.MediaAssetName + ' | ' + issueDate;
                }
                this.mediaAssetModel.MediaOrderId = element.MediaOrderId;
                this.tempArray.push(this.mediaAssetModel);
                this.mediaAssetModel = {};
              });

              this.mediaAssetsList = this.tempArray;

            } else {
              this.mediaAssetsList = [];
            }
          } else {
            this.mediaAssetsList = [];
          }

        } else if (result.StatusCode === 3) {
          this.mediaAssetsList = [];
        } else {
          this.mediaAssetsList = [];
          this.toastr.error(result.Message, 'Error!');
        }
        this.getMediaOrderProduction(this.selectedMediaOrderId);

      }, error => {
        this.hideloader();
        console.log(error);
      });
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getBillToContact(changeBillToValue) {
    try {
      if (changeBillToValue === '0') {
        changeBillToValue = this.completeForm.controls['AdvertiserAgencyInd'].value;
      }
      const advertiserData=[];
      let id = '0';
      const temp = changeBillToValue;
      if (temp === '1') {
        changeBillToValue = 1;
      } else if (temp === '0') {
        changeBillToValue = 0;
      }

      // tslint:disable-next-line:radix
      if (parseInt(changeBillToValue) === 0 || parseInt(changeBillToValue) === 1) {
        this.billingToContactsList = [];
        // tslint:disable-next-line:radix
        if (parseInt(changeBillToValue) === 1) {
          id = this.selectedAdvertiserId;
          // tslint:disable-next-line:radix
        } else if (parseInt(changeBillToValue) === 0) {
          id = this.selectedAgencyId;
        }
      }
      console.log("selected id:"+ id);
      this.incompleteOrderService.getAdvertiserDetail_ByAdvertiserId(this.offset, this.websiteRoot,id).subscribe(result => {
        if (result != null && result !== undefined && result !== '') {
          const ItemData = result.Items.$values;
          if (ItemData.length > 0) {
            ItemData.forEach(itemdatavalue => {
              const type = itemdatavalue.$type;
              if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                advertiserData.push(itemdatavalue);
              }
              if(advertiserData.length>0)
              {
                const objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id = ?', [advertiserData, id]);
                if (objAdvertiser.length > 0) {
                  this.selectedAdvertiserAgencyName=objAdvertiser[0].OrganizationName;
                  this.organizationWiseParty = [];
                  console.log("Selected Name:"+ this.selectedAdvertiserAgencyName);
                  if(this.selectedAdvertiserAgencyName!=='')
                  {
                    this.completeOrderService.getAdvertiserDetail_ByAdvertiserName(0,this.websiteRoot,this.selectedAdvertiserAgencyName).subscribe(result => {
                      if (result != null && result !== undefined && result !== '') {
                        const ItemData = result.Items.$values;
                        for (const itemdatavalue of ItemData) {
                          const type = itemdatavalue.$type;
                          if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                            this.organizationWiseParty.push(itemdatavalue);
                          }
                        }
                        if(this.organizationWiseParty.length>0)
                        {
                          this.billingToContactsList = this.organizationWiseParty;
                        }
                      } else {
                        this.billingToContactsList = [];
                      }
                    }, error => {
                      console.log(error);
                    });
                  }
                }
              }
            });
          }
        }
      }, error => {
        console.log(error);
      });
      // const partyData = this.partyAndOrganizationData;
      // this.organizationWiseParty = [];

      // // tslint:disable-next-line:radix
      // if (id !== undefined && parseInt(id) > 0) {
      //   partyData.forEach(element => {
      //     if (element.PrimaryOrganization !== undefined) {
      //       if (element.PrimaryOrganization.OrganizationPartyId !== undefined) {
      //         const organizationID = element.PrimaryOrganization.OrganizationPartyId;
      //         if (id === organizationID) {
      //           this.organizationWiseParty.push(element);
      //         }
      //       }
      //     }
      //   });
      // } else {
      //   this.organizationWiseParty = [];
      // }

      // this.billingToContactsList = this.organizationWiseParty;

    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getMediaOrderProduction(mOrderId) {
    try {
      this.completeOrderService.getByMediaOrderApi(mOrderId).subscribe(result => {
        if (result.Data != null) {
          this.buttonText = 'Update';
          this.completeOrder = result.Data;

          this.buyId = this.completeOrder.BuyId;
          this.mediaOrderProductionDetailId = this.completeOrder.MediaOrderProductionDetailId;

          this.completeForm.controls['completeOrderProductionStages'].setValue(null);

          /* --------------------------------------------------------*/

          if (this.completeOrder.TrackingNumber === undefined
            || this.completeOrder.TrackingNumber === ''
            || this.completeOrder.TrackingNumber === null) {
            this.completeForm.controls['TrackingNumber'].setValue('0');
          } else {
            if (!isNaN(this.completeOrder.TrackingNumber)) {
              // tslint:disable-next-line:radix
              this.completeForm.controls['TrackingNumber'].setValue(parseInt(this.completeOrder.TrackingNumber));
            } else {
              this.completeForm.controls['TrackingNumber'].setValue('');
            }
          }

          if (this.completeOrder.WebAdUrl === undefined
            || this.completeOrder.WebAdUrl === ''
            || this.completeOrder.WebAdUrl === null) {
            this.completeForm.controls['WebAdUrl'].setValue('');
          } else {
            this.completeForm.controls['WebAdUrl'].setValue(this.completeOrder.WebAdUrl);
          }

          if (this.completeOrder.HeadLine === undefined
            || this.completeOrder.HeadLine === ''
            || this.completeOrder.HeadLine === null) {
            this.completeForm.controls['HeadLine'].setValue('');
          } else {
            this.completeForm.controls['HeadLine'].setValue(this.completeOrder.HeadLine);
          }

          if (this.completeOrder.TearSheets === undefined
            || this.completeOrder.TearSheets === ''
            || this.completeOrder.TearSheets === null) {
            this.completeForm.controls['TearSheets'].setValue('');
          } else {
            this.completeForm.controls['TearSheets'].setValue(this.completeOrder.TearSheets);
          }

          if (this.completeOrder.PageNumber === undefined
            || this.completeOrder.PageNumber === ''
            || this.completeOrder.PageNumber === null) {
            this.completeForm.controls['PageNumber'].setValue('');
          } else {
            this.completeForm.controls['PageNumber'].setValue(this.completeOrder.PageNumber);
          }

          if (this.completeOrder.ClassifiedText === undefined
            || this.completeOrder.ClassifiedText === ''
            || this.completeOrder.ClassifiedText === null) {
            this.completeForm.controls['ClassifiedText'].setValue('');
          } else {
            this.completeForm.controls['ClassifiedText'].setValue(this.completeOrder.ClassifiedText);
          }

          if (this.completeOrder.ProductionComment === undefined
            || this.completeOrder.ProductionComment === ''
            || this.completeOrder.ProductionComment === null) {
            this.completeForm.controls['ProductionComment'].setValue('');
          } else {
            this.completeForm.controls['ProductionComment'].setValue(this.completeOrder.ProductionComment);
          }

          /* --------------------------------------------------------*/

          // Radio button setting, check box setting
          if (this.completeOrder.NewPickupInd === false
            || this.completeOrder.NewPickupInd === null
            || this.completeOrder.NewPickupInd === undefined) {
            this.dvNew = true;
            this.dvPickup = false;
            this.completeForm.controls['NewPickupInd'].setValue('0');
            this.completeForm.controls['PickupMediaOrderId'].enable();
          } else if (this.completeOrder.NewPickupInd === true) {
            this.dvNew = false;
            this.dvPickup = true;
            this.completeForm.controls['NewPickupInd'].setValue('1');
            this.completeForm.controls['PickupMediaOrderId'].disable();
          }

          if (this.completeOrder.AdvertiserAgencyInd === false
            || this.completeOrder.AdvertiserAgencyInd === null
            || this.completeOrder.AdvertiserAgencyInd === undefined) {
            this.completeForm.controls['AdvertiserAgencyInd'].setValue('0');
          } else if (this.completeOrder.AdvertiserAgencyInd === true) {
            this.completeForm.controls['AdvertiserAgencyInd'].setValue('1');
          }

          if (this.completeOrder.CreateExpectedInd === false
            || this.completeOrder.CreateExpectedInd === null
            || this.completeOrder.CreateExpectedInd === undefined) {

            this.completeForm.controls['CreateExpectedInd'].setValue('0');
          } else if (this.completeOrder.CreateExpectedInd === true) {
            this.completeForm.controls['CreateExpectedInd'].setValue('1');
          }

          if (this.completeOrder.OnHandInd === false
            || this.completeOrder.OnHandInd === null
            || this.completeOrder.OnHandInd === undefined) {
            this.completeForm.controls['OnHandInd'].setValue(false);
          } else if (this.completeOrder.OnHandInd === true) {
            this.completeForm.controls['OnHandInd'].setValue(true);
          }

          if (this.completeOrder.ChangesInd === false
            || this.completeOrder.ChangesInd === null
            || this.completeOrder.ChangesInd === undefined) {
            this.completeForm.controls['ChangesInd'].setValue(false);
          } else if (this.completeOrder.ChangesInd === true) {
            this.completeForm.controls['ChangesInd'].setValue(true);
          }

          /* -------------------- FILE -------------------- */

          if (this.completeOrder.OriginalFile != null) {
            const tempOriginal = this.completeOrder.OriginalFile;
            const fileNameOriginal = tempOriginal.substring(tempOriginal.lastIndexOf('/')).split('/').pop();

            this.originalFile = this.completeOrder.OriginalFile;
            this.extensionOriginal = fileNameOriginal.substring(fileNameOriginal.lastIndexOf('.'));
            this.completeForm.controls['OrigFileName'].setValue(fileNameOriginal);
          } else {
            this.originalFile = null;
            this.extensionOriginal = null;
            this.completeForm.controls['OrigFileName'].setValue(null);
          }

          if (this.completeOrder.ProofFile != null) {
            const tempProof = this.completeOrder.ProofFile;
            const fileNameProof = tempProof.substring(tempProof.lastIndexOf('/')).split('/').pop();

            this.proofFile = this.completeOrder.ProofFile;
            this.extensionProof = fileNameProof.substring(fileNameProof.lastIndexOf('.'));
            this.completeForm.controls['ProofFileName'].setValue(fileNameProof);
          } else {
            this.proofFile = null;
            this.extensionProof = null;
            this.completeForm.controls['ProofFileName'].setValue(null);
          }

          if (this.completeOrder.FinalFile != null) {
            const tempFinal = this.completeOrder.FinalFile;
            const fileNameFinal = tempFinal.substring(tempFinal.lastIndexOf('/')).split('/').pop();

            this.finalFile = this.completeOrder.FinalFile;
            this.extensionFinal = fileNameFinal.substring(fileNameFinal.lastIndexOf('.'));
            this.completeForm.controls['FinalFileName'].setValue(fileNameFinal);
          } else {
            this.finalFile = null;
            this.extensionFinal = null;
            this.completeForm.controls['FinalFileName'].setValue(null);
          }

          if (this.extensionOriginal === null) {
            this.isPdfTrueForOriginal = false;
            this.isImgTrueForOriginal = false;
          } else {
            if (this.extensionOriginal === '.pdf') {
              this.isPdfTrueForOriginal = true;
              this.isImgTrueForOriginal = false;
              this.tempPdfPathForOriginal = this.completeOrder.OriginalFile;
            } else if (this.extensionOriginal === '.png'
              || this.extensionOriginal === '.jpg'
              || this.extensionOriginal === '.jpeg') {
              this.isPdfTrueForOriginal = false;
              this.isImgTrueForOriginal = true;
            } else {
              this.isPdfTrueForOriginal = true;
              this.isImgTrueForOriginal = false;
              this.tempPdfPathForOriginal = this.completeOrder.OriginalFile;
            }
          }

          if (this.extensionProof === null) {
            this.isPdfTrueForProof = false;
            this.isImgTrueForProof = false;
          } else {
            if (this.extensionProof === '.pdf') {
              this.isPdfTrueForProof = true;
              this.isImgTrueForProof = false;
              this.tempPdfPathForProof = this.completeOrder.ProofFile;
            } else if (this.extensionProof === '.png'
              || this.extensionProof === '.jpg'
              || this.extensionProof === '.jpeg') {
              this.isPdfTrueForProof = false;
              this.isImgTrueForProof = true;
            } else {
              this.isPdfTrueForProof = true;
              this.isImgTrueForProof = false;
              this.tempPdfPathForProof = this.completeOrder.ProofFile;
            }
          }

          if (this.extensionFinal === null) {
            this.isPdfTrueForfinal = false;
            this.isImgTrueForfinal = false;
          } else {
            if (this.extensionFinal === '.pdf') {
              this.isPdfTrueForfinal = true;
              this.isImgTrueForfinal = false;
              this.tempPdfPathForFinal = this.completeOrder.FinalFile;
            } else if (this.extensionFinal === '.png'
              || this.extensionFinal === '.jpg'
              || this.extensionFinal === '.jpeg') {
              this.isPdfTrueForfinal = false;
              this.isImgTrueForfinal = true;
            } else {
              this.isPdfTrueForfinal = true;
              this.isImgTrueForfinal = false;
              this.tempPdfPathForFinal = this.completeOrder.FinalFile;
            }
          }

          /* -------------------- End FILE -------------------- */

          if (result.Data.MaterialExpectedDate !== undefined && result.Data.MaterialExpectedDate !== ''
            && result.Data.MaterialExpectedDate != null) {
            this.completeForm.controls['MaterialExpectedDate'].setValue(Common.getDateInFormat(result.Data.MaterialExpectedDate));
          } else {
            this.completeForm.controls['MaterialExpectedDate'].setValue('');
          }

          this.originalFileImageSrc = this.completeOrder.OriginalFile;
          this.proofFileImageSrc = this.completeOrder.ProofFile;
          this.finalFileImageSrc = this.completeOrder.FinalFile;

          /* ------------------------------------------------------ */
          if (!isNullOrUndefined(this.completeOrder.PickupMediaOrderId)) {
            this.completeForm.controls['PickupMediaOrderId'].setValue(this.completeOrder.PickupMediaOrderId);
          } else {
            this.completeForm.controls['PickupMediaOrderId'].setValue(null);
          }
          /* ------------------------------------------------------ */
          if (!isNullOrUndefined(this.completeOrder.PositionId)) {
            this.completeForm.controls['PositionId'].setValue(this.completeOrder.PositionId);
          } else {
            this.completeForm.controls['PositionId'].setValue(null);
          }
          /* ------------------------------------------------------ */
          if (!isNullOrUndefined(this.completeOrder.SeparationId)) {
            this.completeForm.controls['SeparationId'].setValue(this.completeOrder.SeparationId);
          } else {
            this.completeForm.controls['SeparationId'].setValue(null);
          }
          /* ------------------------------------------------------ */
          if (!isNullOrUndefined(this.completeOrder.MaterialContactId)) {
            if (this.completeOrder.AdvertiserAgencyInd === false
              || this.completeOrder.AdvertiserAgencyInd === null
              || this.completeOrder.AdvertiserAgencyInd === undefined) {
              this.getBillToContact('0');
            } else if (this.completeOrder.AdvertiserAgencyInd === true) {
              this.getBillToContact('1');
            }
            this.completeForm.controls['MaterialContactId'].setValue(this.completeOrder.MaterialContactId.toString());
          } else {
            this.completeForm.controls['MaterialContactId'].setValue(null);
          }
          /* ------------------------------------------------------ */
          if (!isNullOrUndefined(this.completeOrder.ProductionStatusId)) {
            this.completeForm.controls['ProductionStatusId'].setValue(this.completeOrder.ProductionStatusId);
          } else {
            this.completeForm.controls['ProductionStatusId'].setValue(null);
          }
          /* ------------------------------------------------------ */
          this.completeOrder_ProductionStagesList = [];
          this.completeOrder_ProductionStagesList = this.completeOrder.ProductionStages;
        } else {
          this.buttonText = 'Add';
        }
      }, error => {
        this.hideloader();
        console.log(error + 'status : ' + error.status);
      });
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getAllProductionStatus() {
    try {
      if (this._globalClass.getrProductionStatus().length <= 0) {
        this.incompleteOrderService.getAllProductionStatus().subscribe(result => {
          if (result.StatusCode === 1) {
            if (result.Data != null) {
              if (result.Data.length > 0) {
                this.productionStatusList = result.Data;
                this._globalClass.setrProductionStatus(this.productionStatusList);
              } else {
                this.productionStatusList = [];
              }
            } else {
              this.productionStatusList = [];
            }
          } else if (result.StatusCode === 3) {
            this.productionStatusList = [];
          } else {
            this.productionStatusList = [];
            this.toastr.error(result.Message, 'Error!');
          }
        }, error => {
          this.hideloader();
          console.log(error);
        });
      } else {
        this.productionStatusList = this._globalClass.getrProductionStatus();
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getAllPosition() {
    try {
      if (this._globalClass.getrPositions().length <= 0) {
        this.incompleteOrderService.getAllPosition().subscribe(result => {

          if (result.StatusCode === 1) {
            if (result.Data != null) {
              if (result.Data.length > 0) {
                this.positionsList = result.Data;
                this._globalClass.setrPositions(this.positionsList);
              } else {
                this.positionsList = [];
              }
            } else {
              this.positionsList = [];
            }
          } else if (result.StatusCode === 3) {
            this.positionsList = [];
          } else {
            this.positionsList = [];
            this.toastr.error(result.Message, 'Error!');
          }

        }, error => {
          this.hideloader();
          console.log(error);
        });
      } else {
        this.positionsList = this._globalClass.getrPositions();
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getAllSeparation() {
    try {
      if (this._globalClass.getrSeparations().length <= 0) {
        this.incompleteOrderService.getAllSeparation().subscribe(result => {
          if (result.StatusCode === 1) {
            if (result.Data != null) {
              if (result.Data.length > 0) {
                this.separationsList = result.Data;
                this._globalClass.setrSeparations(this.separationsList);
              } else {
                this.separationsList = [];
              }
            } else {
              this.separationsList = [];
            }
          } else if (result.StatusCode === 3) {
            this.separationsList = [];
          } else {
            this.separationsList = [];
            this.toastr.error(result.Message, 'Error!');
          }
        }, error => {
          this.hideloader();
          console.log(error);
        });
      } else {
        this.separationsList = this._globalClass.getrSeparations();
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getAllProductionStages() {
    try {
      if (this._globalClass.getrProductionStages1().length <= 0) {
        this.incompleteOrderService.getAllProductionStage().subscribe(result => {
          if (result.StatusCode === 1) {
            this.productionStages1List = result.Data;
            this._globalClass.setrProductionStages1(this.productionStages1List);
          } else {
            this.toastr.error(result.Message, 'Error');
          }
        }, error => {
          this.hideloader();
          console.log(error);
        });
      } else {
        this.productionStages1List = this._globalClass.getrProductionStages1();
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  update() {
    try {
      let PickupMediaOrderId = this.completeForm.controls['PickupMediaOrderId'].value;
      let NewPickupInd = this.completeForm.controls['NewPickupInd'].value;
      let AdvertiserAgencyInd = this.completeForm.controls['AdvertiserAgencyInd'].value;
      let CreateExpectedInd = this.completeForm.controls['CreateExpectedInd'].value;
      let MaterialExpectedDate = this.completeForm.controls['MaterialExpectedDate'].value;
      let TrackingNumber = this.completeForm.controls['TrackingNumber'].value;
      let ChangesInd = this.completeForm.controls['ChangesInd'].value;
      let OnHandInd = this.completeForm.controls['OnHandInd'].value;

      let OrigFileName = this.completeForm.controls['OrigFileName'].value;
      let ProofFileName = this.completeForm.controls['ProofFileName'].value;
      let FinalFileName = this.completeForm.controls['FinalFileName'].value;

      let WebAdUrl = this.completeForm.controls['WebAdUrl'].value;
      let TearSheets = this.completeForm.controls['TearSheets'].value;
      let HeadLine = this.completeForm.controls['HeadLine'].value;
      let PageNumber = this.completeForm.controls['PageNumber'].value;
      let ProductionComment = this.completeForm.controls['ProductionComment'].value;
      let MaterialContactId = this.completeForm.controls['MaterialContactId'].value;
      const ClassifiedText = this.completeForm.controls['ClassifiedText'].value;
      const PositionId = this.completeForm.controls['PositionId'].value;
      const ProductionStatusId = this.completeForm.controls['ProductionStatusId'].value;
      const SeparationId = this.completeForm.controls['SeparationId'].value;

      let OriginalFile = this.originalFile;
      let OriginalFileExtension = this.originalFileExtension;

      let ProofFile = this.proofFile;
      let ProofFileExtension = this.proofFileExtension;

      let FinalFile = this.finalFile;
      let FinalFileExtension = this.finalFileExtension;

      if (PickupMediaOrderId != null && PickupMediaOrderId !== '' && PickupMediaOrderId !== undefined) {
        // tslint:disable-next-line:radix
        PickupMediaOrderId = parseInt(PickupMediaOrderId);
      } else {
        PickupMediaOrderId = null;
      }

      if (NewPickupInd === '0') {
        NewPickupInd = false;
      } else if (NewPickupInd === '1') {
        NewPickupInd = true;
      }

      if (AdvertiserAgencyInd === '0') {
        AdvertiserAgencyInd = false;
      } else if (AdvertiserAgencyInd === '1') {
        AdvertiserAgencyInd = true;
      }

      if (CreateExpectedInd === '0') {
        CreateExpectedInd = false;
      } else if (CreateExpectedInd === '1') {
        CreateExpectedInd = true;
      }

      if (MaterialExpectedDate === undefined || MaterialExpectedDate === null || MaterialExpectedDate === '') {
        MaterialExpectedDate = '';
      } else {
        MaterialExpectedDate = Common.getDate(MaterialExpectedDate);
      }

      if (TrackingNumber === undefined || TrackingNumber === null) {
        TrackingNumber = '';
      } else {
        TrackingNumber = (TrackingNumber).toString();
      }

      if (ChangesInd === undefined || ChangesInd === '' || ChangesInd === null) {
        ChangesInd = null;
      }

      if (OnHandInd === undefined || OnHandInd === '' || OnHandInd === null) {
        OnHandInd = null;
      }

      if (OriginalFile === undefined || OriginalFile === null || OriginalFile === '') {
        OrigFileName = null;
        OriginalFile = null;
        OriginalFileExtension = null;
      } else {
        OriginalFileExtension = OrigFileName.substring(OrigFileName.lastIndexOf('.'));
      }

      if (ProofFile === undefined || ProofFile === null || ProofFile === '') {
        ProofFileName = null;
        ProofFile = null;
        ProofFileExtension = null;
      } else {
        ProofFileExtension = ProofFileName.substring(ProofFileName.lastIndexOf('.'));
      }

      if (FinalFile === undefined || FinalFile === null || FinalFile === '') {
        FinalFileName = null;
        FinalFile = null;
        FinalFileExtension = null;
      } else {
        FinalFileExtension = FinalFileName.substring(FinalFileName.lastIndexOf('.'));
      }

      if (WebAdUrl === undefined || WebAdUrl === '' || WebAdUrl === null) {
        WebAdUrl = '';
      }

      if (TearSheets === undefined || TearSheets === '' || TearSheets === null) {
        TearSheets = '';
      } else {
        // tslint:disable-next-line:radix
        TearSheets = parseInt(TearSheets);
      }

      if (HeadLine === undefined || HeadLine === '' || HeadLine === null) {
        HeadLine = '';
      }

      if (PageNumber === undefined || PageNumber === '' || PageNumber === null) {
        PageNumber = '';
      } else {
        PageNumber = (PageNumber).toString();
      }

      if (ProductionComment === undefined || ProductionComment === '' || ProductionComment === null) {
        ProductionComment = '';
      }

      let l_ProductionStatusId = null;
      if (ProductionStatusId != null && ProductionStatusId !== undefined) {
        l_ProductionStatusId = ProductionStatusId;
      }

      // tslint:disable-next-line:radix
      MaterialContactId = parseInt(MaterialContactId);

      const production = {
        PickupMediaOrderId: PickupMediaOrderId,
        NewPickupInd: NewPickupInd,
        AdvertiserAgencyInd: AdvertiserAgencyInd,
        CreateExpectedInd: CreateExpectedInd,
        MaterialExpectedDate: MaterialExpectedDate,
        TrackingNumber: TrackingNumber,
        ChangesInd: ChangesInd,
        OnHandInd: OnHandInd,

        WebAdUrl: WebAdUrl,
        TearSheets: TearSheets,
        HeadLine: HeadLine,
        PageNumber: PageNumber,
        ProductionComment: ProductionComment,
        MaterialContactId: MaterialContactId,
        ClassifiedText: ClassifiedText,
        PositionId: PositionId,
        ProductionStages: this.completeOrder_ProductionStagesList,
        ProductionStatusId: l_ProductionStatusId,
        SeparationId: SeparationId,

        Completed: true,
        MediaOrderId: this.selectedMediaOrderId,
        MediaOrderProductionDetailId: this.mediaOrderProductionDetailId,

        OriginalFile: OriginalFile,
        OriginalFileExtension: OriginalFileExtension,
        OrigFileName: OrigFileName,

        ProofFile: ProofFile,
        ProofFileExtension: ProofFileExtension,
        ProofFileName: ProofFileName,

        FinalFile: FinalFile,
        FinalFileExtension: FinalFileExtension,
        FinalFileName: FinalFileName,
      };

      this.showloader();
      this.completeOrderService.updateProduction(production).subscribe(result => {
        if (result.StatusCode === 1) {
          this.hideloader();
          this.toastr.success('Order save successfully.', 'Success!');
          this.getCompleteOrderByCustomFilterOption();
        } else {
          this.hideloader();
          this.toastr.error(result.Message, 'Error!');
        }
      }, error => {
        this.hideloader();
        console.log(error);
      });

    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  onChangeFrom() {
  }

  onChangeContact() {
  }

  onChangeProductionStatus() {
  }

  onChangeSeparation() {
  }

  onChangePosition() {
  }

  onChangeProductionStages() {
  }

  // --------- Add, Delete Production Stages ---------

  addProductionStages() {
    try {
      if (this.productionStages1List != null) {
        const comOrProdStge = this.completeForm.controls['completeOrderProductionStages'].value;
        // tslint:disable-next-line:max-line-length
        // tslint:disable-next-line:radix
        const data = alasql('SELECT * FROM ? AS add WHERE ProductionStageId = ?', [this.productionStages1List, parseInt(comOrProdStge)]);
        console.log('data',data);
        const final = {
          ProductionStagesName: data[0].Name,
          UpdatedDate: new Date(),
          // tslint:disable-next-line:radix
          ProductionStageId: parseInt(comOrProdStge),
          UpdateBy: this.updatedBy,
          MediaOrderProductionStagesId: '',
        };
        this.completeOrder_ProductionStagesList.push(final);
        this.toastr.success('Production stage added successfully.', 'Success!');
        this.completeForm.controls['completeOrderProductionStages'].setValue(null);
      }
      if (this.completeOrder_ProductionStagesList.length > 0) {
      } else {
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  deleteProductionStage(productionStage) {
    try {
      this.completeOrder_ProductionStagesList.forEach((element, index) => {
        if (element.UpdatedDate === productionStage.UpdatedDate && element.ProductionStagesName === productionStage.ProductionStagesName) {
          this.completeOrder_ProductionStagesList.splice(index, 1);
        }
      });
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  // --------- End Of Add, Delete Production Stages ---------

  setEnableOrDisable(rdoName) {
    try {
      // tslint:disable-next-line:radix
      if (parseInt(rdoName) === 1) {
        this.dvNew = false;
        this.dvPickup = true;
        this.completeForm.controls['PickupMediaOrderId'].disable();
        this.completeForm.controls['ChangesInd'].setValue(false);
        this.completeForm.controls['PickupMediaOrderId'].setValue(null);
        this.completeForm.controls['CreateExpectedInd'].setValue('0');
        // tslint:disable-next-line:radix
      } else if (parseInt(rdoName) === 0) {
        this.dvNew = true;
        this.dvPickup = false;
        this.completeForm.controls['PickupMediaOrderId'].enable();
        this.completeForm.controls['CreateExpectedInd'].setValue('0');
        this.completeForm.controls['OnHandInd'].setValue(false);
        this.completeForm.controls['MaterialExpectedDate'].setValue('');
        this.completeForm.controls['TrackingNumber'].setValue('');
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  setToInCompleteInserationOrder() {
    try {
      if (this.selectedCheckbox.length === 1) {
        const mId = this.selectedMediaOrderIds[0];
        this.showloader();

        this.completeOrderService.setOrderProductionStatusToInComplete(mId).subscribe(result => {
          if (result.StatusCode === 1) {
            this.hideloader();
            if (result.Data != null) {
              const Data = result.Data;
              this.resetValueChkChange();

              this.selectedIssueMediaOrder = [];
              this.selectedCheckbox = [];
              this.selectedMediaOrderIds = [];
              this.selectedAdvertiserIds = [];
              this.selectedAgencyIds = [];

              this.toastr.success('Mark as incompleted successfully.', 'Success!');
              this.getCompleteOrderByCustomFilterOption();
            }
          } else {
            this.hideloader();
            this.toastr.error(result.Message, 'Error!');
          }
        }, error => {
          this.hideloader();
          console.log(error);
        });
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  // ------------ Upload & Image Viewer-----------
  uploadImage(event: any) {
    try {
      if (event.target.files && event.target.files[0]) {

        const logoElement = event.target.files.item(0);
        const uploadType = logoElement.type;
        const filename = event.target.files.item(0).name;
        const ext = filename.substr(filename.lastIndexOf('.') + 1);

        if (uploadType !== undefined && uploadType != null) {
          if (logoElement.type.indexOf('application/pdf') !== -1) {
            const tmppath = URL.createObjectURL(event.target.files[0]);
            this.tempPdfPathForOriginal = tmppath;
            this.isPdfTrueForOriginal = true;
            this.isImgTrueForOriginal = false;
          } else if (logoElement.type.indexOf('image') !== -1) {
            this.isImgTrueForOriginal = true;
            this.isPdfTrueForOriginal = false;
          } else {
            this.isImgTrueForOriginal = false;
            this.isPdfTrueForOriginal = false;
          }

          const reader = new FileReader();
          reader.onload = (e: any) => {
            if (this.isImgTrueForOriginal === false && this.isPdfTrueForOriginal === false) {
              this.originalFileImageSrc = '';
            } else {
              this.originalFileImageSrc = e.target.result;
            }
            const strImage = e.target.result.substring((e.target.result.indexOf(',') + 1));
            this.originalFile = strImage;
          };
          reader.readAsDataURL(event.target.files[0]);

          this.completeForm.controls['OrigFileName'].setValue(filename);
          this.originalFileExtension = ext;
        }
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }

  }

  uploadImageforfinal(event: any) {
    try {
      if (event.target.files && event.target.files[0]) {

        const logoElement = event.target.files.item(0);
        const uploadType = logoElement.type;
        const filename = event.target.files.item(0).name;
        const ext = filename.substr(filename.lastIndexOf('.') + 1);

        if (uploadType !== undefined && uploadType != null) {
          if (logoElement.type.indexOf('application/pdf') !== -1) {
            const tmppath = URL.createObjectURL(event.target.files[0]);
            this.tempPdfPathForFinal = tmppath;
            this.isPdfTrueForfinal = true;
            this.isImgTrueForfinal = false;
          } else if (logoElement.type.indexOf('image') !== -1) {
            this.isImgTrueForfinal = true;
            this.isPdfTrueForfinal = false;
          } else {
            this.isImgTrueForfinal = false;
            this.isPdfTrueForfinal = false;
          }

          const reader = new FileReader();
          reader.onload = (e: any) => {
            if (this.isImgTrueForfinal === false && this.isPdfTrueForfinal === false) {
              this.finalFileImageSrc = '';
            } else {
              this.finalFileImageSrc = e.target.result;
            }
            const strImage = e.target.result.substring((e.target.result.indexOf(',') + 1));
            this.finalFile = strImage;
          };
          reader.readAsDataURL(event.target.files[0]);

          this.completeForm.controls['FinalFileName'].setValue(filename);
          this.finalFileExtension = ext;
        }
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  uploadImageforProof(event: any) {
    try {
      if (event.target.files && event.target.files[0]) {

        const logoElement = event.target.files.item(0);
        const uploadType = logoElement.type;
        const filename = event.target.files.item(0).name;
        const ext = filename.substr(filename.lastIndexOf('.') + 1);

        if (uploadType !== undefined && uploadType != null) {
          if (logoElement.type.indexOf('application/pdf') !== -1) {
            const tmppath = URL.createObjectURL(event.target.files[0]);
            this.tempPdfPathForProof = tmppath;
            this.isPdfTrueForProof = true;
            this.isImgTrueForProof = false;
          } else if (logoElement.type.indexOf('image') !== -1) {
            this.isImgTrueForProof = true;
            this.isPdfTrueForProof = false;
          } else {
            this.isImgTrueForProof = false;
            this.isPdfTrueForProof = false;
          }

          const reader = new FileReader();
          reader.onload = (e: any) => {
            if (this.isImgTrueForProof === false && this.isPdfTrueForProof === false) {
              this.proofFileImageSrc = '';
            } else {
              this.proofFileImageSrc = e.target.result;
            }
            const strImage = e.target.result.substring((e.target.result.indexOf(',') + 1));
            this.proofFile = strImage;
          };
          reader.readAsDataURL(event.target.files[0]);

          this.completeForm.controls['ProofFileName'].setValue(filename);
          this.proofFileExtension = ext;
        }
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  showImageInViewerForOriginalFile() {
    try {
      if (this.isPdfTrueForOriginal === false && this.isImgTrueForOriginal === false) {
        if (this.originalFileImageSrc !== '' && this.originalFileImageSrc !== undefined && this.originalFileImageSrc != null) {
          window.open(this.tempPdfPathForOriginal, '_blank');
        }

      } else {
        if (this.isPdfTrueForOriginal === true) {
          window.open(this.tempPdfPathForOriginal, '_blank');
        } else if (this.isImgTrueForOriginal === true) {
          // tslint:disable-next-line:no-unused-expression
          new imgviewerfun('docs-pictures-originalFile', 'docs-buttons-originalFile');
        }
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  showImageInViewerForProofFile() {
    try {
      if (this.isPdfTrueForProof === false && this.isImgTrueForProof === false) {
        if (this.proofFileImageSrc !== '' && this.proofFileImageSrc !== undefined && this.proofFileImageSrc != null) {
          window.open(this.tempPdfPathForProof, '_blank');
        }
      } else {
        if (this.isPdfTrueForProof === true) {
          window.open(this.tempPdfPathForProof, '_blank');
        } else if (this.isImgTrueForProof === true) {
          // tslint:disable-next-line:no-unused-expression
          new imgviewerfun('docs-pictures-proofFile', 'docs-buttons-proofFile');
        }
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  showImageInViewerForFinalFile() {
    try {
      if (this.isPdfTrueForfinal === false && this.isImgTrueForfinal === false) {
        if (this.finalFileImageSrc !== '' && this.finalFileImageSrc !== undefined && this.finalFileImageSrc != null) {
          window.open(this.tempPdfPathForFinal, '_blank');
        }
      } else {
        if (this.isPdfTrueForfinal === true) {
          window.open(this.tempPdfPathForFinal, '_blank');
        } else if (this.isImgTrueForfinal === true) {
          // tslint:disable-next-line:no-unused-expression
          new imgviewerfun('docs-pictures-finalFile', 'docs-buttons-finalFile');
        }
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  // ------------ Export :: Excel & PDF-----------
  exportCompleteDataToExcel() {
    try {
      const completeOrders = [];
      this.completeOrders.forEach((element, index) => {
        this.objModelExport = {};
        this.objModelExport.AdvertiserName = element.AdvertiserName;
        this.objModelExport.AdDetails = element.AdTypeName + ' | ' + element.AdcolorName + ' | ' + element.AdSizeName;
        this.objModelExport.MaterialInfo = element.MaterialInfo;
        this.objModelExport.Rep = element.RepName;
        this.objModelExport.Position = (element.PositionName === null ? '' : element.PositionName);
        completeOrders.push(this.objModelExport);
      });
      alasql('SELECT * INTO XLSX("CompleteOrders.xlsx",{headers:true}) FROM ?', [completeOrders]);
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  exportCompleteDataToPdf() {
    try {
      const tmpCompleteOrdersForAutoTable = [];
      let obj = [];
      this.completeOrders.forEach((element, index) => {
        obj = [];
        obj.push(element.AdvertiserName == null ? '' : element.AdvertiserName);
        obj.push(element.AdTypeName + ' | ' + element.AdcolorName + ' | ' + element.AdSizeName);
        obj.push(element.MaterialInfo);
        obj.push(element.RepName == null ? '' : element.RepName);
        obj.push(element.PositionName == null ? '' : element.PositionName);
        tmpCompleteOrdersForAutoTable.push(obj);
      });

      const columns = ['Advertiser', 'AdDetails', 'Material Info', 'Rep', 'Position'];
      // Only pt supported (not mm or in)
      const doc = new jsPDF('p', 'pt');
      doc.autoTable(columns, tmpCompleteOrdersForAutoTable, {
        theme: 'plain'
      });
      doc.save('CompleteOrders.pdf');
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  ngOnDestroy() {
    this.subscriptionForCompleteExport.unsubscribe();
    this.subscriptionForCompleteOrder.unsubscribe();
  }

  showloader() {
    this.appComponent.isLoading = true;
  }

  hideloader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 1000);
  }
}
