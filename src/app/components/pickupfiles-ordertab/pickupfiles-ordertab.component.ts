import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { Subscription } from 'rxjs';
import { ToastrService, ToastrModule } from 'ngx-toastr';
import { AppComponent } from '../../app.component';
import { IncompleteOrderService } from '../../services/incomplete-order.service';
import { PickupFilesOrderService } from '../../services/pickup-files-order.service';
import { ProductionViewtabService } from '../../services/production-viewtab.service';
import { GlobalClass } from '../../GlobalClass';
import * as Common from '../../common/common';
import * as alasql from 'alasql';
import { isNullOrUndefined } from 'util';
declare var imgviewerfun: any;
declare var jsPDF: any;

@Component({
  selector: 'asi-pickupfiles-ordertab',
  templateUrl: './pickupfiles-ordertab.component.html',
  styleUrls: ['./pickupfiles-ordertab.component.css']
})
export class PickupfilesOrdertabComponent implements OnInit, OnDestroy {

  mediaOrderProductionDetailId: any;

  subscriptionForPickupOrder: Subscription;
  subscriptionForPickupExport: Subscription;

  pickupfilesOrderForm: FormGroup;

  pickUpFilesOrders: any[] = [];
  pickUpFilesOrder: any = {};
  mediaAssetsList = [];
  billingToContactsList = [];
  productionStatusList = [];
  separationsList = [];
  positionsList = [];
  productionStages1List = [];
  pickUpFilesOrder_ProductionStagesList = [];
  issueDatesList = [];

  selectedCheckbox = [];
  selectedMediaOrderIds = [];
  selectedAdvertiserIds = [];
  selectedAgencyIds = [];

  partyAndOrganizationData = [];

  tempArray = [];
  advertisers = [];
  agencies = [];
  personData = [];
  organizationWiseParty = [];

  dataObject: any = {};
  selectedIssueMediaOrder: any[] = [];
  mediaAssetModel: any = {};
  objModelExport: any = {};


  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';

  originalFile = '';
  originalFileImageSrc = '';
  originalFileExtension = '';

  finalFile = null;
  finalFileExtension = '';
  finalFileImageSrc = '';
  selectedAdvertiserAgencyName='';

  proofFile = null;
  proofFileExtension = '';
  proofFileImageSrc = '';

  extensionOriginal = '';
  extensionProof = '';
  extensionFinal = '';

  tempPdfPathForOriginal = '';
  tempPdfPathForProof = '';
  tempPdfPathForFinal = '';

  buttonText = 'Add';

  selectedMediaOrderId = '';
  selectedAdvertiserId = '';
  selectedAgencyId = '';

  buyId = 0;
  updatedBy = '';
  completed: any;

  setProductionNoClick = true;
  flag = true;
  dvNew = false;
  dvPickup = true;

  isPdfTrueForProof = false;
  isPdfTrueForfinal = false;
  isPdfTrueForOriginal = false;

  isImgTrueForOriginal = false;
  isImgTrueForfinal = false;
  isImgTrueForProof = false;

  hasNext: boolean;
  offset: number;

  constructor(private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public appComponent: AppComponent,
    private incompleteOrderService: IncompleteOrderService,
    private pickupFilesOrderService: PickupFilesOrderService,
    private productionViewtabService: ProductionViewtabService,
    private _globalClass: GlobalClass) {
    ToastrModule.forRoot();
  }

  ngOnInit() {
    this.getToken();
    this.pickupfilesOrderFormValidation();
    try {
      this.pickUpFilesOrder = {};
      this.pickUpFilesOrder.ProductionStages = [];
      this.pickUpFilesOrder.NewPickupInd = '1';
      this.pickUpFilesOrder.CreateExpectedInd = '0';
      this.pickUpFilesOrder.AdvertiserAgencyInd = '1';
      this.pickUpFilesOrder.OriginalFileExtension = '';
    } catch (error) {
      this. hideloader();
      console.log(error);
    }

    this.pickupfilesOrderForm.controls['PickupMediaOrderId'].disable();
    this.pickupfilesOrderForm.controls['NewPickupInd'].setValue('1');
    this.pickupfilesOrderForm.controls['CreateExpectedInd'].setValue('0');
    this.pickupfilesOrderForm.controls['AdvertiserAgencyInd'].setValue('1');

    this.subscriptionForPickupOrder = this.productionViewtabService.get_OrderPickupFilterOption().subscribe(message => {
      if (message.text === 'getPickUpFilesOrderByCustomFilterOption') {
        this.getPickUpFilesOrderByCustomFilterOption();
      }
    });

    this.subscriptionForPickupExport = this.productionViewtabService.get_PickupExport().subscribe(message => {
      if (message.text === 'Excel') {
        this.exporttoExcel();
      } else if (message.text === 'PDF') {
        this.exportToPdf();
      }
    });

    if (this.appComponent.pickUpFilesTab === true) {
      const MediaAssetId = this.appComponent.productionForm.controls['MediaAssetId'].value;
      const IssueDateId = this.appComponent.productionForm.controls['IssueDateId'].value;
      const AdTypeId = this.appComponent.productionForm.controls['AdTypeId'].value;
      const RepId = this.appComponent.productionForm.controls['RepId'].value;
      if (!isNullOrUndefined(AdTypeId) && AdTypeId !== 0
        || !isNullOrUndefined(MediaAssetId) && MediaAssetId !== 0
        || !isNullOrUndefined(IssueDateId) && IssueDateId !== 0
        || !isNullOrUndefined(RepId) && RepId !== 0) {
        console.log('getPickUpFilesOrderByCustomFilterOption calling');
        this.getPickUpFilesOrderByCustomFilterOption();
      }
    }

    this.getProductionDropDownDataInitially();
    //this.getAdvertiserThroughASI();
  }

  pickupfilesOrderFormValidation() {
    this.pickupfilesOrderForm = this.formBuilder.group({
      'NewPickupInd': [null],
      'PickupMediaOrderId': [null],
      'AdvertiserAgencyInd': [null],
      'CreateExpectedInd': [null],
      'MaterialExpectedDate': [null],
      'TrackingNumber': [null],
      'ChangesInd': [null],
      'OnHandInd': [null],
      'OrigFileName': [null],
      'ProofFileName': [null],
      'FinalFileName': [null],
      'WebAdUrl': [null],
      'TearSheets': [null],
      'HeadLine': [null],
      'PageNumber': [null],
      'ProductionComment': [null],
      'MaterialContactId': [null],
      'MediaOrderId': [null],
      'ClassifiedText': [null],
      'SeparationId': [null],
      'PositionId': [null],
      'pickUpFilesOrderProductionStages': [null],
      'ProductionStatusId': [null],
    });
  }

  getToken() {
    try {
      this.updatedBy = environment.CurrentUserName;
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  getProductionDropDownDataInitially() {
    try {
      this.incompleteOrderService.getProductionDropdownInitially().subscribe(result => {
        if (result.StatusCode === 1) {
          this.hideloader();
          if (result.Data != null) {
            if (result.Data.length > 0) {
              const Data = result.Data;
              this.fillDropDownInitially(Data);
            } else {
              this.productionStatusList = [];
              this.positionsList = [];
              this.separationsList = [];
              this.productionStages1List = [];
            }
          } else {
            this.productionStatusList = [];
            this.positionsList = [];
            this.separationsList = [];
            this.productionStages1List = [];
          }
        } else {
          this.hideloader();
          this.productionStatusList = [];
          this.positionsList = [];
          this.separationsList = [];
          this.productionStages1List = [];
        }

      }, error => {
        this.hideloader();
        console.log(error);
      });
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  fillDropDownInitially(objData) {
    try {
      if (objData.length > 0) {
        const dbproductionStatus = objData[0].ProductionStatus;
        const dbProductionStages1 = objData[0].ProductionStages;
        const dbPositions = objData[0].Positions;
        const dbSeparations = objData[0].Separations;

        if (dbproductionStatus.length > 0) {
          this.productionStatusList = dbproductionStatus;
        } else {
          this.productionStatusList = [];
        }

        if (dbPositions.length > 0) {
          this.positionsList = dbPositions;
        } else {
          this.positionsList = [];
        }

        if (dbSeparations.length > 0) {
          this.separationsList = dbSeparations;
        } else {
          this.separationsList = [];
        }

        if (dbProductionStages1.length > 0) {
          this.productionStages1List = dbProductionStages1;
        } else {
          this.productionStages1List = [];
        }

      } else {
        this.productionStatusList = [];
        this.positionsList = [];
        this.separationsList = [];
        this.productionStages1List = [];
      }
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  getAdvertiserThroughASI() {
    try {
      if (this._globalClass.getmainPartyAndOrganizationData().length <= 0
        && this._globalClass.getmainAdvertisers().length <= 0
        && this._globalClass.getmainAgencies().length <= 0
        && this._globalClass.getmainBillingToContacts().length <= 0) {

        this.hasNext = true;
        this.offset = 0;
        this.getAdvertiserData();
      } else {
        this.partyAndOrganizationData = this._globalClass.getmainPartyAndOrganizationData();
        this.advertisers = this._globalClass.getmainAdvertisers();
        this.agencies = this._globalClass.getmainAgencies();
        this.personData = this._globalClass.getmainBillingToContacts();
      }

    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  getPickUpFilesOrderByCustomFilterOption() {
    this.selectedIssueMediaOrder = [];

    this.selectedCheckbox = [];
    this.selectedMediaOrderIds = [];
    this.selectedAdvertiserIds = [];
    this.selectedAgencyIds = [];

    this.resetValueChkChange();
    try {
      this.dataObject = {};
      this.dataObject.MediaAssetId = this.appComponent.productionForm.controls['MediaAssetId'].value;
      this.dataObject.IssueDateId = this.appComponent.productionForm.controls['IssueDateId'].value;
      this.dataObject.AdTypeId = this.appComponent.productionForm.controls['AdTypeId'].value;
      this.dataObject.RepId = this.appComponent.productionForm.controls['RepId'].value;

      const obj = {
        AdTypeId: this.dataObject.AdTypeId,
        IssueDateId: this.dataObject.IssueDateId,
        MediaAssetId: this.dataObject.MediaAssetId,
        RepId: this.dataObject.RepId,
      };

      this.showloader();
      this.pickupFilesOrderService.getPickUpFilesOrderByCustomFilterOption(obj).subscribe(result => {
        this.hideloader();
        this._globalClass.setPageFraction(0);
        if (result.StatusCode === 1) {
          this.hideloader();
          if (result.Data != null) {
            if (result.Data.length > 0) {

              const data = result.Data;
              this.tempArray = [];

              data.forEach((element, key) => {
                this.pickUpFilesOrder = element;
                const t = this._globalClass.getPageFraction() + element.PageFraction;
                this._globalClass.setPageFraction(t);

                this.appComponent.pageFraction = t;

                let TN = '';
                let MED = '';
                let MediaAssetIssueDate = '';

                if (element.TrackingNumber != null && element.TrackingNumber !== '') {
                  TN = ' | ' + (element.TrackingNumber).toString();
                }

                if (element.MaterialExpectedDate != null && element.MaterialExpectedDate !== '') {
                  MED = ' | ' + Common.getDateInFormat(element.MaterialExpectedDate);
                }

                if (element.PickupIndFromPg != null && element.PickupIndFromPg !== '') {
                  MediaAssetIssueDate = element.PickupIndFromMediaAsset
                    + ' | ' + Common.getDateInFormat(element.PickupIndFromIssueDate)
                    + ' | ' + element.PickupIndFromPg;
                } else if (element.PickupIndFromIssueDate !== null && element.PickupIndFromIssueDate !== ''
                  && element.PickupIndFromIssueDate !== '0001-01-01T00:00:00') {
                  MediaAssetIssueDate = element.PickupIndFromMediaAsset + ' | ' + Common.getDateInFormat(element.PickupIndFromIssueDate);
                } else if (element.PickupIndFromMediaAsset != null && element.PickupIndFromMediaAsset !== '') {
                  MediaAssetIssueDate = element.PickupIndFromMediaAsset;
                }

                if (element.NewPickupInd) {
                  if (element.CreateExpectedInd) {
                    if (element.OnHandInd) {
                      this.pickUpFilesOrder.MaterialInfo = 'New material we create on hand ' + TN + MED;
                    } else {
                      this.pickUpFilesOrder.MaterialInfo = 'New material we create ' + TN + MED;
                    }
                  } else {
                    if (element.OnHandInd) {
                      this.pickUpFilesOrder.MaterialInfo = 'New material expected on hand ' + TN + MED;
                    } else {
                      this.pickUpFilesOrder.MaterialInfo = 'New material expected ' + TN + MED;
                    }
                  }
                } else if (element.ChangesInd) {
                  this.pickUpFilesOrder.MaterialInfo = 'Pick-up with changes from ' + MediaAssetIssueDate;
                } else if (MediaAssetIssueDate !== '' && MediaAssetIssueDate != null) {
                  this.pickUpFilesOrder.MaterialInfo = 'Pick-up from ' + MediaAssetIssueDate;
                } else {
                  this.pickUpFilesOrder.MaterialInfo = 'Pick-up';
                }

                this.tempArray.push(this.pickUpFilesOrder);
                this.pickUpFilesOrder = [];
              });

              this.pickUpFilesOrders = this.tempArray;
              this.setAdvertiserNamePickUpFilesOrders(this.tempArray);

            } else {
              this.pickUpFilesOrders = [];
            }
          } else {
            this.pickUpFilesOrders = [];
          }
        } else if (result.StatusCode === 3) {
          this.pickUpFilesOrders = [];
        } else {
          this.toastr.error(result.Message, '');
        }
      }, error => {
        this.hideloader();
        console.log(error);
      });
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  setAdvertiserNamePickUpFilesOrders(objOrders) {
    try {
      if (objOrders.length > 0) {
        objOrders.forEach((element, index) => {
          const advertiserId = element.AdvertiserId;
          this.incompleteOrderService.getAdvertiserDetail_ByAdvertiserId(this.offset, this.websiteRoot,advertiserId).subscribe(result => {
            if (result != null && result !== undefined && result !== '') {
              const ItemData = result.Items.$values;
              const advertiserData=[];
              if (ItemData.length > 0) {
                ItemData.forEach(itemdatavalue => {
                  const type = itemdatavalue.$type;
                  if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                    advertiserData.push(itemdatavalue);
                  }
                  if(advertiserData.length>0)
                  {
                    const objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id = ?', [advertiserData, advertiserId]);
                    if (objAdvertiser.length > 0) {
                      console.log(objAdvertiser);
                      const aName=objAdvertiser[0].OrganizationName;
                      this.pickUpFilesOrders[index]['AdvertiserName'] = aName;
                    }
                  }
                });
              }
            }
          }, error => {
            console.log(error);
          });
        });
      } else {
        this.pickUpFilesOrders = [];
      }
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  getAdvertiserData() {
    try {
      this.showloader();
      this.incompleteOrderService.getAllAdvertiser1(this.offset, this.websiteRoot).subscribe(result => {
        if (result !== null && result !== undefined && result !== '') {
          const ItemData = result.Items.$values;
          if (ItemData.length > 0) {

            ItemData.forEach(element => {
              this.partyAndOrganizationData.push(element);
              const type = element.$type;
              if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                this.advertisers.push(element);
                this.agencies.push(element);
              } else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                this.billingToContactsList.push(element);
              }
            });

            this.offset = result.Offset;
            const NextOffset = this.offset + 500;
            if (result.Count === 500) {
              this.offset = NextOffset;
              this.getAdvertiserData();
            } else {
              this.hideloader();
              this._globalClass.setmainPartyAndOrganizationData(this.partyAndOrganizationData);
              this._globalClass.setmainAdvertisers(this.advertisers);
              this._globalClass.setmainAgencies(this.agencies);
              this._globalClass.setmainBillingToContacts(this.billingToContactsList);
            }
          } else {
            this.hideloader();
            this._globalClass.setmainPartyAndOrganizationData(this.partyAndOrganizationData);
            this._globalClass.setmainAdvertisers(this.advertisers);
            this._globalClass.setmainAgencies(this.agencies);
            this._globalClass.setmainBillingToContacts(this.billingToContactsList);
          }
        } else {
          this.hideloader();
          this.advertisers = [];
          this.agencies = [];
          this.billingToContactsList = [];
          this.partyAndOrganizationData = [];
        }
      }, error => {
        this.hideloader();
        console.log(error);
      });
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  checkboxChange(objPickUpFilesOrder) {
    try {
      const qId = objPickUpFilesOrder.IssueDateId;
      const mId = objPickUpFilesOrder.MediaAssetId;
      const mOrderId = objPickUpFilesOrder.MediaOrderId;
      const advertiserId = objPickUpFilesOrder.AdvertiserId;
      const agencyId = objPickUpFilesOrder.BT_ID;
      const buyId = objPickUpFilesOrder.BuyId;
      this.buyId = buyId;

      const idx = this.selectedCheckbox.indexOf(mOrderId);

      if (idx > -1) {
        this.selectedCheckbox.splice(idx, 1);
        this.selectedMediaOrderIds.forEach((element, index) => {
          // tslint:disable-next-line:radix
          if (parseInt(element) === parseInt(mOrderId)) {
            this.selectedMediaOrderIds.splice(index, 1);
            this.selectedAdvertiserIds.splice(index, 1);
            this.selectedAgencyIds.splice(index, 1);
          }
        });

        if (this.selectedCheckbox.length < 1) {
          this.resetValueChkChange();
        }

        if (this.selectedCheckbox.length === 1) {
          this.selectedMediaOrderId = this.selectedMediaOrderIds[0];
          this.selectedAdvertiserId = this.selectedAdvertiserIds[0];
          this.selectedAgencyId = this.selectedAgencyIds[0];
          console.log("checkboxChange:"+this.selectedAdvertiserId+"-"+ this.selectedAgencyId);
          this.getOrdersNotInBuyer(buyId, this.selectedAdvertiserId);
          const changeBillToValue = this.pickupfilesOrderForm.controls['AdvertiserAgencyInd'].value;
          this.getBillToContact(changeBillToValue);
          this.setProductionNoClick = false;
        }

        if (this.selectedCheckbox.length > 1) {
          this.resetValueChkChange();
          this.selectedAdvertiserIds = [];
        }

      } else {
        this.selectedCheckbox.push(mOrderId);
        this.selectedMediaOrderIds.push(mOrderId);
        this.selectedAdvertiserIds.push(advertiserId);
        this.selectedAgencyIds.push(agencyId);

        if (this.selectedCheckbox.length === 1) {
          this.selectedMediaOrderId = this.selectedMediaOrderIds[0];
          this.selectedAdvertiserId = this.selectedAdvertiserIds[0];
          this.selectedAgencyId = this.selectedAgencyIds[0];
          this.getOrdersNotInBuyer(buyId, this.selectedAdvertiserId);
          const changeBillToValue = this.pickupfilesOrderForm.controls['AdvertiserAgencyInd'].value;
          console.log("checkboxChange:"+this.selectedAdvertiserId+"-"+ this.selectedAgencyId);
          this.getBillToContact(changeBillToValue);
          this.setProductionNoClick = false;
        }
      }

      if (this.selectedCheckbox.length > 1) {
        this.resetValueChkChange();
      }

    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  resetValueChkChange() {
    try {
      this.selectedMediaOrderId = '';
      this.selectedAdvertiserId = '';

      this.selectedAgencyId = '';
      this.setProductionNoClick = true;
    } catch (error) {
      this. hideloader();
      console.log(error);
    }

    try {
      this.pickUpFilesOrder = {};

      this.buyId = 0;
      this.completed = null;
      this.pickupfilesOrderForm.reset();

      this.pickupfilesOrderForm.controls['NewPickupInd'].setValue('1');
      this.pickupfilesOrderForm.controls['CreateExpectedInd'].setValue('0');
      this.pickupfilesOrderForm.controls['AdvertiserAgencyInd'].setValue('1');

      this.pickupfilesOrderForm.controls['OnHandInd'].setValue(false);
      this.pickupfilesOrderForm.controls['ChangesInd'].setValue(false);

      this.pickUpFilesOrder_ProductionStagesList = [];

      this.originalFile = null;
      this.originalFileImageSrc = null;
      this.originalFileExtension = null;

      this.finalFile = null;
      this.finalFileExtension = null;
      this.finalFileImageSrc = null;

      this.proofFile = null;
      this.proofFileExtension = null;
      this.proofFileImageSrc = null;

      this.tempPdfPathForOriginal = null;
      this.tempPdfPathForProof = null;
      this.tempPdfPathForFinal = null;

      this.buttonText = 'Add';
      this.setProductionNoClick = true;
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  getOrdersNotInBuyer(buyId, adid) {
    try {
      this.incompleteOrderService.getordersnotinbuyer(buyId, adid).subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data != null) {
            if (result.Data.length > 0) {

              this.tempArray = [];

              result.Data.forEach((element, key) => {
                const issueDate = Common.getDateInFormat(element.IssueDate);
                const pageNo = element.PageNumber == null ? '' : element.PageNumber;
                if (pageNo !== null && pageNo !== undefined && pageNo !== '') {
                  this.mediaAssetModel.MediaAssetIssueDate = element.MediaAssetName + ' | ' + issueDate + ' | ' + pageNo;
                } else {
                  this.mediaAssetModel.MediaAssetIssueDate = element.MediaAssetName + ' | ' + issueDate;
                }
                this.mediaAssetModel.MediaOrderId = element.MediaOrderId;
                this.tempArray.push(this.mediaAssetModel);
                this.mediaAssetModel = {};
              });

              this.mediaAssetsList = this.tempArray;

            } else {
              this.mediaAssetsList = [];
            }
          } else {
            this.mediaAssetsList = [];
          }

        } else if (result.StatusCode === 3) {
          this.mediaAssetsList = [];
        } else {
          this.mediaAssetsList = [];
          this.toastr.error(result.Message, 'Error!');
        }
        this.getMediaOrderProduction(this.selectedMediaOrderId);

      }, error => {
        this. hideloader();
        console.log(error);
      });
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  getBillToContact(changeBillToValue) {
    try {
      if (changeBillToValue === '0') {
        changeBillToValue = this.pickupfilesOrderForm.controls['AdvertiserAgencyInd'].value;
      }
      const advertiserData=[];
      let id = '0';
      const temp = changeBillToValue;
      if (temp === '1') {
        changeBillToValue = 1;
      } else if (temp === '0') {
        changeBillToValue = 0;
      }
      if (parseInt(changeBillToValue) === 0 || parseInt(changeBillToValue) === 1) {
        this.billingToContactsList = [];
        // tslint:disable-next-line:radix
        if (parseInt(changeBillToValue) === 1) {
          id = this.selectedAdvertiserId;
          // tslint:disable-next-line:radix
        } else if (parseInt(changeBillToValue) === 0) {
          id = this.selectedAgencyId;
        }
        console.log("selected id:"+ id);
        this.incompleteOrderService.getAdvertiserDetail_ByAdvertiserId(this.offset, this.websiteRoot,id).subscribe(result => {
          if (result != null && result !== undefined && result !== '') {
            const ItemData = result.Items.$values;
            if (ItemData.length > 0) {
              ItemData.forEach(itemdatavalue => {
                const type = itemdatavalue.$type;
                if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                  advertiserData.push(itemdatavalue);
                }
                if(advertiserData.length>0)
                {
                  const objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id = ?', [advertiserData, id]);
                  if (objAdvertiser.length > 0) {
                    this.selectedAdvertiserAgencyName=objAdvertiser[0].OrganizationName;
                    this.organizationWiseParty = [];
                    console.log("Selected Name:"+ this.selectedAdvertiserAgencyName);
                    if(this.selectedAdvertiserAgencyName!=='')
                    {
                      this.incompleteOrderService.getAdvertiserDetail_ByAdvertiserName(0,this.websiteRoot,this.selectedAdvertiserAgencyName).subscribe(result => {
                        if (result != null && result !== undefined && result !== '') {
                          const ItemData = result.Items.$values;
                          for (const itemdatavalue of ItemData) {
                            const type = itemdatavalue.$type;
                            if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                              this.organizationWiseParty.push(itemdatavalue);
                            }
                          }
                          if(this.organizationWiseParty.length>0)
                          {
                            this.billingToContactsList = this.organizationWiseParty;
                          }
                        } else {
                          this.billingToContactsList = [];
                        }
                      }, error => {
                        console.log(error);
                      });
                    }
                  }
                }
              });
            }
          }
        }, error => {
          console.log(error);
        });
      }
      // // tslint:disable-next-line:radix
      // if (parseInt(changeBillToValue) === 0 || parseInt(changeBillToValue) === 1) {
      //   this.billingToContactsList = [];
      //   // tslint:disable-next-line:radix
      //   if (parseInt(changeBillToValue) === 1) {
      //     id = this.selectedAdvertiserId;
      //     // tslint:disable-next-line:radix
      //   } else if (parseInt(changeBillToValue) === 0) {
      //     id = this.selectedAgencyId;
      //   }
      // }

      // const partyData = this.partyAndOrganizationData;
      // this.organizationWiseParty = [];

      // // tslint:disable-next-line:radix
      // if (id !== undefined && parseInt(id) > 0) {
      //   partyData.forEach(element => {
      //     if (element.PrimaryOrganization !== undefined) {
      //       if (element.PrimaryOrganization.OrganizationPartyId !== undefined) {
      //         const organizationID = element.PrimaryOrganization.OrganizationPartyId;
      //         if (id === organizationID) {
      //           this.organizationWiseParty.push(element);
      //         }
      //       }
      //     }
      //   });
      // } else {
      //   this.organizationWiseParty = [];
      // }
      // this.billingToContactsList = this.organizationWiseParty;
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  getMediaOrderProduction(mOrderId) {
    try {
      this.pickupFilesOrderService.getInCompleteOrderByMediaOrderId(mOrderId).subscribe(result => {
        if (result.Data != null) {
          this.buttonText = 'Update';
          this.pickUpFilesOrder = result.Data;

          this.buyId = this.pickUpFilesOrder.BuyId;
          this.mediaOrderProductionDetailId = this.pickUpFilesOrder.MediaOrderProductionDetailId;
          this.completed = this.pickUpFilesOrder.Completed;

          this.pickupfilesOrderForm.controls['pickUpFilesOrderProductionStages'].setValue(null);

          /* --------------------------------------------------------*/

          if (this.pickUpFilesOrder.TrackingNumber === undefined
            || this.pickUpFilesOrder.TrackingNumber === ''
            || this.pickUpFilesOrder.TrackingNumber === null) {
            this.pickupfilesOrderForm.controls['TrackingNumber'].setValue('0');
          } else {
            if (!isNaN(this.pickUpFilesOrder.TrackingNumber)) {
              // tslint:disable-next-line:radix
              this.pickupfilesOrderForm.controls['TrackingNumber'].setValue(parseInt(this.pickUpFilesOrder.TrackingNumber));
            } else {
              this.pickupfilesOrderForm.controls['TrackingNumber'].setValue('');
            }
          }

          if (this.pickUpFilesOrder.WebAdUrl === undefined
            || this.pickUpFilesOrder.WebAdUrl === ''
            || this.pickUpFilesOrder.WebAdUrl === null) {
            this.pickupfilesOrderForm.controls['WebAdUrl'].setValue('');
          } else {
            this.pickupfilesOrderForm.controls['WebAdUrl'].setValue(this.pickUpFilesOrder.WebAdUrl);
          }

          if (this.pickUpFilesOrder.HeadLine === undefined
            || this.pickUpFilesOrder.HeadLine === ''
            || this.pickUpFilesOrder.HeadLine === null) {
            this.pickupfilesOrderForm.controls['HeadLine'].setValue('');
          } else {
            this.pickupfilesOrderForm.controls['HeadLine'].setValue(this.pickUpFilesOrder.HeadLine);
          }

          if (this.pickUpFilesOrder.TearSheets === undefined
            || this.pickUpFilesOrder.TearSheets === ''
            || this.pickUpFilesOrder.TearSheets === null) {
            this.pickupfilesOrderForm.controls['TearSheets'].setValue('');
          } else {
            this.pickupfilesOrderForm.controls['TearSheets'].setValue(this.pickUpFilesOrder.TearSheets);
          }

          if (this.pickUpFilesOrder.PageNumber === undefined
            || this.pickUpFilesOrder.PageNumber === ''
            || this.pickUpFilesOrder.PageNumber === null) {
            this.pickupfilesOrderForm.controls['PageNumber'].setValue('');
          } else {
            this.pickupfilesOrderForm.controls['PageNumber'].setValue(this.pickUpFilesOrder.PageNumber);
          }

          if (this.pickUpFilesOrder.ClassifiedText === undefined
            || this.pickUpFilesOrder.ClassifiedText === ''
            || this.pickUpFilesOrder.ClassifiedText === null) {
            this.pickupfilesOrderForm.controls['ClassifiedText'].setValue('');
          } else {
            this.pickupfilesOrderForm.controls['ClassifiedText'].setValue(this.pickUpFilesOrder.ClassifiedText);
          }

          if (this.pickUpFilesOrder.ProductionComment === undefined
            || this.pickUpFilesOrder.ProductionComment === ''
            || this.pickUpFilesOrder.ProductionComment === null) {
            this.pickupfilesOrderForm.controls['ProductionComment'].setValue('');
          } else {
            this.pickupfilesOrderForm.controls['ProductionComment'].setValue(this.pickUpFilesOrder.ProductionComment);
          }

          /* --------------------------------------------------------*/

          // Radio button setting, check box setting
          if (this.pickUpFilesOrder.NewPickupInd === false
            || this.pickUpFilesOrder.NewPickupInd === null
            || this.pickUpFilesOrder.NewPickupInd === undefined) {
            this.dvNew = true;
            this.dvPickup = false;
            this.pickupfilesOrderForm.controls['NewPickupInd'].setValue('0');
            this.pickupfilesOrderForm.controls['PickupMediaOrderId'].enable();
          } else if (this.pickUpFilesOrder.NewPickupInd === true) {
            this.dvNew = false;
            this.dvPickup = true;
            this.pickupfilesOrderForm.controls['NewPickupInd'].setValue('1');
            this.pickupfilesOrderForm.controls['PickupMediaOrderId'].disable();
          }

          if (this.pickUpFilesOrder.AdvertiserAgencyInd === false
            || this.pickUpFilesOrder.AdvertiserAgencyInd === null
            || this.pickUpFilesOrder.AdvertiserAgencyInd === undefined) {
            this.pickupfilesOrderForm.controls['AdvertiserAgencyInd'].setValue('0');
          } else if (this.pickUpFilesOrder.AdvertiserAgencyInd === true) {
            this.pickupfilesOrderForm.controls['AdvertiserAgencyInd'].setValue('1');
          }

          if (this.pickUpFilesOrder.CreateExpectedInd === false
            || this.pickUpFilesOrder.CreateExpectedInd === null
            || this.pickUpFilesOrder.CreateExpectedInd === undefined) {

            this.pickupfilesOrderForm.controls['CreateExpectedInd'].setValue('0');
          } else if (this.pickUpFilesOrder.CreateExpectedInd === true) {
            this.pickupfilesOrderForm.controls['CreateExpectedInd'].setValue('1');
          }

          if (this.pickUpFilesOrder.OnHandInd === false
            || this.pickUpFilesOrder.OnHandInd === null
            || this.pickUpFilesOrder.OnHandInd === undefined) {
            this.pickupfilesOrderForm.controls['OnHandInd'].setValue(false);
          } else if (this.pickUpFilesOrder.OnHandInd === true) {
            this.pickupfilesOrderForm.controls['OnHandInd'].setValue(true);
          }

          if (this.pickUpFilesOrder.ChangesInd === false
            || this.pickUpFilesOrder.ChangesInd === null
            || this.pickUpFilesOrder.ChangesInd === undefined) {
            this.pickupfilesOrderForm.controls['ChangesInd'].setValue(false);
          } else if (this.pickUpFilesOrder.ChangesInd === true) {
            this.pickupfilesOrderForm.controls['ChangesInd'].setValue(true);
          }

          /* -------------------- FILE -------------------- */

          if (this.pickUpFilesOrder.OriginalFile != null) {
            const tempOriginal = this.pickUpFilesOrder.OriginalFile;
            const fileNameOriginal = tempOriginal.substring(tempOriginal.lastIndexOf('/')).split('/').pop();

            this.originalFile = this.pickUpFilesOrder.OriginalFile;
            this.extensionOriginal = fileNameOriginal.substring(fileNameOriginal.lastIndexOf('.'));
            this.pickupfilesOrderForm.controls['OrigFileName'].setValue(fileNameOriginal);
          } else {
            this.originalFile = null;
            this.extensionOriginal = null;
            this.pickupfilesOrderForm.controls['OrigFileName'].setValue(null);
          }

          if (this.pickUpFilesOrder.ProofFile != null) {
            const tempProof = this.pickUpFilesOrder.ProofFile;
            const fileNameProof = tempProof.substring(tempProof.lastIndexOf('/')).split('/').pop();

            this.proofFile = this.pickUpFilesOrder.ProofFile;
            this.extensionProof = fileNameProof.substring(fileNameProof.lastIndexOf('.'));
            this.pickupfilesOrderForm.controls['ProofFileName'].setValue(fileNameProof);
          } else {
            this.proofFile = null;
            this.extensionProof = null;
            this.pickupfilesOrderForm.controls['ProofFileName'].setValue(null);
          }

          if (this.pickUpFilesOrder.FinalFile != null) {
            const tempFinal = this.pickUpFilesOrder.FinalFile;
            const fileNameFinal = tempFinal.substring(tempFinal.lastIndexOf('/')).split('/').pop();

            this.finalFile = this.pickUpFilesOrder.FinalFile;
            this.extensionFinal = fileNameFinal.substring(fileNameFinal.lastIndexOf('.'));
            this.pickupfilesOrderForm.controls['FinalFileName'].setValue(fileNameFinal);
          } else {
            this.finalFile = null;
            this.extensionFinal = null;
            this.pickupfilesOrderForm.controls['FinalFileName'].setValue(null);
          }

          if (this.extensionOriginal === null) {
            this.isPdfTrueForOriginal = false;
            this.isImgTrueForOriginal = false;
          } else {
            if (this.extensionOriginal === '.pdf') {
              this.isPdfTrueForOriginal = true;
              this.isImgTrueForOriginal = false;
              this.tempPdfPathForOriginal = this.pickUpFilesOrder.OriginalFile;
            } else if (this.extensionOriginal === '.png'
              || this.extensionOriginal === '.jpg'
              || this.extensionOriginal === '.jpeg') {
              this.isPdfTrueForOriginal = false;
              this.isImgTrueForOriginal = true;
            } else {
              this.isPdfTrueForOriginal = true;
              this.isImgTrueForOriginal = false;
              this.tempPdfPathForOriginal = this.pickUpFilesOrder.OriginalFile;
            }
          }

          if (this.extensionProof === null) {
            this.isPdfTrueForProof = false;
            this.isImgTrueForProof = false;
          } else {
            if (this.extensionProof === '.pdf') {
              this.isPdfTrueForProof = true;
              this.isImgTrueForProof = false;
              this.tempPdfPathForProof = this.pickUpFilesOrder.ProofFile;
            } else if (this.extensionProof === '.png'
              || this.extensionProof === '.jpg'
              || this.extensionProof === '.jpeg') {
              this.isPdfTrueForProof = false;
              this.isImgTrueForProof = true;
            } else {
              this.isPdfTrueForProof = true;
              this.isImgTrueForProof = false;
              this.tempPdfPathForProof = this.pickUpFilesOrder.ProofFile;
            }
          }

          if (this.extensionFinal === null) {
            this.isPdfTrueForfinal = false;
            this.isImgTrueForfinal = false;
          } else {
            if (this.extensionFinal === '.pdf') {
              this.isPdfTrueForfinal = true;
              this.isImgTrueForfinal = false;
              this.tempPdfPathForFinal = this.pickUpFilesOrder.FinalFile;
            } else if (this.extensionFinal === '.png'
              || this.extensionFinal === '.jpg'
              || this.extensionFinal === '.jpeg') {
              this.isPdfTrueForfinal = false;
              this.isImgTrueForfinal = true;
            } else {
              this.isPdfTrueForfinal = true;
              this.isImgTrueForfinal = false;
              this.tempPdfPathForFinal = this.pickUpFilesOrder.FinalFile;
            }
          }
          /* -------------------- End FILE -------------------- */

          if (result.Data.MaterialExpectedDate !== undefined && result.Data.MaterialExpectedDate !== ''
            && result.Data.MaterialExpectedDate != null) {
            this.pickupfilesOrderForm.controls['MaterialExpectedDate'].setValue(Common.getDateInFormat(result.data.MaterialExpectedDate));
          } else {
            this.pickupfilesOrderForm.controls['MaterialExpectedDate'].setValue('');
          }

          this.originalFileImageSrc = this.pickUpFilesOrder.OriginalFile;
          this.proofFileImageSrc = this.pickUpFilesOrder.ProofFile;
          this.finalFileImageSrc = this.pickUpFilesOrder.FinalFile;

          /* ------------------------------------------------------ */
          if (!isNullOrUndefined(this.pickUpFilesOrder.PickupMediaOrderId)) {
            this.pickupfilesOrderForm.controls['PickupMediaOrderId'].setValue(this.pickUpFilesOrder.PickupMediaOrderId);
          } else {
            this.pickupfilesOrderForm.controls['PickupMediaOrderId'].setValue(null);
          }
          /* ------------------------------------------------------ */
          if (!isNullOrUndefined(this.pickUpFilesOrder.PositionId)) {
            this.pickupfilesOrderForm.controls['PositionId'].setValue(this.pickUpFilesOrder.PositionId);
          } else {
            this.pickupfilesOrderForm.controls['PositionId'].setValue(null);
          }
          /* ------------------------------------------------------ */
          if (!isNullOrUndefined(this.pickUpFilesOrder.SeparationId)) {
            this.pickupfilesOrderForm.controls['SeparationId'].setValue(this.pickUpFilesOrder.SeparationId);
          } else {
            this.pickupfilesOrderForm.controls['SeparationId'].setValue(null);
          }
          /* ------------------------------------------------------ */
          if (!isNullOrUndefined(this.pickUpFilesOrder.MaterialContactId)) {
            if (this.pickUpFilesOrder.AdvertiserAgencyInd === false
              || this.pickUpFilesOrder.AdvertiserAgencyInd === null
              || this.pickUpFilesOrder.AdvertiserAgencyInd === undefined) {
              this.getBillToContact('0');
            } else if (this.pickUpFilesOrder.AdvertiserAgencyInd === true) {
              this.getBillToContact('1');
            }
            this.pickupfilesOrderForm.controls['MaterialContactId'].setValue(this.pickUpFilesOrder.MaterialContactId.toString());
          } else {
            this.pickupfilesOrderForm.controls['MaterialContactId'].setValue(null);
          }
          /* ------------------------------------------------------ */
          if (!isNullOrUndefined(this.pickUpFilesOrder.ProductionStatusId)) {
            this.pickupfilesOrderForm.controls['ProductionStatusId'].setValue(this.pickUpFilesOrder.ProductionStatusId);

          } else {
            this.pickupfilesOrderForm.controls['ProductionStatusId'].setValue(null);
          }
          /* ------------------------------------------------------ */
          this.pickUpFilesOrder_ProductionStagesList = [];
          this.pickUpFilesOrder_ProductionStagesList = this.pickUpFilesOrder.ProductionStages;

        } else {
          this.buttonText = 'Add';
        }
      }, error => {
        this. hideloader();
        console.log(error + 'status : ' + error.status);
      });
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  getAllProductionStatus() {
    try {
      if (this._globalClass.getrProductionStatus().length <= 0) {
        this.incompleteOrderService.getAllProductionStatus().subscribe(result => {
          if (result.StatusCode === 1) {
            if (result.Data != null) {
              if (result.Data.length > 0) {
                this.productionStatusList = result.Data;
                this._globalClass.setrProductionStatus(this.productionStatusList);
              } else {
                this.productionStatusList = [];
              }
            } else {
              this.productionStatusList = [];
            }
          } else if (result.StatusCode === 3) {
            this.productionStatusList = [];
          } else {
            this.productionStatusList = [];
            this.toastr.error(result.Message, 'Error!');
          }
        }, error => {
          this. hideloader();
          console.log(error);
        });

      } else {
        this.productionStatusList = this._globalClass.getrProductionStatus();
      }
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  getAllPosition() {
    try {
      if (this._globalClass.getrPositions().length <= 0) {
        this.incompleteOrderService.getAllPosition().subscribe(result => {

          if (result.StatusCode === 1) {
            if (result.Data != null) {
              if (result.Data.length > 0) {
                this.positionsList = result.Data;
                this._globalClass.setrPositions(this.positionsList);
              } else {
                this.positionsList = [];
              }
            } else {
              this.positionsList = [];
            }
          } else if (result.StatusCode === 3) {
            this.positionsList = [];
          } else {
            this.positionsList = [];
            this.toastr.error(result.Message, 'Error!');
          }

        }, error => {
          this. hideloader();
          console.log(error);
        });
      } else {
        this.positionsList = this._globalClass.getrPositions();
      }
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  getAllSeparation() {
    try {
      if (this._globalClass.getrSeparations().length <= 0) {
        this.incompleteOrderService.getAllSeparation().subscribe(result => {
          if (result.StatusCode === 1) {
            if (result.Data != null) {
              if (result.Data.length > 0) {
                this.separationsList = result.Data;
                this._globalClass.setrSeparations(this.separationsList);
              } else {
                this.separationsList = [];
              }
            } else {
              this.separationsList = [];
            }
          } else if (result.StatusCode === 3) {
            this.separationsList = [];
          } else {
            this.separationsList = [];
            this.toastr.error(result.Message, 'Error!');
          }
        }, error => {
          this. hideloader();
          console.log(error);
        });
      } else {
        this.separationsList = this._globalClass.getrSeparations();
      }
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  getAllProductionStages() {
    try {
      if (this._globalClass.getrProductionStages1().length <= 0) {
        this.incompleteOrderService.getAllProductionStage().subscribe(result => {
          if (result.StatusCode === 1) {
            this.productionStages1List = result.Data;
            this._globalClass.setrProductionStages1(this.productionStages1List);
          } else {
            this.toastr.error(result.Message, 'Error');
          }
        }, error => {
          this. hideloader();
          console.log(error);
        });
      } else {
        this.productionStages1List = this._globalClass.getrProductionStages1();
      }
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  update() {
    try {
      let pickupMediaOrderId = this.pickupfilesOrderForm.controls['PickupMediaOrderId'].value;
      let newPickupInd = this.pickupfilesOrderForm.controls['NewPickupInd'].value;
      let advertiserAgencyInd = this.pickupfilesOrderForm.controls['AdvertiserAgencyInd'].value;
      let createExpectedInd = this.pickupfilesOrderForm.controls['CreateExpectedInd'].value;
      let materialExpectedDate = this.pickupfilesOrderForm.controls['MaterialExpectedDate'].value;
      let trackingNumber = this.pickupfilesOrderForm.controls['TrackingNumber'].value;
      let changesInd = this.pickupfilesOrderForm.controls['ChangesInd'].value;
      let onHandInd = this.pickupfilesOrderForm.controls['OnHandInd'].value;

      let origFileName = this.pickupfilesOrderForm.controls['OrigFileName'].value;
      let proofFileName = this.pickupfilesOrderForm.controls['ProofFileName'].value;
      let finalFileName = this.pickupfilesOrderForm.controls['FinalFileName'].value;

      let webAdUrl = this.pickupfilesOrderForm.controls['WebAdUrl'].value;
      let tearSheets = this.pickupfilesOrderForm.controls['TearSheets'].value;
      let headLine = this.pickupfilesOrderForm.controls['HeadLine'].value;
      let pageNumber = this.pickupfilesOrderForm.controls['PageNumber'].value;
      let productionComment = this.pickupfilesOrderForm.controls['ProductionComment'].value;
      let materialContactId = this.pickupfilesOrderForm.controls['MaterialContactId'].value;
      const classifiedText = this.pickupfilesOrderForm.controls['ClassifiedText'].value;
      const positionId = this.pickupfilesOrderForm.controls['PositionId'].value;
      const productionStatusId = this.pickupfilesOrderForm.controls['ProductionStatusId'].value;
      const separationId = this.pickupfilesOrderForm.controls['SeparationId'].value;

      let originalFile = this.originalFile;
      let originalFileExtension = this.originalFileExtension;

      let proofFile = this.proofFile;
      let proofFileExtension = this.proofFileExtension;

      let finalFile = this.finalFile;
      let finalFileExtension = this.finalFileExtension;

      if (pickupMediaOrderId != null && pickupMediaOrderId !== '' && pickupMediaOrderId !== undefined) {
        // tslint:disable-next-line:radix
        pickupMediaOrderId = parseInt(pickupMediaOrderId);
      } else {
        pickupMediaOrderId = null;
      }

      if (newPickupInd === '0') {
        newPickupInd = false;
      } else if (newPickupInd === '1') {
        newPickupInd = true;
      }

      if (advertiserAgencyInd === '0') {
        advertiserAgencyInd = false;
      } else if (advertiserAgencyInd === '1') {
        advertiserAgencyInd = true;
      }

      if (createExpectedInd === '0') {
        createExpectedInd = false;
      } else if (createExpectedInd === '1') {
        createExpectedInd = true;
      }

      if (materialExpectedDate === undefined || materialExpectedDate === null || materialExpectedDate === '') {
        materialExpectedDate = '';
      } else {
        materialExpectedDate = Common.getDate(materialExpectedDate);
      }

      if (trackingNumber === undefined || trackingNumber === null) {
        trackingNumber = '';
      } else {
        trackingNumber = (trackingNumber).toString();
      }

      if (changesInd === undefined || changesInd === '' || changesInd === null) {
        changesInd = null;
      }

      if (onHandInd === undefined || onHandInd === '' || onHandInd === null) {
        onHandInd = null;
      }

      if (originalFile === undefined || originalFile === null || originalFile === '') {
        origFileName = null;
        originalFile = null;
        originalFileExtension = null;
      } else {
        originalFileExtension = origFileName.substring(origFileName.lastIndexOf('.'));
      }

      if (proofFile === undefined || proofFile === null || proofFile === '') {
        proofFileName = null;
        proofFile = null;
        proofFileExtension = null;
      } else {
        proofFileExtension = proofFileName.substring(proofFileName.lastIndexOf('.'));
      }

      if (finalFile === undefined || finalFile === null || finalFile === '') {
        finalFileName = null;
        finalFile = null;
        finalFileExtension = null;
      } else {
        finalFileExtension = finalFileName.substring(finalFileName.lastIndexOf('.'));
      }

      if (webAdUrl === undefined || webAdUrl === '' || webAdUrl === null) {
        webAdUrl = '';
      }

      if (tearSheets === undefined || tearSheets === '' || tearSheets === null) {
        tearSheets = '';
      } else {
        // tslint:disable-next-line:radix
        tearSheets = parseInt(tearSheets);
      }

      if (headLine === undefined || headLine === '' || headLine === null) {
        headLine = '';
      }

      if (pageNumber === undefined || pageNumber === '' || pageNumber === null) {
        pageNumber = '';
      } else {
        pageNumber = (pageNumber).toString();
      }

      if (productionComment === undefined || productionComment === '' || productionComment === null) {
        productionComment = '';
      }

      let l_ProductionStatusId = null;

      if (productionStatusId != null && productionStatusId !== undefined) {
        l_ProductionStatusId = productionStatusId;
      }

      // tslint:disable-next-line:radix
      materialContactId = parseInt(materialContactId);

      const production = {
        PickupMediaOrderId: pickupMediaOrderId,
        NewPickupInd: newPickupInd,
        AdvertiserAgencyInd: advertiserAgencyInd,
        CreateExpectedInd: createExpectedInd,
        MaterialExpectedDate: materialExpectedDate,
        TrackingNumber: trackingNumber,
        ChangesInd: changesInd,
        OnHandInd: onHandInd,

        WebAdUrl: webAdUrl,
        TearSheets: tearSheets,
        HeadLine: headLine,
        PageNumber: pageNumber,
        ProductionComment: productionComment,
        MaterialContactId: materialContactId,
        ClassifiedText: classifiedText,
        PositionId: positionId,
        ProductionStages: this.pickUpFilesOrder_ProductionStagesList,
        ProductionStatusId: l_ProductionStatusId,
        SeparationId: separationId,

        Completed: this.completed,
        MediaOrderId: this.selectedMediaOrderId,
        MediaOrderProductionDetailId: this.mediaOrderProductionDetailId,

        OriginalFile: originalFile,
        OriginalFileExtension: originalFileExtension,
        OrigFileName: origFileName,

        ProofFile: proofFile,
        ProofFileExtension: proofFileExtension,
        ProofFileName: proofFileName,

        FinalFile: finalFile,
        FinalFileExtension: finalFileExtension,
        FinalFileName: finalFileName,
      };

      this.showloader();
      this.pickupFilesOrderService.updateProduction(production).subscribe(result => {
        if (result.StatusCode === 1) {
          this.hideloader();
          this.toastr.success('Order save successfully.', 'Success!');
          this.getPickUpFilesOrderByCustomFilterOption();
        } else {
          this.hideloader();
          this.toastr.error(result.Message, 'Error!');
        }
      }, error => {
        this.hideloader();
        console.log(error);
      });

    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  onChangeFrom() {
  }

  onChangeContact() {
  }

  onChangeProductionStatus() {
  }

  onChangeSeparation() {
  }

  onChangePosition() {
  }

  onChangeProductionStages() {
  }

  // --------- Add, Delete Production Stages ---------

  addProductionStages() {
    try {
      if (this.productionStages1List != null) {
        const comOrProdStge = this.pickupfilesOrderForm.controls['pickUpFilesOrderProductionStages'].value;
        // tslint:disable-next-line:max-line-length
        // tslint:disable-next-line:radix
        const data = alasql('SELECT * FROM ? AS add WHERE ProductionStageId = ?', [this.productionStages1List, parseInt(comOrProdStge)]);
        console.log('data',data);
        const final = {
          ProductionStagesName: data[0].Name,
          UpdatedDate: new Date(),
          // tslint:disable-next-line:radix
          ProductionStageId: parseInt(comOrProdStge),
          UpdateBy: this.updatedBy,
          MediaOrderProductionStagesId: '',
        };
        this.pickUpFilesOrder_ProductionStagesList.push(final);
        this.toastr.success('Production stage added successfully.', 'Success!');
        this.pickupfilesOrderForm.controls['pickUpFilesOrderProductionStages'].setValue(null);
      }
      if (this.pickUpFilesOrder_ProductionStagesList.length > 0) {
      } else {
      }
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  deleteProductionStage(productionStage) {
    try {
      this.pickUpFilesOrder_ProductionStagesList.forEach((element, index) => {
        if (element.UpdatedDate === productionStage.UpdatedDate && element.ProductionStagesName === productionStage.ProductionStagesName) {
          this.pickUpFilesOrder_ProductionStagesList.splice(index, 1);
        }
      });
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  // --------- End Of Add, Delete Production Stages ---------

  setEnableOrDisable(rdoName) {
    try {
      // tslint:disable-next-line:radix
      if (parseInt(rdoName) === 1) {
        this.dvNew = false;
        this.dvPickup = true;
        this.pickupfilesOrderForm.controls['PickupMediaOrderId'].disable();
        this.pickupfilesOrderForm.controls['ChangesInd'].setValue(false);
        this.pickupfilesOrderForm.controls['PickupMediaOrderId'].setValue(null);
        this.pickupfilesOrderForm.controls['CreateExpectedInd'].setValue('0');
        // tslint:disable-next-line:radix
      } else if (parseInt(rdoName) === 0) {
        this.dvNew = true;
        this.dvPickup = false;
        this.pickupfilesOrderForm.controls['PickupMediaOrderId'].enable();
        this.pickupfilesOrderForm.controls['CreateExpectedInd'].setValue('0');
        this.pickupfilesOrderForm.controls['OnHandInd'].setValue(false);
        this.pickupfilesOrderForm.controls['MaterialExpectedDate'].setValue('');
        this.pickupfilesOrderForm.controls['TrackingNumber'].setValue('');
      }
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  setToCompleteInserationOrder() {
    try {
      if (this.selectedCheckbox.length === 1) {
        const mId = this.selectedMediaOrderIds[0];
        this.showloader();
        this.pickupFilesOrderService.setOrderProductionStatusToInComplete(mId).subscribe(result => {
          if (result.StatusCode === 1) {
            this.hideloader();
            if (result.Data != null) {
              const Data = result.Data;
              this.resetValueChkChange();

              this.selectedIssueMediaOrder = [];
              this.selectedCheckbox = [];
              this.selectedMediaOrderIds = [];
              this.selectedAdvertiserIds = [];
              this.selectedAgencyIds = [];

              this.toastr.success('Mark as completed successfully.', 'Success!');
              this.getPickUpFilesOrderByCustomFilterOption();
            }
          } else {
            this.hideloader();
            this.toastr.error(result.Message, 'Error!');
          }
        }, error => {
          this.hideloader();
          console.log(error);
        });
      }
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  // ------------ Upload & Image Viewer-----------
  uploadImage(event: any) {
    try {
      if (event.target.files && event.target.files[0]) {

        const logoElement = event.target.files.item(0);
        const uploadType = logoElement.type;
        const filename = event.target.files.item(0).name;
        const ext = filename.substr(filename.lastIndexOf('.') + 1);

        if (uploadType !== undefined && uploadType != null) {
          if (logoElement.type.indexOf('application/pdf') !== -1) {
            const tmppath = URL.createObjectURL(event.target.files[0]);
            this.tempPdfPathForOriginal = tmppath;
            this.isPdfTrueForOriginal = true;
            this.isImgTrueForOriginal = false;
          } else if (logoElement.type.indexOf('image') !== -1) {
            this.isImgTrueForOriginal = true;
            this.isPdfTrueForOriginal = false;
          } else {
            this.isImgTrueForOriginal = false;
            this.isPdfTrueForOriginal = false;
          }

          const reader = new FileReader();
          reader.onload = (e: any) => {
            if (this.isImgTrueForOriginal === false && this.isPdfTrueForOriginal === false) {
              this.originalFileImageSrc = '';
            } else {
              this.originalFileImageSrc = e.target.result;
            }
            const strImage = e.target.result.substring((e.target.result.indexOf(',') + 1));
            this.originalFile = strImage;
          };
          reader.readAsDataURL(event.target.files[0]);

          this.pickupfilesOrderForm.controls['OrigFileName'].setValue(filename);
          this.originalFileExtension = ext;
        }
      }
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  uploadImageforfinal(event: any) {
    try {
      if (event.target.files && event.target.files[0]) {

        const logoElement = event.target.files.item(0);
        const uploadType = logoElement.type;
        const filename = event.target.files.item(0).name;
        const ext = filename.substr(filename.lastIndexOf('.') + 1);

        if (uploadType !== undefined && uploadType != null) {
          if (logoElement.type.indexOf('application/pdf') !== -1) {
            const tmppath = URL.createObjectURL(event.target.files[0]);
            this.tempPdfPathForFinal = tmppath;
            this.isPdfTrueForfinal = true;
            this.isImgTrueForfinal = false;
          } else if (logoElement.type.indexOf('image') !== -1) {
            this.isImgTrueForfinal = true;
            this.isPdfTrueForfinal = false;
          } else {
            this.isImgTrueForfinal = false;
            this.isPdfTrueForfinal = false;
          }

          const reader = new FileReader();
          reader.onload = (e: any) => {
            if (this.isImgTrueForfinal === false && this.isPdfTrueForfinal === false) {
              this.finalFileImageSrc = '';
            } else {
              this.finalFileImageSrc = e.target.result;
            }
            const strImage = e.target.result.substring((e.target.result.indexOf(',') + 1));
            this.finalFile = strImage;
          };
          reader.readAsDataURL(event.target.files[0]);

          this.pickupfilesOrderForm.controls['FinalFileName'].setValue(filename);
          this.finalFileExtension = ext;
        }
      }
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  uploadImageforProof(event: any) {
    try {
      if (event.target.files && event.target.files[0]) {

        const logoElement = event.target.files.item(0);
        const uploadType = logoElement.type;
        const filename = event.target.files.item(0).name;
        const ext = filename.substr(filename.lastIndexOf('.') + 1);

        if (uploadType !== undefined && uploadType != null) {
          if (logoElement.type.indexOf('application/pdf') !== -1) {
            const tmppath = URL.createObjectURL(event.target.files[0]);
            this.tempPdfPathForProof = tmppath;
            this.isPdfTrueForProof = true;
            this.isImgTrueForProof = false;
          } else if (logoElement.type.indexOf('image') !== -1) {
            this.isImgTrueForProof = true;
            this.isPdfTrueForProof = false;
          } else {
            this.isImgTrueForProof = false;
            this.isPdfTrueForProof = false;
          }

          const reader = new FileReader();
          reader.onload = (e: any) => {
            if (this.isImgTrueForProof === false && this.isPdfTrueForProof === false) {
              this.proofFileImageSrc = '';
            } else {
              this.proofFileImageSrc = e.target.result;
            }
            const strImage = e.target.result.substring((e.target.result.indexOf(',') + 1));
            this.proofFile = strImage;
          };
          reader.readAsDataURL(event.target.files[0]);

          this.pickupfilesOrderForm.controls['ProofFileName'].setValue(filename);
          this.proofFileExtension = ext;
        }
      }
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  showImageInViewerForOriginalFile() {
    try {
      if (this.isPdfTrueForOriginal === false && this.isImgTrueForOriginal === false) {
        if (this.originalFileImageSrc !== '' && this.originalFileImageSrc !== undefined && this.originalFileImageSrc != null) {
          window.open(this.tempPdfPathForOriginal, '_blank');
        }
      } else {
        if (this.isPdfTrueForOriginal === true) {
          window.open(this.tempPdfPathForOriginal, '_blank');
        } else if (this.isImgTrueForOriginal === true) {
          // tslint:disable-next-line:no-unused-expression
          new imgviewerfun('docs-pictures-originalFile', 'docs-buttons-originalFile');
        }
      }
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  showImageInViewerForProofFile() {
    try {
      if (this.isPdfTrueForProof === false && this.isImgTrueForProof === false) {
        if (this.proofFileImageSrc !== '' && this.proofFileImageSrc !== undefined && this.proofFileImageSrc != null) {
          window.open(this.tempPdfPathForProof, '_blank');
        }
      } else {
        if (this.isPdfTrueForProof === true) {
          window.open(this.tempPdfPathForProof, '_blank');
        } else if (this.isImgTrueForProof === true) {
          // tslint:disable-next-line:no-unused-expression
          new imgviewerfun('docs-pictures-proofFile', 'docs-buttons-proofFile');
        }
      }
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  showImageInViewerForFinalFile() {
    try {
      if (this.isPdfTrueForfinal === false && this.isImgTrueForfinal === false) {
        if (this.finalFileImageSrc !== '' && this.finalFileImageSrc !== undefined && this.finalFileImageSrc != null) {
          window.open(this.tempPdfPathForFinal, '_blank');
        }
      } else {
        if (this.isPdfTrueForfinal === true) {
          window.open(this.tempPdfPathForFinal, '_blank');
        } else if (this.isImgTrueForfinal === true) {
          // tslint:disable-next-line:no-unused-expression
          new imgviewerfun('docs-pictures-finalFile', 'docs-buttons-finalFile');
        }
      }
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  // ------------ Export :: Excel & PDF-----------
  exporttoExcel() {
    try {
      const pickupOrders = [];
      this.pickUpFilesOrders.forEach((element, index) => {
        this.objModelExport = {};
        this.objModelExport.AdvertiserName = element.AdvertiserName;
        this.objModelExport.AdDetails = element.AdTypeName + ' | ' + element.AdcolorName + ' | ' + element.AdSizeName;
        this.objModelExport.MaterialInfo = element.MaterialInfo;
        this.objModelExport.Rep = element.RepName;
        this.objModelExport.Position = (element.PositionName==null?'':element.PositionName);
        pickupOrders.push(this.objModelExport);
      });
      alasql('SELECT * INTO XLSX("PickUpFilesOrders.xlsx",{headers:true}) FROM ?', [pickupOrders]);
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  exportToPdf() {
    try {
      const tmpPickUpFilesOrdersForAutoTable = [];
      let obj = [];
      this.pickUpFilesOrders.forEach((element, index) => {
        obj = [];
        obj.push(element.AdvertiserName == null ? '' : element.AdvertiserName);
        obj.push(element.AdTypeName + ' | ' + element.AdcolorName + ' | ' + element.AdSizeName);
        obj.push(element.MaterialInfo);
        obj.push(element.RepName == null ? '' : element.RepName);
        obj.push(element.PositionName == null ? '' : element.PositionName);
        tmpPickUpFilesOrdersForAutoTable.push(obj);
      });

      const columns = ['Advertiser', 'AdDetails', 'Material Info', 'Rep', 'Position'];
      // Only pt supported (not mm or in)
      const doc = new jsPDF('p', 'pt');
      doc.autoTable(columns, tmpPickUpFilesOrdersForAutoTable, {
        theme: 'plain'
      });
      doc.save('PickUpFilesOrders.pdf');
    } catch (error) {
      this. hideloader();
      console.log(error);
    }
  }

  ngOnDestroy() {
    this.subscriptionForPickupExport.unsubscribe();
    this.subscriptionForPickupOrder.unsubscribe();
  }

  showloader() {
    this.appComponent.isLoading = true;
  }

  hideloader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 1000);
  }
}
