import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { Subscription } from 'rxjs';
import { ToastrService, ToastrModule } from 'ngx-toastr';
import { AppComponent } from '../../app.component';
import { IncompleteOrderService } from '../../services/incomplete-order.service';
import { ProductionViewtabService } from '../../services/production-viewtab.service';
import { GlobalClass } from '../../GlobalClass';
import * as Common from '../../common/common';
import * as alasql from 'alasql';
import { isNullOrUndefined } from 'util';
declare var imgviewerfun: any;
declare var jsPDF: any;

@Component({
  selector: 'asi-incomplete-ordertab',
  templateUrl: './incomplete-ordertab.component.html',
  styleUrls: ['./incomplete-ordertab.component.css']
})
export class IncompleteOrdertabComponent implements OnInit, OnDestroy {
  mediaOrderProductionDetailId: any;

  subscriptionForCustomOrder: Subscription;
  subscriptionForCustomExport: Subscription;

  inCompleteForm: FormGroup;

  inCompleteOrders: any[] = [];
  inCompleteOrder: any = {};
  mediaAssetsList = [];
  billingToContactsList = [];
  productionStatusList = [];
  separationsList = [];
  positionsList = [];
  productionStages1List = [];
  inCompleteOrder_ProductionStagesList = [];
  issueDatesList = [];

  selectedCheckbox = [];
  selectedMediaOrderIds = [];
  selectedAdvertiserIds = [];
  selectedAgencyIds = [];

  partyAndOrganizationData = [];
  tempArray = [];
  advertisers = [];
  agencies = [];
  personData = [];
  organizationWiseParty = [];

  dataObject: any = {};
  selectedIssueMediaOrder: any[] = [];
  mediaAssetModel: any = {};
  objModelExport: any = {};

  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';

  originalFile = '';
  originalFileImageSrc = '';
  originalFileExtension = '';

  finalFile = null;
  finalFileExtension = '';
  finalFileImageSrc = '';

  proofFile = null;
  proofFileExtension = '';
  proofFileImageSrc = '';

  extensionOriginal: string;
  extensionProof: string;
  extensionFinal: string;

  tempPdfPathForOriginal = '';
  tempPdfPathForProof = '';
  tempPdfPathForFinal = '';

  buttonText = 'Add';

  selectedMediaOrderId = '';
  selectedAdvertiserId = '';
  selectedAgencyId = '';
  selectedAdvertiserAgencyName='';
  buyId = 0;
  updatedBy = '';

  setProductionNoClick = true;
  flag = true;
  dvNew = false;
  dvPickup = true;
  isPdfTrueForProof = false;
  isPdfTrueForfinal = false;
  isPdfTrueForOriginal = false;
  isImgTrueForOriginal = false;
  isImgTrueForfinal = false;
  isImgTrueForProof = false;
  hasNext: boolean;
  offset: number;

  constructor(private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public appComponent: AppComponent,
    private incompleteOrderService: IncompleteOrderService,
    private productionViewtabService: ProductionViewtabService,
    private _globalClass: GlobalClass) {
    ToastrModule.forRoot();
  }

  ngOnInit() {
    this.inCompleteFormValidation();
    this.inCompleteForm.controls['PickupMediaOrderId'].disable();
    try {
      this.inCompleteOrder = {};
      this.inCompleteOrder.ProductionStages = [];
      this.inCompleteOrder.NewPickupInd = '1';
      this.inCompleteOrder.CreateExpectedInd = '0';
      this.inCompleteOrder.AdvertiserAgencyInd = '1';
      this.inCompleteOrder.OriginalFileExtension = '';
    } catch (error) {
      this.hideloader();
      console.log(error);
    }

    this.inCompleteForm.controls['NewPickupInd'].setValue('1');
    this.inCompleteForm.controls['CreateExpectedInd'].setValue('0');
    this.inCompleteForm.controls['AdvertiserAgencyInd'].setValue('1');

    try {
      this.updatedBy = environment.CurrentUserName;
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      this.hideloader();
      console.log(error);
    }

    this.subscriptionForCustomOrder = this.productionViewtabService.get_OrderCustomFilterOption().subscribe(message => {
      if (message.text === 'getOrderByCustomFilterOption') {
        this.getOrderByCustomFilterOption();
      }
    });

    this.subscriptionForCustomExport = this.productionViewtabService.get_CustomExport().subscribe(message => {
      if (message.text === 'Excel') {
        this.exporttoExcel();
      } else if (message.text === 'PDF') {
        this.exportToPdf();
      }
    });

    if (this.appComponent.inCompleteTab === true) {
      const MediaAssetId = this.appComponent.productionForm.controls['MediaAssetId'].value;
      const IssueDateId = this.appComponent.productionForm.controls['IssueDateId'].value;
      const AdTypeId = this.appComponent.productionForm.controls['AdTypeId'].value;
      const RepId = this.appComponent.productionForm.controls['RepId'].value;
      if (!isNullOrUndefined(AdTypeId) && AdTypeId !== 0
        || !isNullOrUndefined(MediaAssetId) && MediaAssetId !== 0
        || !isNullOrUndefined(IssueDateId) && IssueDateId !== 0
        || !isNullOrUndefined(RepId) && RepId !== 0) {
        this.getOrderByCustomFilterOption();
      }
    }

    this.getProductionDropDownDataInitially();
    //this.getAdvertiserThroughASI();
  }
  
  inCompleteFormValidation() {
    this.inCompleteForm = this.formBuilder.group({
      'NewPickupInd': [null],
      'PickupMediaOrderId': [null],
      'AdvertiserAgencyInd': [null],
      'CreateExpectedInd': [null],
      'MaterialExpectedDate': [null],
      'TrackingNumber': [null],
      'ChangesInd': [null],
      'OnHandInd': [null],
      'OrigFileName': [null],
      'ProofFileName': [null],
      'FinalFileName': [null],
      'WebAdUrl': [null],
      'TearSheets': [null],
      'HeadLine': [null],
      'PageNumber': [null],
      'ProductionComment': [null],
      'MaterialContactId': [null],
      'MediaOrderId': [null],
      'ClassifiedText': [null],
      'SeparationId': [null],
      'PositionId': [null],
      'inCompleteOrderProductionStages': [null],
      'ProductionStatusId': [null],
    });
  }
  getProductionDropDownDataInitially() {
    try {
      this.incompleteOrderService.getProductionDropdownInitially().subscribe(result => {
        if (result.StatusCode === 1) {
          this.hideloader();
          if (result.Data != null) {
            if (result.Data.length > 0) {
              const Data = result.Data;
              this.fillDropDownInitially(Data);
            } else {
              this.productionStatusList = [];
              this.positionsList = [];
              this.separationsList = [];
              this.productionStages1List = [];
            }
          } else {
            this.productionStatusList = [];
            this.positionsList = [];
            this.separationsList = [];
            this.productionStages1List = [];
          }
        } else {
          this.hideloader();
          this.productionStatusList = [];
          this.positionsList = [];
          this.separationsList = [];
          this.productionStages1List = [];
        }

      }, error => {
        this.hideloader();
        console.log(error);
      });
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  fillDropDownInitially(objData) {
    try {
      if (objData.length > 0) {
        const dbproductionStatus = objData[0].ProductionStatus;
        const dbProductionStages1 = objData[0].ProductionStages;
        const dbPositions = objData[0].Positions;
        const dbSeparations = objData[0].Separations;

        if (dbproductionStatus.length > 0) {
          this.productionStatusList = dbproductionStatus;
        } else {
          this.productionStatusList = [];
        }

        if (dbPositions.length > 0) {
          this.positionsList = dbPositions;
        } else {
          this.positionsList = [];
        }

        if (dbSeparations.length > 0) {
          this.separationsList = dbSeparations;
        } else {
          this.separationsList = [];
        }

        if (dbProductionStages1.length > 0) {
          this.productionStages1List = dbProductionStages1;
        } else {
          this.productionStages1List = [];
        }

      } else {
        this.productionStatusList = [];
        this.positionsList = [];
        this.separationsList = [];
        this.productionStages1List = [];
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getAdvertiserThroughASI() {
    try {
      if (this._globalClass.getmainPartyAndOrganizationData().length <= 0
        && this._globalClass.getmainAdvertisers().length <= 0
        && this._globalClass.getmainAgencies().length <= 0
        && this._globalClass.getmainBillingToContacts().length <= 0) {

        this.hasNext = true;
        this.offset = 0;

        this.getAdvertiserData();
      } else {
        this.partyAndOrganizationData = this._globalClass.getmainPartyAndOrganizationData();
        this.advertisers = this._globalClass.getmainAdvertisers();
        this.agencies = this._globalClass.getmainAgencies();
        this.personData = this._globalClass.getmainBillingToContacts();
      }

    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getOrderByCustomFilterOption() {
    this.selectedIssueMediaOrder = [];

    this.selectedCheckbox = [];
    this.selectedMediaOrderIds = [];
    this.selectedAdvertiserIds = [];
    this.selectedAgencyIds = [];

    this.resetValueChkChange();
    try {
      this.dataObject = {};
        this.dataObject.MediaAssetId = this.appComponent.productionForm.controls['MediaAssetId'].value;
        this.dataObject.IssueDateId = this.appComponent.productionForm.controls['IssueDateId'].value;
        this.dataObject.AdTypeId = this.appComponent.productionForm.controls['AdTypeId'].value;
        this.dataObject.RepId = this.appComponent.productionForm.controls['RepId'].value;

        const obj = {
          AdTypeId: this.dataObject.AdTypeId,
          IssueDateId: this.dataObject.IssueDateId,
          MediaAssetId: this.dataObject.MediaAssetId,
          RepId: this.dataObject.RepId,
        };

        this.showloader();
        this.incompleteOrderService.getOrderByCustomFilterOption(obj, this.reqVerificationToken).subscribe(result => {
          this.hideloader();
          this._globalClass.setPageFraction(0);

          if (result.StatusCode === 1) {
            if (result.Data != null) {
              if (result.Data.length > 0) {

                const data = result.Data;
                this.tempArray = [];

                data.forEach((element, key) => {
                  this.inCompleteOrder = element;

                  const t = this._globalClass.getPageFraction() + element.PageFraction;
                  this._globalClass.setPageFraction(t);

                  this.appComponent.pageFraction = t;

                  let TN = '';
                  let MED = '';
                  let MediaAssetIssueDate = '';

                  if (element.TrackingNumber != null && element.TrackingNumber !== '') {
                    TN = ' | ' + (element.TrackingNumber).toString();
                  }

                  if (element.MaterialExpectedDate != null && element.MaterialExpectedDate !== '') {
                    MED = ' | ' + Common.getDateInFormat(element.MaterialExpectedDate);
                  }

                  if (element.PickupIndFromPg != null && element.PickupIndFromPg !== '') {
                    MediaAssetIssueDate = element.PickupIndFromMediaAsset
                      + ' | ' + Common.getDateInFormat(element.PickupIndFromIssueDate)
                      + ' | ' + element.PickupIndFromPg;
                  } else if (element.PickupIndFromIssueDate !== null && element.PickupIndFromIssueDate !== ''
                    && element.PickupIndFromIssueDate !== '0001-01-01T00:00:00') {
                    MediaAssetIssueDate = element.PickupIndFromMediaAsset + ' | ' + Common.getDateInFormat(element.PickupIndFromIssueDate);
                  } else if (element.PickupIndFromMediaAsset != null && element.PickupIndFromMediaAsset !== '') {
                    MediaAssetIssueDate = element.PickupIndFromMediaAsset;
                  }

                  if (element.NewPickupInd) {
                    if (element.CreateExpectedInd) {
                      if (element.OnHandInd) {
                        this.inCompleteOrder.MaterialInfo = 'New material we create on hand ' + TN + MED;
                      } else {
                        this.inCompleteOrder.MaterialInfo = 'New material we create ' + TN + MED;
                      }
                    } else {
                      if (element.OnHandInd) {
                        this.inCompleteOrder.MaterialInfo = 'New material expected on hand ' + TN + MED;
                      } else {
                        this.inCompleteOrder.MaterialInfo = 'New material expected ' + TN + MED;
                      }
                    }
                  } else if (element.ChangesInd) {
                    this.inCompleteOrder.MaterialInfo = 'Pick-up with changes from ' + MediaAssetIssueDate;
                  } else if (MediaAssetIssueDate !== '' && MediaAssetIssueDate != null) {
                    this.inCompleteOrder.MaterialInfo = 'Pick-up from ' + MediaAssetIssueDate;
                  } else {
                    this.inCompleteOrder.MaterialInfo = 'Pick-up';
                  }

                  this.tempArray.push(this.inCompleteOrder);
                  this.inCompleteOrder = {};
                });

                this.inCompleteOrders = this.tempArray;
                this.setAdvertiserNameInCompleteOrders(this.tempArray);

              } else {
                this.inCompleteOrders = [];
              }
            } else {
              this.inCompleteOrders = [];
            }
          } else if (result.StatusCode === 3) {
            this.inCompleteOrders = [];
          } else {
            this.toastr.error(result.Message, '');
          }
        }, error => {
          this.hideloader();
          console.log(error);
        });
      // if (this._globalClass.getmainPartyAndOrganizationData().length > 0
      //   && this._globalClass.getmainAdvertisers().length > 0
      //   && this._globalClass.getmainAgencies().length > 0
      //   && this._globalClass.getmainBillingToContacts().length > 0) {

        

      // } else {
      //   //this.getAdvertiserThroughASI();
      // }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  setAdvertiserNameInCompleteOrders(objOrders: any) {
    try {
      if (objOrders.length > 0) {
        objOrders.forEach((element, index) => {
          const advertiserId = element.AdvertiserId;
          //this.showloader();
          this.incompleteOrderService.getAdvertiserDetail_ByAdvertiserId(this.offset, this.websiteRoot,advertiserId).subscribe(result => {
            if (result != null && result !== undefined && result !== '') {
              const ItemData = result.Items.$values;
              const advertiserData=[];
              if (ItemData.length > 0) {
                ItemData.forEach(itemdatavalue => {
                  const type = itemdatavalue.$type;
                  if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                    advertiserData.push(itemdatavalue);
                  }
                  if(advertiserData.length>0)
                  {
                    const objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id = ?', [advertiserData, advertiserId]);
                    if (objAdvertiser.length > 0) {
                      const aName=objAdvertiser[0].OrganizationName;
                      this.inCompleteOrders[index]['AdvertiserName'] = aName;
                    }
                  }
                });
              }
            }
          }, error => {
            console.log(error);
          });
          // const objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id LIKE ?', [this.partyAndOrganizationData, advertiserId]);
          // if (objAdvertiser.length > 0) {
          //   const aName = objAdvertiser[0].Name;
          //   this.inCompleteOrders[index]['AdvertiserName'] = aName;
          // } else {
          //   const aName = '';
          //   this.inCompleteOrders[index]['AdvertiserName'] = aName;
          // }
        });
        // if (this._globalClass.getmainPartyAndOrganizationData().length > 0) {
        //   this.partyAndOrganizationData = this._globalClass.getmainPartyAndOrganizationData();
          

        //   if (this.inCompleteOrders.length > 0) {
        //     this.inCompleteOrders = alasql('SELECT * FROM ? AS add ORDER BY AdvertiserName ASC', [this.inCompleteOrders]);
        //   }

        // } else {
        //   this.inCompleteOrders = [];
        //   this.getAdvertiserThroughASI();
        // }
      } else {
        this.inCompleteOrders = [];
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getAdvertiserData() {
    try {
      this.showloader();
      this.incompleteOrderService.getAllAdvertiser1(this.offset, this.websiteRoot).subscribe(result => {
        if (result !== null && result !== undefined && result !== '') {
          const ItemData = result.Items.$values;
          if (ItemData.length > 0) {

            ItemData.forEach(element => {
              this.partyAndOrganizationData.push(element);
              const type = element.$type;
              if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                this.advertisers.push(element);
                this.agencies.push(element);
              } else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                this.billingToContactsList.push(element);
              }
            });

            this.offset = result.Offset;
            const NextOffset = this.offset + 500;

            if (result.Count === 500) {
              this.offset = NextOffset;
              this.getAdvertiserData();
            } else {
              this.hideloader();
              this._globalClass.setmainPartyAndOrganizationData(this.partyAndOrganizationData);
              this._globalClass.setmainAdvertisers(this.advertisers);
              this._globalClass.setmainAgencies(this.agencies);
              this._globalClass.setmainBillingToContacts(this.billingToContactsList);
            }
          } else {
            this.hideloader();
            this._globalClass.setmainPartyAndOrganizationData(this.partyAndOrganizationData);
            this._globalClass.setmainAdvertisers(this.advertisers);
            this._globalClass.setmainAgencies(this.agencies);
            this._globalClass.setmainBillingToContacts(this.billingToContactsList);
          }
        } else {
          this.hideloader();
          this.advertisers = [];
          this.agencies = [];
          this.billingToContactsList = [];
          this.partyAndOrganizationData = [];
        }
      }, error => {
        this.hideloader();
        console.log(error);
      });
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  checkboxChange(objInCompleteOrder: any) {
    try {
      const qId = objInCompleteOrder.IssueDateId;
      const mId = objInCompleteOrder.MediaAssetId;
      const mOrderId = objInCompleteOrder.MediaOrderId;
      const advertiserId = objInCompleteOrder.AdvertiserId;
      const agencyId = objInCompleteOrder.BT_ID;
      const buyId = objInCompleteOrder.BuyId;
      const advertiserName=objInCompleteOrder.AdvertiserName;
      this.buyId = buyId;
      const idx = this.selectedCheckbox.indexOf(mOrderId);
      if (idx > -1) {

        this.selectedCheckbox.splice(idx, 1);
        this.selectedMediaOrderIds.forEach((element, index) => {
          // tslint:disable-next-line:radix
          if (parseInt(element) === parseInt(mOrderId)) {
            this.selectedMediaOrderIds.splice(index, 1);
            this.selectedAdvertiserIds.splice(index, 1);
            this.selectedAgencyIds.splice(index, 1);
          }
        });

        if (this.selectedCheckbox.length < 1) {
          this.resetValueChkChange();
        }

        if (this.selectedCheckbox.length === 1) {
          this.selectedMediaOrderId = this.selectedMediaOrderIds[0];
          this.selectedAdvertiserId = this.selectedAdvertiserIds[0];
          this.selectedAgencyId = this.selectedAgencyIds[0];
          this.getOrdersNotInBuyer(buyId, this.selectedAdvertiserId);
          const changeBillToValue = this.inCompleteForm.controls['AdvertiserAgencyInd'].value;
          this.getBillToContact(changeBillToValue);
          this.setProductionNoClick = false;
        }

        if (this.selectedCheckbox.length > 1) {
          this.resetValueChkChange();
          this.selectedAdvertiserIds = [];
        }

      } else {
        this.selectedCheckbox.push(mOrderId);
        this.selectedMediaOrderIds.push(mOrderId);
        this.selectedAdvertiserIds.push(advertiserId);
        this.selectedAgencyIds.push(agencyId);
        if (this.selectedCheckbox.length === 1) {
          this.selectedMediaOrderId = this.selectedMediaOrderIds[0];
          this.selectedAdvertiserId = this.selectedAdvertiserIds[0];
          this.selectedAgencyId = this.selectedAgencyIds[0];
          console.log(this.selectedAdvertiserId +"-"+ this.selectedAgencyId);
          this.getOrdersNotInBuyer(buyId, this.selectedAdvertiserId);
          const changeBillToValue = this.inCompleteForm.controls['AdvertiserAgencyInd'].value;
          this.getBillToContact(changeBillToValue);
          this.setProductionNoClick = false;
        }
      }

      if (this.selectedCheckbox.length > 1) {
        this.resetValueChkChange();
      }

    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  resetValueChkChange() {
    try {
      this.selectedMediaOrderId = '';
      this.selectedAdvertiserId = '';

      this.selectedAgencyId = '';
      this.setProductionNoClick = true;
    } catch (error) {
      this.hideloader();
      console.log(error);
    }

    try {
      this.inCompleteOrder = {};

      this.buyId = 0;

      this.inCompleteForm.reset();

      this.inCompleteForm.controls['NewPickupInd'].setValue('1');
      this.inCompleteForm.controls['CreateExpectedInd'].setValue('0');
      this.inCompleteForm.controls['AdvertiserAgencyInd'].setValue('1');

      this.inCompleteForm.controls['OnHandInd'].setValue(false);
      this.inCompleteForm.controls['ChangesInd'].setValue(false);

      this.inCompleteOrder_ProductionStagesList = [];

      this.originalFile = null;
      this.originalFileImageSrc = null;
      this.originalFileExtension = null;

      this.finalFile = null;
      this.finalFileExtension = null;
      this.finalFileImageSrc = null;

      this.proofFile = null;
      this.proofFileExtension = null;
      this.proofFileImageSrc = null;

      this.tempPdfPathForOriginal = null;
      this.tempPdfPathForProof = null;
      this.tempPdfPathForFinal = null;

      this.buttonText = 'Add';
      this.setProductionNoClick = true;
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  // Get list of orders which is not in buyer
  getOrdersNotInBuyer(buyId, adid) {
    try {
      this.incompleteOrderService.getordersnotinbuyer(buyId, adid).subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data != null) {
            if (result.Data.length > 0) {
              this.tempArray = [];
              result.Data.forEach((element, key) => {
                const issueDate = Common.getDateInFormat(element.IssueDate);
                const pageNo = element.PageNumber == null ? '' : element.PageNumber;
                if (pageNo !== null && pageNo !== undefined && pageNo !== '') {
                  this.mediaAssetModel.MediaAssetIssueDate = element.MediaAssetName + ' | ' + issueDate + ' | ' + pageNo;
                } else {
                  this.mediaAssetModel.MediaAssetIssueDate = element.MediaAssetName + ' | ' + issueDate;
                }
                this.mediaAssetModel.MediaOrderId = element.MediaOrderId;
                this.tempArray.push(this.mediaAssetModel);
                this.mediaAssetModel = {};
              });

              this.mediaAssetsList = this.tempArray;

            } else {
              this.mediaAssetsList = [];
            }
          } else {
            this.mediaAssetsList = [];
          }

        } else if (result.StatusCode === 3) {
          this.mediaAssetsList = [];
        } else {
          this.mediaAssetsList = [];
          this.toastr.error(result.Message, 'Error!');
        }
        this.getMediaOrderProduction(this.selectedMediaOrderId);

      }, error => {
        this. hideloader();
        console.log(error);
      });
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getBillToContact(changeBillToValue) {
    try {
      if (changeBillToValue === '0') {
        changeBillToValue = this.inCompleteForm.controls['AdvertiserAgencyInd'].value;
      }
      const advertiserData=[];
      let id = '0';
      const temp = changeBillToValue;
      if (temp === '1') {
        changeBillToValue = 1;
      } else if (temp === '0') {
        changeBillToValue = 0;
      }

      // tslint:disable-next-line:radix
      if (parseInt(changeBillToValue) === 0 || parseInt(changeBillToValue) === 1) {
        this.billingToContactsList = [];
        // tslint:disable-next-line:radix
        if (parseInt(changeBillToValue) === 1) {
          id = this.selectedAdvertiserId;
          // tslint:disable-next-line:radix
        } else if (parseInt(changeBillToValue) === 0) {
          id = this.selectedAgencyId;
        }
        console.log("selected id:"+ id);
        this.incompleteOrderService.getAdvertiserDetail_ByAdvertiserId(this.offset, this.websiteRoot,id).subscribe(result => {
          if (result != null && result !== undefined && result !== '') {
            const ItemData = result.Items.$values;
            if (ItemData.length > 0) {
              ItemData.forEach(itemdatavalue => {
                const type = itemdatavalue.$type;
                if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                  advertiserData.push(itemdatavalue);
                }
                if(advertiserData.length>0)
                {
                  const objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id = ?', [advertiserData, id]);
                  if (objAdvertiser.length > 0) {
                    this.selectedAdvertiserAgencyName=objAdvertiser[0].OrganizationName;
                    this.organizationWiseParty = [];
                    console.log("Selected Name:"+ this.selectedAdvertiserAgencyName);
                    if(this.selectedAdvertiserAgencyName!=='')
                    {
                      this.incompleteOrderService.getAdvertiserDetail_ByAdvertiserName(0,this.websiteRoot,this.selectedAdvertiserAgencyName).subscribe(result => {
                        if (result != null && result !== undefined && result !== '') {
                          const ItemData = result.Items.$values;
                          for (const itemdatavalue of ItemData) {
                            const type = itemdatavalue.$type;
                            if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                              this.organizationWiseParty.push(itemdatavalue);
                            }
                          }
                          if(this.organizationWiseParty.length>0)
                          {
                            this.billingToContactsList = this.organizationWiseParty;
                          }
                        } else {
                          this.billingToContactsList = [];
                        }
                      }, error => {
                        console.log(error);
                      });
                    }
                  }
                }
              });
            }
          }
        }, error => {
          console.log(error);
        });
      }
      //const partyData = this.partyAndOrganizationData;
      // tslint:disable-next-line:radix
      // if (id !== undefined && parseInt(id) > 0) {
      //   partyData.forEach(element => {
      //     if (element.PrimaryOrganization !== undefined) {
      //       if (element.PrimaryOrganization.OrganizationPartyId !== undefined) {
      //         const organizationID = element.PrimaryOrganization.OrganizationPartyId;
      //         if (id === organizationID) {
      //           this.organizationWiseParty.push(element);
      //         }
      //       }
      //     }
      //   });
      // } else {
      //   this.organizationWiseParty = [];
      // }
      //this.billingToContactsList = this.organizationWiseParty;
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getMediaOrderProduction(mOrderId) {
    try {
      this.incompleteOrderService.getByMediaOrderApi(mOrderId).subscribe(result => {
        if (result.Data != null) {
          this.buttonText = 'Update';
          this.inCompleteOrder = result.Data;

          this.buyId = this.inCompleteOrder.BuyId;
          this.mediaOrderProductionDetailId = this.inCompleteOrder.MediaOrderProductionDetailId;

          this.inCompleteForm.controls['inCompleteOrderProductionStages'].setValue(null);

          /* --------------------------------------------------------*/

          if (this.inCompleteOrder.TrackingNumber === undefined
            || this.inCompleteOrder.TrackingNumber === ''
            || this.inCompleteOrder.TrackingNumber === null) {
            this.inCompleteForm.controls['TrackingNumber'].setValue('0');
          } else {
            if (!isNaN(this.inCompleteOrder.TrackingNumber)) {
              // tslint:disable-next-line:radix
              this.inCompleteForm.controls['TrackingNumber'].setValue(parseInt(this.inCompleteOrder.TrackingNumber));
            } else {
              this.inCompleteForm.controls['TrackingNumber'].setValue('');
            }
          }

          if (this.inCompleteOrder.WebAdUrl === undefined
            || this.inCompleteOrder.WebAdUrl === ''
            || this.inCompleteOrder.WebAdUrl === null) {
            this.inCompleteForm.controls['WebAdUrl'].setValue('');
          } else {
            this.inCompleteForm.controls['WebAdUrl'].setValue(this.inCompleteOrder.WebAdUrl);
          }

          if (this.inCompleteOrder.HeadLine === undefined
            || this.inCompleteOrder.HeadLine === ''
            || this.inCompleteOrder.HeadLine === null) {
            this.inCompleteForm.controls['HeadLine'].setValue('');
          } else {
            this.inCompleteForm.controls['HeadLine'].setValue(this.inCompleteOrder.HeadLine);
          }

          if (this.inCompleteOrder.TearSheets === undefined
            || this.inCompleteOrder.TearSheets === ''
            || this.inCompleteOrder.TearSheets === null) {
            this.inCompleteForm.controls['TearSheets'].setValue('');
          } else {
            this.inCompleteForm.controls['TearSheets'].setValue(this.inCompleteOrder.TearSheets);
          }

          if (this.inCompleteOrder.PageNumber === undefined
            || this.inCompleteOrder.PageNumber === ''
            || this.inCompleteOrder.PageNumber === null) {
            this.inCompleteForm.controls['PageNumber'].setValue('');
          } else {
            this.inCompleteForm.controls['PageNumber'].setValue(this.inCompleteOrder.PageNumber);
          }

          if (this.inCompleteOrder.ClassifiedText === undefined
            || this.inCompleteOrder.ClassifiedText === ''
            || this.inCompleteOrder.ClassifiedText === null) {
            this.inCompleteForm.controls['ClassifiedText'].setValue('');
          } else {
            this.inCompleteForm.controls['ClassifiedText'].setValue(this.inCompleteOrder.ClassifiedText);
          }

          if (this.inCompleteOrder.ProductionComment === undefined
            || this.inCompleteOrder.ProductionComment === ''
            || this.inCompleteOrder.ProductionComment === null) {
            this.inCompleteForm.controls['ProductionComment'].setValue('');
          } else {
            this.inCompleteForm.controls['ProductionComment'].setValue(this.inCompleteOrder.ProductionComment);
          }

          /* --------------------------------------------------------*/

          if (this.inCompleteOrder.NewPickupInd === false
            || this.inCompleteOrder.NewPickupInd === null
            || this.inCompleteOrder.NewPickupInd === undefined) {
            this.dvNew = true;
            this.dvPickup = false;
            this.inCompleteForm.controls['NewPickupInd'].setValue('0');
            this.inCompleteForm.controls['PickupMediaOrderId'].enable();
          } else if (this.inCompleteOrder.NewPickupInd === true) {
            this.dvNew = false;
            this.dvPickup = true;
            this.inCompleteForm.controls['NewPickupInd'].setValue('1');
            this.inCompleteForm.controls['PickupMediaOrderId'].disable();
          }

          if (this.inCompleteOrder.AdvertiserAgencyInd === false
            || this.inCompleteOrder.AdvertiserAgencyInd === null
            || this.inCompleteOrder.AdvertiserAgencyInd === undefined) {
            this.inCompleteForm.controls['AdvertiserAgencyInd'].setValue('0');
          } else if (this.inCompleteOrder.AdvertiserAgencyInd === true) {
            this.inCompleteForm.controls['AdvertiserAgencyInd'].setValue('1');
          }

          if (this.inCompleteOrder.CreateExpectedInd === false
            || this.inCompleteOrder.CreateExpectedInd === null
            || this.inCompleteOrder.CreateExpectedInd === undefined) {

            this.inCompleteForm.controls['CreateExpectedInd'].setValue('0');
          } else if (this.inCompleteOrder.CreateExpectedInd === true) {
            this.inCompleteForm.controls['CreateExpectedInd'].setValue('1');
          }

          if (this.inCompleteOrder.OnHandInd === false
            || this.inCompleteOrder.OnHandInd === null
            || this.inCompleteOrder.OnHandInd === undefined) {
            this.inCompleteForm.controls['OnHandInd'].setValue(false);
          } else if (this.inCompleteOrder.OnHandInd === true) {
            this.inCompleteForm.controls['OnHandInd'].setValue(true);
          }

          if (this.inCompleteOrder.ChangesInd === false
            || this.inCompleteOrder.ChangesInd === null
            || this.inCompleteOrder.ChangesInd === undefined) {
            this.inCompleteForm.controls['ChangesInd'].setValue(false);
          } else if (this.inCompleteOrder.ChangesInd === true) {
            this.inCompleteForm.controls['ChangesInd'].setValue(true);
          }

          /* -------------------- FILE -------------------- */


          if (this.inCompleteOrder.OriginalFile != null) {
            const tempOriginal = this.inCompleteOrder.OriginalFile;
            const fileNameOriginal = tempOriginal.substring(tempOriginal.lastIndexOf('/')).split('/').pop();

            this.originalFile = this.inCompleteOrder.OriginalFile;
            this.extensionOriginal = fileNameOriginal.substring(fileNameOriginal.lastIndexOf('.'));
            this.inCompleteForm.controls['OrigFileName'].setValue(fileNameOriginal);
          } else {
            this.originalFile = null;
            this.extensionOriginal = null;
            this.inCompleteForm.controls['OrigFileName'].setValue(null);
          }

          if (this.inCompleteOrder.ProofFile != null) {
            const tempProof = this.inCompleteOrder.ProofFile;
            const fileNameProof = tempProof.substring(tempProof.lastIndexOf('/')).split('/').pop();

            this.proofFile = this.inCompleteOrder.ProofFile;
            this.extensionProof = fileNameProof.substring(fileNameProof.lastIndexOf('.'));
            this.inCompleteForm.controls['ProofFileName'].setValue(fileNameProof);
          } else {
            this.proofFile = null;
            this.extensionProof = null;
            this.inCompleteForm.controls['ProofFileName'].setValue(null);
          }

          if (this.inCompleteOrder.FinalFile != null) {
            const tempFinal = this.inCompleteOrder.FinalFile;
            const fileNameFinal = tempFinal.substring(tempFinal.lastIndexOf('/')).split('/').pop();

            this.finalFile = this.inCompleteOrder.FinalFile;
            this.extensionFinal = fileNameFinal.substring(fileNameFinal.lastIndexOf('.'));
            this.inCompleteForm.controls['FinalFileName'].setValue(fileNameFinal);
          } else {
            this.finalFile = null;
            this.extensionFinal = null;
            this.inCompleteForm.controls['FinalFileName'].setValue(null);
          }

          if (this.extensionOriginal === null) {
            this.isPdfTrueForOriginal = false;
            this.isImgTrueForOriginal = false;
          } else {
            if (this.extensionOriginal === '.pdf') {
              this.isPdfTrueForOriginal = true;
              this.isImgTrueForOriginal = false;
              this.tempPdfPathForOriginal = this.inCompleteOrder.OriginalFile;
            } else if (this.extensionOriginal === '.png'
              || this.extensionOriginal === '.jpg'
              || this.extensionOriginal === '.jpeg') {
              this.isPdfTrueForOriginal = false;
              this.isImgTrueForOriginal = true;
            } else {
              this.isPdfTrueForOriginal = true;
              this.isImgTrueForOriginal = false;
              this.tempPdfPathForOriginal = this.inCompleteOrder.OriginalFile;
            }
          }

          if (this.extensionProof === null) {
            this.isPdfTrueForProof = false;
            this.isImgTrueForProof = false;
          } else {
            if (this.extensionProof === '.pdf') {
              this.isPdfTrueForProof = true;
              this.isImgTrueForProof = false;
              this.tempPdfPathForProof = this.inCompleteOrder.ProofFile;
            } else if (this.extensionProof === '.png'
              || this.extensionProof === '.jpg'
              || this.extensionProof === '.jpeg') {
              this.isPdfTrueForProof = false;
              this.isImgTrueForProof = true;
            } else {
              this.isPdfTrueForProof = true;
              this.isImgTrueForProof = false;
              this.tempPdfPathForProof = this.inCompleteOrder.ProofFile;
            }
          }

          if (this.extensionFinal === null) {
            this.isPdfTrueForfinal = false;
            this.isImgTrueForfinal = false;
          } else {
            if (this.extensionFinal === '.pdf') {
              this.isPdfTrueForfinal = true;
              this.isImgTrueForfinal = false;
              this.tempPdfPathForFinal = this.inCompleteOrder.FinalFile;
            } else if (this.extensionFinal === '.png'
              || this.extensionFinal === '.jpg'
              || this.extensionFinal === '.jpeg') {
              this.isPdfTrueForfinal = false;
              this.isImgTrueForfinal = true;
            } else {
              this.isPdfTrueForfinal = true;
              this.isImgTrueForfinal = false;
              this.tempPdfPathForFinal = this.inCompleteOrder.FinalFile;
            }
          }

          /* -------------------- FILE -------------------- */


          if (result.Data.MaterialExpectedDate !== undefined && result.Data.MaterialExpectedDate !== ''
            && result.Data.MaterialExpectedDate != null) {
            this.inCompleteForm.controls['MaterialExpectedDate'].setValue(Common.getDateInFormat(result.Data.MaterialExpectedDate));
          } else {
            this.inCompleteForm.controls['MaterialExpectedDate'].setValue('');
          }

          this.originalFileImageSrc = this.inCompleteOrder.OriginalFile;
          this.proofFileImageSrc = this.inCompleteOrder.ProofFile;
          this.finalFileImageSrc = this.inCompleteOrder.FinalFile;

          /* ------------------------------------------------------ */
          if (!isNullOrUndefined(this.inCompleteOrder.PickupMediaOrderId)) {
            this.inCompleteForm.controls['PickupMediaOrderId'].setValue(this.inCompleteOrder.PickupMediaOrderId);
          } else {
            this.inCompleteForm.controls['PickupMediaOrderId'].setValue(null);
          }
          /* ------------------------------------------------------ */

          if (!isNullOrUndefined(this.inCompleteOrder.PositionId)) {
            this.inCompleteForm.controls['PositionId'].setValue(this.inCompleteOrder.PositionId);
          } else {
            this.inCompleteForm.controls['PositionId'].setValue(null);
          }
          /* ------------------------------------------------------ */

          if (!isNullOrUndefined(this.inCompleteOrder.SeparationId)) {
            this.inCompleteForm.controls['SeparationId'].setValue(this.inCompleteOrder.SeparationId);
          } else {
            this.inCompleteForm.controls['SeparationId'].setValue(null);
          }
          /* ------------------------------------------------------ */

          if (!isNullOrUndefined(this.inCompleteOrder.MaterialContactId)) {
            if (this.inCompleteOrder.AdvertiserAgencyInd === false
              || this.inCompleteOrder.AdvertiserAgencyInd === null
              || this.inCompleteOrder.AdvertiserAgencyInd === undefined) {
              this.getBillToContact('0');
            } else if (this.inCompleteOrder.AdvertiserAgencyInd === true) {
              this.getBillToContact('1');
            }

            this.inCompleteForm.controls['MaterialContactId'].setValue(this.inCompleteOrder.MaterialContactId.toString());
          } else {
            this.inCompleteForm.controls['MaterialContactId'].setValue(null);
          }
          /* ------------------------------------------------------ */

          if (!isNullOrUndefined(this.inCompleteOrder.ProductionStatusId)) {
            this.inCompleteForm.controls['ProductionStatusId'].setValue(this.inCompleteOrder.ProductionStatusId);
          } else {
            this.inCompleteForm.controls['ProductionStatusId'].setValue(null);
          }
          /* ------------------------------------------------------ */

          this.inCompleteOrder_ProductionStagesList = [];
          this.inCompleteOrder_ProductionStagesList = this.inCompleteOrder.ProductionStages;

        } else {
          this.buttonText = 'Add';
        }
      }, error => {
        this. hideloader();
        console.log(error + 'status : ' + error.status);
      });
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getAllProductionStatus() {
    try {
      if (this._globalClass.getrProductionStatus().length <= 0) {

        this.incompleteOrderService.getAllProductionStatus().subscribe(result => {
          if (result.StatusCode === 1) {
            if (result.Data != null) {
              if (result.Data.length > 0) {
                this.productionStatusList = result.Data;
                this._globalClass.setrProductionStatus(this.productionStatusList);
              } else {
                this.productionStatusList = [];
              }
            } else {
              this.productionStatusList = [];
            }
          } else if (result.StatusCode === 3) {
            this.productionStatusList = [];
          } else {
            this.productionStatusList = [];
            this.toastr.error(result.Message, 'Error!');
          }
        }, error => {
          this. hideloader();
          console.log(error);
        });

      } else {
        this.productionStatusList = this._globalClass.getrProductionStatus();
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getAllPosition() {
    try {
      if (this._globalClass.getrPositions().length <= 0) {
        this.incompleteOrderService.getAllPosition().subscribe(result => {

          if (result.StatusCode === 1) {
            if (result.Data != null) {
              if (result.Data.length > 0) {
                this.positionsList = result.Data;
                this._globalClass.setrPositions(this.positionsList);
              } else {
                this.positionsList = [];
              }
            } else {
              this.positionsList = [];
            }
          } else if (result.StatusCode === 3) {
            this.positionsList = [];
          } else {
            this.positionsList = [];
            this.toastr.error(result.Message, 'Error!');
          }
        }, error => {
          this. hideloader();
          console.log(error);
        });
      } else {
        this.positionsList = this._globalClass.getrPositions();
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getAllSeparation() {
    try {
      if (this._globalClass.getrSeparations().length <= 0) {
        this.incompleteOrderService.getAllSeparation().subscribe(result => {
          if (result.StatusCode === 1) {
            if (result.Data != null) {
              if (result.Data.length > 0) {
                this.separationsList = result.Data;
                this._globalClass.setrSeparations(this.separationsList);
              } else {
                this.separationsList = [];
              }
            } else {
              this.separationsList = [];
            }
          } else if (result.StatusCode === 3) {
            this.separationsList = [];
          } else {
            this.separationsList = [];
            this.toastr.error(result.Message, 'Error!');
          }
        }, error => {
          this. hideloader();
          console.log(error);
        });
      } else {
        this.separationsList = this._globalClass.getrSeparations();
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  getAllProductionStages() {
    try {
      if (this._globalClass.getrProductionStages1().length <= 0) {
        this.incompleteOrderService.getAllProductionStage().subscribe(result => {
          if (result.StatusCode === 1) {
            this.productionStages1List = result.Data;
            this._globalClass.setrProductionStages1(this.productionStages1List);
          } else {
            this.toastr.error(result.Message, 'Error');
          }
        }, error => {
          this. hideloader();
          console.log(error);
        });
      } else {
        this.productionStages1List = this._globalClass.getrProductionStages1();
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  update() {
    try {
      let pickupMediaOrderId = this.inCompleteForm.controls['PickupMediaOrderId'].value;
      let newPickupInd = this.inCompleteForm.controls['NewPickupInd'].value;
      let advertiserAgencyInd = this.inCompleteForm.controls['AdvertiserAgencyInd'].value;
      let createExpectedInd = this.inCompleteForm.controls['CreateExpectedInd'].value;
      let materialExpectedDate = this.inCompleteForm.controls['MaterialExpectedDate'].value;
      let trackingNumber = this.inCompleteForm.controls['TrackingNumber'].value;
      let changesInd = this.inCompleteForm.controls['ChangesInd'].value;
      let onHandInd = this.inCompleteForm.controls['OnHandInd'].value;

      let origFileName = this.inCompleteForm.controls['OrigFileName'].value;
      let proofFileName = this.inCompleteForm.controls['ProofFileName'].value;
      let finalFileName = this.inCompleteForm.controls['FinalFileName'].value;

      let webAdUrl = this.inCompleteForm.controls['WebAdUrl'].value;
      let tearSheets = this.inCompleteForm.controls['TearSheets'].value;
      let headLine = this.inCompleteForm.controls['HeadLine'].value;
      let pageNumber = this.inCompleteForm.controls['PageNumber'].value;
      let productionComment = this.inCompleteForm.controls['ProductionComment'].value;
      let materialContactId = this.inCompleteForm.controls['MaterialContactId'].value;
      const classifiedText = this.inCompleteForm.controls['ClassifiedText'].value;
      const positionId = this.inCompleteForm.controls['PositionId'].value;
      const productionStatusId = this.inCompleteForm.controls['ProductionStatusId'].value;
      const separationId = this.inCompleteForm.controls['SeparationId'].value;

      let originalFile = this.originalFile;
      let originalFileExtension = this.originalFileExtension;

      let proofFile = this.proofFile;
      let proofFileExtension = this.proofFileExtension;

      let finalFile = this.finalFile;
      let finalFileExtension = this.finalFileExtension;

      if (pickupMediaOrderId != null && pickupMediaOrderId !== '' && pickupMediaOrderId !== undefined) {
        // tslint:disable-next-line:radix
        pickupMediaOrderId = parseInt(pickupMediaOrderId);
      } else {
        pickupMediaOrderId = null;
      }

      if (newPickupInd === '0') {
        newPickupInd = false;
      } else if (newPickupInd === '1') {
        newPickupInd = true;
      }

      if (advertiserAgencyInd === '0') {
        advertiserAgencyInd = false;
      } else if (advertiserAgencyInd === '1') {
        advertiserAgencyInd = true;
      }

      if (createExpectedInd === '0') {
        createExpectedInd = false;
      } else if (createExpectedInd === '1') {
        createExpectedInd = true;
      }

      if (materialExpectedDate === undefined || materialExpectedDate === null || materialExpectedDate === '') {
        materialExpectedDate = '';
      } else {
        materialExpectedDate = Common.getDate(materialExpectedDate);
      }

      if (trackingNumber === undefined || trackingNumber === null) {
        trackingNumber = '';
      } else {
        trackingNumber = (trackingNumber).toString();
      }

      if (changesInd === undefined || changesInd === '' || changesInd === null) {
        changesInd = null;
      }

      if (onHandInd === undefined || onHandInd === '' || onHandInd === null) {
        onHandInd = null;
      }

      if (originalFile === undefined || originalFile === null || originalFile === '') {
        origFileName = null;
        originalFile = null;
        originalFileExtension = null;
      } else {
        originalFileExtension = origFileName.substring(origFileName.lastIndexOf('.'));
      }

      if (proofFile === undefined || proofFile === null || proofFile === '') {
        proofFileName = null;
        proofFile = null;
        proofFileExtension = null;
      } else {
        proofFileExtension = proofFileName.substring(proofFileName.lastIndexOf('.'));
      }

      if (finalFile === undefined || finalFile === null || finalFile === '') {
        finalFileName = null;
        finalFile = null;
        finalFileExtension = null;
      } else {
        finalFileExtension = finalFileName.substring(finalFileName.lastIndexOf('.'));
      }

      if (webAdUrl === undefined || webAdUrl === '' || webAdUrl === null) {
        webAdUrl = '';
      }

      if (tearSheets === undefined || tearSheets === '' || tearSheets === null) {
        tearSheets = '';
      } else {
        // tslint:disable-next-line:radix
        tearSheets = parseInt(tearSheets);
      }

      if (headLine === undefined || headLine === '' || headLine === null) {
        headLine = '';
      }

      if (pageNumber === undefined || pageNumber === '' || pageNumber === null) {
        pageNumber = '';
      } else {
        pageNumber = (pageNumber).toString();
      }

      if (productionComment === undefined || productionComment === '' || productionComment === null) {
        productionComment = '';
      }

      let l_ProductionStatusId = null;

      if (productionStatusId != null && productionStatusId !== undefined) {
        l_ProductionStatusId = productionStatusId;
      }

      // tslint:disable-next-line:radix
      materialContactId = parseInt(materialContactId);

      const production = {
        PickupMediaOrderId: pickupMediaOrderId,
        NewPickupInd: newPickupInd,
        AdvertiserAgencyInd: advertiserAgencyInd,
        CreateExpectedInd: createExpectedInd,
        MaterialExpectedDate: materialExpectedDate,
        TrackingNumber: trackingNumber,
        ChangesInd: changesInd,
        OnHandInd: onHandInd,

        WebAdUrl: webAdUrl,
        TearSheets: tearSheets,
        HeadLine: headLine,
        PageNumber: pageNumber,
        ProductionComment: productionComment,
        MaterialContactId: materialContactId,
        ClassifiedText: classifiedText,
        PositionId: positionId,
        ProductionStages: this.inCompleteOrder_ProductionStagesList,
        ProductionStatusId: l_ProductionStatusId,
        SeparationId: separationId,

        Completed: false,
        MediaOrderId: this.selectedMediaOrderId,
        MediaOrderProductionDetailId: this.mediaOrderProductionDetailId,

        OriginalFile: originalFile,
        OriginalFileExtension: originalFileExtension,
        OrigFileName: origFileName,

        ProofFile: proofFile,
        ProofFileExtension: proofFileExtension,
        ProofFileName: proofFileName,

        FinalFile: finalFile,
        FinalFileExtension: finalFileExtension,
        FinalFileName: finalFileName
      };
console.log(production);
      this.showloader();
      this.incompleteOrderService.updateProduction(production).subscribe(result => {
        if (result.StatusCode === 1) {
          this.hideloader();
          this.toastr.success('Order save successfully.', 'Success!');

          this.getOrderByCustomFilterOption();
        } else {
          this.hideloader();
          this.toastr.error(result.Message, 'Error!');
        }
      }, error => {
        this.hideloader();
        console.log(error);
      });

    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  onChangeFrom() {
  }

  onChangeContact() {
  }

  onChangeProductionStatus() {
  }

  onChangeSeparation() {
  }

  onChangePosition() {
  }

  onChangeProductionStages() {
  }

  // --------- Add, Delete Production Stages ---------

  addProductionStages() {
    try {
      if (this.productionStages1List != null) {
        const inComOrProdStge = this.inCompleteForm.controls['inCompleteOrderProductionStages'].value;

        // tslint:disable-next-line:max-line-length
        const data = alasql('SELECT * FROM ? AS add WHERE ProductionStageId = ?', [this.productionStages1List, parseInt(inComOrProdStge)]);
        console.log('data',data);
        const final = {
          ProductionStagesName: data[0].Name,
          UpdatedDate: new Date(),
          // tslint:disable-next-line:radix
          ProductionStageId: parseInt(inComOrProdStge),
          UpdateBy: this.updatedBy,
          MediaOrderProductionStagesId: '',
        };
        this.inCompleteOrder_ProductionStagesList.push(final);
        this.toastr.success('Production stage added successfully.', 'Success!');
        this.inCompleteForm.controls['inCompleteOrderProductionStages'].setValue(null);
      }
      if (this.inCompleteOrder_ProductionStagesList.length > 0) {
      } else {
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  deleteProductionStage(productionStage) {
    try {
      this.inCompleteOrder_ProductionStagesList.forEach((element, index) => {
        if (element.UpdatedDate === productionStage.UpdatedDate && element.ProductionStagesName === productionStage.ProductionStagesName) {
          this.inCompleteOrder_ProductionStagesList.splice(index, 1);
        }
      });
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }
  // --------- End Of Add, Delete Production Stages ---------

  // Enable disable code for pickup and new
  setEnableOrDisable(rdoName) {
    try {
      // tslint:disable-next-line:radix
      if (parseInt(rdoName) === 1) {
        this.dvNew = false;
        this.dvPickup = true;
        this.inCompleteForm.controls['PickupMediaOrderId'].disable();
        this.inCompleteForm.controls['ChangesInd'].setValue(false);
        this.inCompleteForm.controls['PickupMediaOrderId'].setValue(null);
        this.inCompleteForm.controls['CreateExpectedInd'].setValue('0');
        // tslint:disable-next-line:radix
      } else if (parseInt(rdoName) === 0) {
        this.dvNew = true;
        this.dvPickup = false;
        this.inCompleteForm.controls['PickupMediaOrderId'].enable();
        this.inCompleteForm.controls['CreateExpectedInd'].setValue('0');
        this.inCompleteForm.controls['OnHandInd'].setValue(false);
        this.inCompleteForm.controls['MaterialExpectedDate'].setValue('');
        this.inCompleteForm.controls['TrackingNumber'].setValue('');
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }


  setToCompleteInserationOrder() {
    try {
      if (this.selectedCheckbox.length === 1) {
        const mId = this.selectedMediaOrderIds[0];
        this.showloader();

        this.incompleteOrderService.setOrderProductionStatusToComplete(mId).subscribe(result => {
          if (result.StatusCode === 1) {
            this.hideloader();
            if (result.Data != null) {
              const Data = result.Data;
              this.resetValueChkChange();

              this.selectedIssueMediaOrder = [];
              this.selectedCheckbox = [];
              this.selectedMediaOrderIds = [];
              this.selectedAdvertiserIds = [];
              this.selectedAgencyIds = [];

              this.toastr.success('Mark as completed successfully.', 'Success!');
              this.getOrderByCustomFilterOption();
            }
          } else {
            this.hideloader();
            this.toastr.error(result.Message, 'Error!');
          }
        }, error => {
          this.hideloader();
          console.log(error);
        });
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  // ------------ Upload & Image Viewer-----------
  uploadImage(event: any) {
    try {
      if (event.target.files && event.target.files[0]) {
        const logoElement = event.target.files.item(0);
        const uploadType = logoElement.type;
        const filename = event.target.files.item(0).name;
        const ext = filename.substr(filename.lastIndexOf('.') + 1);
        if (uploadType !== undefined && uploadType != null) {
          if (logoElement.type.indexOf('application/pdf') !== -1) {
            const tmppath = URL.createObjectURL(event.target.files[0]);
            this.tempPdfPathForOriginal = tmppath;
            this.isPdfTrueForOriginal = true;
            this.isImgTrueForOriginal = false;
          } else if (logoElement.type.indexOf('image') !== -1) {
            this.isImgTrueForOriginal = true;
            this.isPdfTrueForOriginal = false;
          } else {
            this.isImgTrueForOriginal = false;
            this.isPdfTrueForOriginal = false;
          }

          const reader = new FileReader();
          reader.onload = (e: any) => {
            if (this.isImgTrueForOriginal === false && this.isPdfTrueForOriginal === false) {
              this.originalFileImageSrc = '';
            } else {
              this.originalFileImageSrc = e.target.result;
            }
            const strImage = e.target.result.substring((e.target.result.indexOf(',') + 1));
            this.originalFile = strImage;
          };
          reader.readAsDataURL(event.target.files[0]);

          this.inCompleteForm.controls['OrigFileName'].setValue(filename);
          this.originalFileExtension = ext;
        }
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  uploadImageforfinal(event: any) {
    try {
      if (event.target.files && event.target.files[0]) {

        const logoElement = event.target.files.item(0);
        const uploadType = logoElement.type;
        const filename = event.target.files.item(0).name;
        const ext = filename.substr(filename.lastIndexOf('.') + 1);

        if (uploadType !== undefined && uploadType != null) {
          if (logoElement.type.indexOf('application/pdf') !== -1) {
            const tmppath = URL.createObjectURL(event.target.files[0]);
            this.tempPdfPathForFinal = tmppath;
            this.isPdfTrueForfinal = true;
            this.isImgTrueForfinal = false;
          } else if (logoElement.type.indexOf('image') !== -1) {
            this.isImgTrueForfinal = true;
            this.isPdfTrueForfinal = false;
          } else {
            this.isImgTrueForfinal = false;
            this.isPdfTrueForfinal = false;
          }

          const reader = new FileReader();
          reader.onload = (e: any) => {
            if (this.isImgTrueForfinal === false && this.isPdfTrueForfinal === false) {
              this.finalFileImageSrc = '';
            } else {
              this.finalFileImageSrc = e.target.result;
            }
            const strImage = e.target.result.substring((e.target.result.indexOf(',') + 1));
            this.finalFile = strImage;
          };
          reader.readAsDataURL(event.target.files[0]);

          this.inCompleteForm.controls['FinalFileName'].setValue(filename);
          this.finalFileExtension = ext;
        }
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  uploadImageforProof(event: any) {
    try {
      if (event.target.files && event.target.files[0]) {

        const logoElement = event.target.files.item(0);
        const uploadType = logoElement.type;
        const filename = event.target.files.item(0).name;
        const ext = filename.substr(filename.lastIndexOf('.') + 1);

        if (uploadType !== undefined && uploadType != null) {
          if (logoElement.type.indexOf('application/pdf') !== -1) {
            const tmppath = URL.createObjectURL(event.target.files[0]);
            this.tempPdfPathForProof = tmppath;
            this.isPdfTrueForProof = true;
            this.isImgTrueForProof = false;
          } else if (logoElement.type.indexOf('image') !== -1) {
            this.isImgTrueForProof = true;
            this.isPdfTrueForProof = false;
          } else {
            this.isImgTrueForProof = false;
            this.isPdfTrueForProof = false;
          }

          const reader = new FileReader();
          reader.onload = (e: any) => {
            if (this.isImgTrueForProof === false && this.isPdfTrueForProof === false) {
              this.proofFileImageSrc = '';
            } else {
              this.proofFileImageSrc = e.target.result;
            }
            const strImage = e.target.result.substring((e.target.result.indexOf(',') + 1));
            this.proofFile = strImage;
          };
          reader.readAsDataURL(event.target.files[0]);

          this.inCompleteForm.controls['ProofFileName'].setValue(filename);
          this.proofFileExtension = ext;
        }
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  showImageInViewerForOriginalFile() {
    try {
      if (this.isPdfTrueForOriginal === false && this.isImgTrueForOriginal === false) {
        if (this.originalFileImageSrc !== '' && this.originalFileImageSrc !== undefined && this.originalFileImageSrc != null) {
          window.open(this.tempPdfPathForOriginal, '_blank');
        }
      } else {
        if (this.isPdfTrueForOriginal === true) {
          window.open(this.tempPdfPathForOriginal, '_blank');
        } else if (this.isImgTrueForOriginal === true) {
          // tslint:disable-next-line:no-unused-expression
          new imgviewerfun('docs-pictures-originalFile', 'docs-buttons-originalFile');
        }
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  showImageInViewerForProofFile() {
    try {
      if (this.isPdfTrueForProof === false && this.isImgTrueForProof === false) {
        if (this.proofFileImageSrc !== '' && this.proofFileImageSrc !== undefined && this.proofFileImageSrc != null) {
          window.open(this.tempPdfPathForProof, '_blank');
        }
      } else {
        if (this.isPdfTrueForProof === true) {
          window.open(this.tempPdfPathForProof, '_blank');
        } else if (this.isImgTrueForProof === true) {
          // tslint:disable-next-line:no-unused-expression
          new imgviewerfun('docs-pictures-proofFile', 'docs-buttons-proofFile');
        }
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  showImageInViewerForFinalFile() {
    try {
      if (this.isPdfTrueForfinal === false && this.isImgTrueForfinal === false) {
        if (this.finalFileImageSrc !== '' && this.finalFileImageSrc !== undefined && this.finalFileImageSrc != null) {
          window.open(this.tempPdfPathForFinal, '_blank');
        }
      } else {
        if (this.isPdfTrueForfinal === true) {
          window.open(this.tempPdfPathForFinal, '_blank');
        } else if (this.isImgTrueForfinal === true) {
          // tslint:disable-next-line:no-unused-expression
          new imgviewerfun('docs-pictures-finalFile', 'docs-buttons-finalFile');
        }
      }
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  // ------------ End of upload and Image Viewer


  // ------------ Export :: Excel & PDF-----------
  exporttoExcel() {
    try {
      const inCompleteOrders = [];
      this.inCompleteOrders.forEach((element, index) => {
        this.objModelExport = {};
        this.objModelExport.AdvertiserName = element.AdvertiserName;
        this.objModelExport.AdDetails = element.AdTypeName + ' | ' + element.AdcolorName + ' | ' + element.AdSizeName;
        this.objModelExport.MaterialInfo = element.MaterialInfo;
        this.objModelExport.Rep = element.RepName;
        this.objModelExport.Position =(element.PositionName===null?'': element.PositionName);
        inCompleteOrders.push(this.objModelExport);
      });
      alasql('SELECT * INTO XLSX("InCompleteOrders.xlsx",{headers:true}) FROM ?', [inCompleteOrders]);
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  exportToPdf() {
    try {
      const tmpInCompleteOrdersForAutoTable = [];
      let obj = [];
      this.inCompleteOrders.forEach((element, index) => {
        obj = [];
        obj.push(element.AdvertiserName == null ? '' : element.AdvertiserName);
        obj.push(element.AdTypeName + ' | ' + element.AdcolorName + ' | ' + element.AdSizeName);
        obj.push(element.MaterialInfo);
        obj.push(element.RepName == null ? '' : element.RepName);
        obj.push(element.PositionName == null ? '' : element.PositionName);
        tmpInCompleteOrdersForAutoTable.push(obj);
      });

      const columns = ['Advertiser', 'AdDetails', 'Material Info', 'Rep', 'Position'];
      // Only pt supported (not mm or in)
      const doc = new jsPDF('p', 'pt');
      doc.autoTable(columns, tmpInCompleteOrdersForAutoTable, {
        theme: 'plain'
      });
      doc.save('InCompleteOrders.pdf');
    } catch (error) {
      this.hideloader();
      console.log(error);
    }
  }

  // ---------------End of Excel PDF--------------

  ngOnDestroy() {
    this.subscriptionForCustomExport.unsubscribe();
    this.subscriptionForCustomOrder.unsubscribe();
  }

  showloader() {
    this.appComponent.isLoading = true;
  }

  hideloader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 1000);
  }
}
