import { Injectable } from '@angular/core';


export function getDateInFormat(date: any): string {
    try {
        let monthtemp: Number;
        let daytemp: Number;

        const dateObj = new Date(date);
        monthtemp = dateObj.getMonth() + 1;
        let month = monthtemp.toString();
        if (month.toString().length === 1) {
            month = '0' + month;
        }
        daytemp = dateObj.getDate();
        let day = daytemp.toString();
        if (day.toString().length === 1) {
            day = '0' + day;
        }

        const year = dateObj.getFullYear();
        const newdate = month + '/' + day + '/' + year;
        return newdate;
    } catch (error) {
        console.log(error);
    }
}


export function getDate(dateTime: any): string {
    const coverDate_yy = new Date(dateTime).getFullYear();
    const coverDate_mm = new Date(dateTime).getMonth() + 1;
    const coverDate_dd = new Date(dateTime).getDate();
    const coverDate = coverDate_mm + '/' + coverDate_dd + '/' + coverDate_yy;
    return coverDate;
}

export function changeSelect2DropDownColor(controlId, controlValue): void {

}


@Injectable()
export class Common {
    constructor() { }
}
