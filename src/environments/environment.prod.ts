export const environment = {
  production: true,

  ApiBaseURL:  (<HTMLInputElement>document.getElementById('apiUrl')).value,
  websiteRoot: JSON.parse((<HTMLInputElement>document.getElementById('__ClientContext')).value).websiteRoot,
  imageUrl: 'areas/ng/iMISAngular_ProductionView/assets/image',  // New
  baseUrl: JSON.parse((<HTMLInputElement>document.getElementById('__ClientContext')).value).baseUrl,
  token: (<HTMLInputElement>document.getElementById('__RequestVerificationToken')).value,
  CurrentUserName: (<HTMLInputElement>document.getElementById('ctl01_AccountArea_PartyName')).innerText
};
